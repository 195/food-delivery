<?php

	/**
	* 
	*/
	class AdvertisesController extends AppController
	{
		public $uses = array('Advertise', 'AdvertiseDetail', 'Restaurant', 'Food');

		public function index(){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 1){
				$this->layout = 'admin';
				$this->Session->delete("advertise");
				$restaurant = $this->Restaurant->findByManager($this->Session->read('user.id'));
				$advertise = $this->Advertise->find('all', array('conditions' => array('restaurant_code' => $restaurant['Restaurant']['code'])));
				// pr($advertise);
				$advertises = array();
				date_default_timezone_set('Asia/Ho_Chi_Minh'); 
				foreach ($advertise as $adv) {
					if (date("Y-m-d") < $adv['Advertise']['time_start']) {
						$status = 'Chưa bắt đầu';
					} else if(date("Y-m-d") > $adv['Advertise']['time_end']){
						$status = 'Đã kết thúc';
					} else {
						$status = 'Đang diễn ra';
					}
					$data = array(
						'id' => $adv['Advertise']['id'],
						'name' => $adv['Advertise']['name'],
						'time_start' => $adv['Advertise']['time_start'],
						'time_end' => $adv['Advertise']['time_end'],
						'content' => $adv['Advertise']['content'],
						'status' => $status
						);
					array_push($advertises, $data);
				}
				$this->set('advertises',$advertises);
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function add_advertise(){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 1){
				$this->layout = 'admin';
				$this->Session->delete("advertise");
				$restaurant = $this->Restaurant->findByManager($this->Session->read('user.id'));
				$food = $this->Food->find('list', array("conditions" => array('restaurant_code' => $restaurant['Restaurant']['code'])));
				
				//lấy danh sách các món ăn đã tồn tại trong chương trình khuyến mại khác
				$advertise_created = $this->Advertise->find('all', array('conditions' => array('restaurant_code' => $restaurant['Restaurant']['code'])));
				$food_array = array();
				if(!empty($advertise_created)){
					foreach ($advertise_created as $adv) {
						$foods_selected = $this->AdvertiseDetail->find('all', array('conditions' => array('advertise_id' => $adv['Advertise']['id'])));
						if(!empty($foods_selected)) {
							foreach ($foods_selected as $foods) {
								$food_check = $this->Food->find('first', array('conditions' => array('id' => $foods['AdvertiseDetail']['food_id'])));
								$food_id = $food_check['Food']['id'];
								$food_name = $food_check['Food']['name'];
								$food_array[$food_id] = $food_name;
							}
						}
					}
				}
				// bỏ đi các món đã có trong chương trình khuyến mại khác
				$food_select = array_diff($food, $food_array);
				$this->set(compact('food_select'));
			
				$error_duplicate = null;
				$error_time = null;
				$error_check_required_price = null;
				$error_food_choice = null;
				if($this->request->is('post')){
				
					$this->Advertise->set($this->request->data);
					if($this->Advertise->validates()){
						$advertise = $this->request->data;
						// pr($advertise); die;
						$this->Session->delete("advertise");
						$this->Session->write("advertise",$advertise);

						// so sánh thời gian bắt đầu và thời gian kết thúc
						if($adv['Advertise']['time_start'] < $adv['Advertise']['time_end']){
							$check_time_rule = true;
						} else {
							$check_time_rule = false;
						}

						if($check_time_rule){
							// so sánh các món ăn trùng nhau
							$check_duplicate = true;
							$food_arr = $advertise['Advertise']['Food'];
							$check_exist_food = false;
							for ($i=0; $i < sizeof($food_arr) ; $i++) { 
								$number_duplicate = 0;
								if(isset($food_arr[$i]) && $food_arr[$i] != null){
									$food_crr_id = $food_arr[$i];
									$position_check = $i;
									$check_exist_food = true;
									for ($j = 0; $j < sizeof($food_arr) ; $j++) { 
										if(isset($food_arr[$j]) && $food_arr[$j] != null){
											$check_id = $food_arr[$j];
											if(strpos($food_crr_id, $check_id) !== false){
												$number_duplicate = $number_duplicate + 1;
												$position = $j;
												if($number_duplicate > 1){
													break;
												}
											}
										}
										
									}
								}

								if($number_duplicate > 1){
									$check_duplicate = false;
									break;
								}
							}

							// kiểm tra nhập giá tiền
							$price_arr = $advertise['Advertise']['price_sale'];
							$error_check_required_price = null;

							for ($k=0; $k < sizeof($price_arr) ; $k++) { 
								$price_sale = $price_arr[$k];
								if($food_arr[$k] != null){
									if($price_sale == null){
										if($error_check_required_price == null){
											$error_check_required_price = "Điền giá món ".($k+1);
										}else{
											$error_check_required_price .= ", món ".($k+1);
										}
									}
								}
							}

							date_default_timezone_set("Asia/Ho_Chi_Minh");
							$create_time = date("Y-m-d H:i:s");
							
							if($check_exist_food){
								if($error_check_required_price == null){
									if($check_duplicate){
										$data = array(
											'name' => $advertise['Advertise']['name'],
											'time_start' => $advertise['Advertise']['time_start'],
											'time_end' => $advertise['Advertise']['time_end'],
											'restaurant_code' => $restaurant['Restaurant']['code'],
											'content' => $advertise['Advertise']['content'],
											'create_time' => $create_time,
										);
										if($this->Advertise->save($data)){
											$last_id = $this->Advertise->find('first', array('conditions' => array('restaurant_code' => $restaurant['Restaurant']['code'], 'create_time' => $create_time)));
											for ($i=0; $i < sizeof($food_arr) ; $i++) { 
												if(isset($food_arr[$i]) && $food_arr[$i] != null){
													$price = $price_arr[$i];
													$food_id = $food_arr[$i];
													$details_data = array(
														"advertise_id" => $last_id['Advertise']['id'],
														"food_id" => $food_id,
														"sale_off_price" => $price,
														"create_time" => $create_time
														);
													if($this->AdvertiseDetail->save($details_data)){
														$food_data = array(
															"id" => $food_id,
															"is_sale" => 1
															);
														$this->Food->save($food_data);
													}
														
													
												}
											}
											$this->redirect(array("controller" => "advertises", "action" => "index"));

										}
									}else{
										$error_duplicate = "Món ".($position_check+1)." trùng vơí món ".($position+1);
									}
								}
							}else{
								$error_food_choice = "Khong co mon nao duoc chon de khuyen mai";
							}
						}else{
							$error_time = "Thơì gian kêt thuc sơm hơn thơì gian băt đâù";
						}

						
					}

					
				}
				$this->set(compact("error_duplicate","error_time","error_check_required_price","error_food_choice"));
			} else {
				$this->redirect(array('controller' => 'users' , 'action' => 'login_admin'));
			}
		}

		public function get_advertise_detail($id){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 1){
				$this->layout = 'admin';
				$this->Session->delete("advertise");
				$restaurant = $this->Restaurant->findByManager($this->Session->read('user.id'));
				$food = $this->Food->find('list', array("conditions" => array('restaurant_code' => $restaurant['Restaurant']['code'])));
				
				//lấy danh sách các món ăn đã tồn tại trong chương trình khuyến mại khác 
				$advertise_created = $this->Advertise->find('all', array('conditions' => array('restaurant_code' => $restaurant['Restaurant']['code'],'id !=' => $id),
					));
				$food_array = array();
				if(!empty($advertise_created)){
					foreach ($advertise_created as $adv) {
						$foods_selected = $this->AdvertiseDetail->find('all', array('conditions' => array('advertise_id' => $adv['Advertise']['id'])));
						if(!empty($foods_selected)) {
							foreach ($foods_selected as $foods) {
								$food_check = $this->Food->find('first', array('conditions' => array('id' => $foods['AdvertiseDetail']['food_id'])));
								$food_id = $food_check['Food']['id'];
								$food_name = $food_check['Food']['name'];
								$food_array[$food_id] = $food_name;
							}
						}
					}
				}

				$food_select = array_diff($food, $food_array);
				$this->set(compact('food_select'));

				// lấy thông tin khuyến mại
				$advertise_updated = $this->Advertise->findById($id);
				$name = $advertise_updated['Advertise']['name'];
				$time_start = $advertise_updated['Advertise']['time_start'];
				$time_end = $advertise_updated['Advertise']['time_end'];
				$content = $advertise_updated['Advertise']['content'];
				$advertise_updated_db = $this->AdvertiseDetail->find("all",array("advertise_id" => $id));

				// lấy danh sách món ăn khuyến mại
				$advertise_updated_details = array();
				for ($i = 0; $i < 5; $i++) { 
					$advertise_updated_details[$i]['no'] = $i+1;
					if(isset($advertise_updated_db[$i]['AdvertiseDetail']['id'])){
						$advertise_updated_details[$i]['id'] = $advertise_updated_db[$i]['AdvertiseDetail']['id'];
						$advertise_updated_details[$i]['food_id'] = $advertise_updated_db[$i]['AdvertiseDetail']['food_id'];
						$advertise_updated_details[$i]['sale_off_price'] = $advertise_updated_db[$i]['AdvertiseDetail']['sale_off_price'];
					}else{
						$advertise_updated_details[$i]['id'] = null;
						$advertise_updated_details[$i]['food_id'] = null;
						$advertise_updated_details[$i]['sale_off_price'] = null;
					}
					
				}

				$error_duplicate = null;
				$error_time = null;
				$error_food_choice = null;
				$error_check_required_price = null;
				if($this->request->is("post")){
					$this->Advertise->set($this->request->data);
					if($this->Advertise->validates()){
						$advertise = $this->request->data;
						// pr($advertise); die;
						$this->Session->delete("advertise");
						$this->Session->write("advertise",$advertise);

						// so sánh thời gian bắt đầu và thời gian kết thúc
						if($adv['Advertise']['time_start'] < $adv['Advertise']['time_end']){
							$check_time_rule = true;
						} else {
							$check_time_rule = false;
						}

						if($check_time_rule){
							$check_duplicate = true;
							$food_arr = $advertise['Advertise']['Food'];
							$check_exist_food = false;
							for ($i=0; $i < sizeof($food_arr) ; $i++) { 
								$number_duplicate = 0;
								if(isset($food_arr[$i]) && $food_arr[$i] != null){
									$food_crr_id = $food_arr[$i];
									$position_check = $i;
									$check_exist_food = true;
									for ($j = 0; $j < sizeof($food_arr) ; $j++) { 
										if(isset($food_arr[$j]) && $food_arr[$j] != null){
											$check_id = $food_arr[$j];
											if(strpos($food_crr_id, $check_id) !== false){
												$number_duplicate = $number_duplicate + 1;
												$position = $j;
												if($number_duplicate > 1){
													break;
												}
											}
										}										
									}
								}
								if($number_duplicate > 1){
									$check_duplicate = false;
									break;
								}
							}

							$price_arr = $advertise['Advertise']['price_sale'];
							$error_check_required_price = null;

							for ($k=0; $k < sizeof($price_arr) ; $k++) { 
								$price_sale = $price_arr[$k];
								if($food_arr[$k] != null){
									if($price_sale == null){
										if($error_check_required_price == null){
											$error_check_required_price = "Dien gia mon".($k+1);
										}else{
											$error_check_required_price .= " ,mon ".($k+1);
										}
									}else{
										
									}
								}
							}

							date_default_timezone_set("Asia/Ho_Chi_Minh");
							$update_time = date("Y-m-d H:i:s");
							$adv_id_arr = $advertise['Advertise']['adv_id'];

							if($check_exist_food){
								if($error_check_required_price == null){
									if($check_duplicate){
										$data = array(
										'id' => $id,
										'name' => $advertise['Advertise']['name'],
										'time_start' => $advertise['Advertise']['time_start'],
										'time_end' => $advertise['Advertise']['time_end'],
										'restaurant_code' => $restaurant['Restaurant']['code'],
										'content' => $advertise['Advertise']['content'],
										'update_time' => $update_time,
										);
										if($this->Advertise->save($data)){
										
											for ($i=0; $i < sizeof($food_arr) ; $i++) { 
												if(isset($food_arr[$i]) && $food_arr[$i] != null){
													$adv_id = $adv_id_arr[$i];
													$price = $price_arr[$i];
													$food_id = $food_arr[$i];
													if($adv_id != null){
														$details_data = array(
															'id' => $adv_id,
															"advertise_id" => $id,
															"food_id" => $food_id,
															"sale_off_price" => $price,
															"update_time" => $update_time
															);
													}else{
														$details_data = array(
															"advertise_id" => $id,
															"food_id" => $food_id,
															"sale_off_price" => $price,
															"create_time" => $update_time
															);
													}
													
													
													if($this->AdvertiseDetail->save($details_data)){

														$food_data = array(
															"id" => $food_id,
															"is_sale" => 1
															);
														$this->Food->save($food_data);
													}
														
													
												}
											}
											$this->redirect(array("controller" => "advertises", "action" => "index"));

										}
									}else{
										$error_duplicate = "Món ".($position_check+1)." trùng vơí món ".($position+1);
									}
								}
							}else{
								$error_food_choice = "Khong co mon nao duoc chon de khuyen mai";
							}
						}else{
							$error_time = "Thơì gian kêt thuc sơm hơn thơì gian băt đâù";
						}

						
					}


				}

				$this->set(compact("advertise_updated_details","name","time_end","time_start","content",
					"error_time","error_check_required_price","error_duplicate","error_food_choice"));
			} else {
				$this->redirect(array('controller' => 'users' , 'action' => 'login_admin'));
			}
		}


		public function delete_advertise($id){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 1){
				$this->redirect = false;
				if($this->Advertise->delete($id)){
					$food = $this->AdvertiseDetail->find('all', array('conditions' => array('advertise_id' => $id)));
					if(!empty($food)){
						foreach ($food as $food) {
							$data = array(
								'id' => $food['AdvertiseDetail']['food_id'],
								'is_sale' => 0,
								);
							$this->Food->save($data);
							$this->AdvertiseDetail->delete($food['AdvertiseDetail']['id']);
						}
					}
					return $this->redirect(array('controller' => 'advertises', 'action' => 'index') );
				}
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login'));
			}
		}
	}

?>