<?php

	/**
	* 
	*/
	class BillsController extends AppController
	{	

		public $uses = array("Food","Restaurant", "Bill","BillDetail","Advertise","AdvertiseDetail","User");

		public function index(){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == '1'){
				$this->layout = 'admin';
				$restaurant = $this->Restaurant->findByManager($this->Session->read('user.id'));
				$bill_processing = $this->Bill->find('all', array('conditions' => array('status' => 0, 'restaurant_code' => $restaurant['Restaurant']['code'])));
				$bill_success = $this->Bill->find('all', array('conditions' => array('status' => 1, 'restaurant_code' => $restaurant['Restaurant']['code'])));
				$bill_cancel = $this->Bill->find('all', array('conditions' => array('status' => 2, 'restaurant_code' => $restaurant['Restaurant']['code'])));
				$this->set(compact('bill_processing', 'bill_success', 'bill_cancel'));
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function add_to_cart($id = null){
			$this->autoRender = false;

			date_default_timezone_set("Asia/Ho_Chi_Minh");
			$time = date("H:i");
			$today = date("Y-m-d");
			$this->Session->delete("not_open");


			$food_id = $id;
			$food_db = $this->Food->findById($food_id);
			
			$restaurant_code = $food_db['Food']['restaurant_code'];
			$restaurant_db = $this->Restaurant->findByCode($restaurant_code);

			$open_time = $restaurant_db['Restaurant']['time_start'];
			$closed_time = $restaurant_db['Restaurant']['time_end'];
			$time_start = strtotime($open_time);
			$time_end = strtotime($closed_time);
			$time_current = strtotime(date('H:i:s'));
			if(($time_current - $time_start >= 0)&&($time_end - $time_current >= 0)){
				$open = 1;
			} else {
				$open = 0;
			}

			if($open == 1){
				$quantity = 1;
				$name = $food_db['Food']['name'];
				$is_sale = $food_db['Food']['is_sale'];
				$price = $food_db['Food']['price'];  
				if($is_sale == 0){
					$price = $food_db['Food']['price'];
				}else{
					//lấy giá khuyến mại nếu còn thời gian khuyến mại
					$advertise_details_db = $this->AdvertiseDetail->find("all", array("food_id" => $id));
					foreach ($advertise_details_db as $value) {
						$advertise_id = $value['AdvertiseDetail']['advertise_id'];
						$advertise_db = $this->Advertise->findById($advertise_id);
						// pr($advertise_id);
						// pr($advertise_db);
						$time_start = $advertise_db['Advertise']['time_start'];
						$time_end = $advertise_db['Advertise']['time_end'];

						if($today >= $time_start && $today <= $time_end){
							$price = $value['AdvertiseDetail']['sale_off_price'];
						}
					}
				}
				
				$bill_details = array(
					'name' => $name,
					'quantity' => $quantity,
					'price' => $price,
					'id' => $food_id
					);
				$this->Session->write('Bill.restaurant_code', $restaurant_code);
				//kiểm tra số lượng trong giỏ hàng
				if($this->Session->check("Bill.bill_item_count")){
					$item_count = $this->Session->read('Bill.bill_item_count');
				}else{
					$item_count = 0;
				}
				
				if($item_count == 0){
					$bill_details_new = array($item_count => $bill_details);					
					$this->Session->write("Bill.bill_details",$bill_details_new);
				}else{
					$bill_details_new = $this->Session->read("Bill.bill_details");
					$i = 0;
					$is_exist = 0;
					$position = null;
					foreach ($bill_details_new as $key => $value) {
						$food_id_check = $bill_details_new[$i]['id'];
						if(strpos($food_id_check, $food_id) !== false){
							$is_exist = 1;
							$position = $i;
							break;
						}
						$i++;
					}
					if($is_exist == 0){
						array_push($bill_details_new, $bill_details);
						$this->Session->delete("Bill.bill_details");
						$this->Session->write("Bill.bill_details",$bill_details_new);
					}else{
						$bill_details_new[$i]['quantity'] = $bill_details_new[$i]['quantity'] + 1;
						$bill_details_new[$i]['price'] = $bill_details_new[$i]['price'] + $price;

						$this->Session->delete("Bill.bill_details");
						$this->Session->write("Bill.bill_details",$bill_details_new);
 					}
					
				}
				
				if($this->Session->check("Bill.bill_total")){
					$money_total = $this->Session->read('Bill.bill_total');
					$money_total = $money_total + $price;
					$this->Session->delete("Bill.bill_total");
					$this->Session->write("Bill.bill_total", $money_total);
				}else{
					$money_total = 0;
					$money_total = $money_total + $price;
					$this->Session->delete("Bill.bill_total");
					$this->Session->write("Bill.bill_total", $money_total);
				}
				
				$item_count = $item_count + 1;
				$this->Session->delete("Bill.bill_item_count");
				$this->Session->write("Bill.bill_item_count",$item_count);
				
			}
			$this->set('open', $open);
			$this->redirect(array("controller" => "foods", "action" => "get_food_list",$restaurant_db['Restaurant']['code']));
			
			
		}


		public function add_bill_info($restaurant_code = null){
			$this->layout = 'customer';

			$user_id = $this->Session->read("user.id");
			$user_db = $this->User->findById($user_id);

			$receiver = null;
			$address = null;
			$delivery_phone = null;
			
			if($user_db != null){
				$receiver = $user_db['User']['name'];
				$address = $user_db['User']['address'];
				$delivery_phone = $user_db['User']['phone'];
			}

			$this->set(compact("receiver","address","delivery_phone"));

			if($this->request->is('post')){	
				$this->Bill->set($this->request->data);
				if($this->Bill->validates()){
					$data = $this->request->data;

					$receiver = $data['Bill']['receiver'];
					$delivery_address = $data['Bill']['delivery_address'];
					$delivery_phone = $data['Bill']['delivery_phone'];
					$note = $data['Bill']['note'];

					$restaurant_db = $this->Restaurant->findByCode($restaurant_code);
					$ship_distance = $this->GetDrivingDistance($restaurant_db['Restaurant']['address'], $delivery_address);

					$this->Session->write("Bill.delivery_address",$delivery_address);
					$this->Session->write("Bill.delivery_phone", $delivery_phone);
					$this->Session->write("Bill.ship_distance",$ship_distance);
					$this->Session->write("Bill.note", $note);
					$this->Session->write("Bill.receiver", $receiver);
					$this->redirect(array("controller" => "bills", "action" => "confirm_bill",$restaurant_code));
				}
			}
		}

		public function update_quantity(){
			$this->autoRender = false;
			if($this->request->isPost()){
				$data = $this->request->data;
				$position = $data['position'];
				$quantity = $data['quantity'];
				$total_price = $data['total_price'];
				$price = $data['price'];
				$item_count = $data['item_count'];

				$pre_bill_details = $this->Session->read("Bill.bill_details");
				$this->Session->delete("Bill.bill_item_count");
				$this->Session->delete("Bill.bill_total");
				$this->Session->delete("Bill.bill_details");
				$this->Session->write("Bill.bill_item_count",$item_count);
				$this->Session->write("Bill.bill_total",$total_price);

				$pre_bill_details[$position]['quantity'] = $quantity;
				$pre_bill_details[$position]['price'] = $price;

				$this->Session->write("Bill.bill_details",$pre_bill_details);
				
			}
			
		}

		
		

		public function delete_element(){
			$this->autoRender = false;
			if($this->request->isPost()){
				$data = $this->request->data;
				$position = $data['position'];
				$total_price = $data['total_price'];
				$item_count = $data['item_count'];

				$pre_bill_details = $this->Session->read("Bill.bill_details");
				$this->Session->delete("Bill.bill_item_count");
				$this->Session->delete("Bill.bill_total");
				$this->Session->delete("Bill.bill_details");
				$this->Session->write("Bill.bill_item_count",$item_count);
				$this->Session->write("Bill.bill_total",$total_price);
				$new_bill_details = array();
				$i = 0;
				$new_bill_details_length = 0;
				foreach ($pre_bill_details as $key => $value) {
					if($i != position){
						if($new_bill_details_length == 0){
							$new_bill_details = array($new_bill_details_length => $pre_bill_details[$i]); 
						}else{
							array_push($new_bill_details, $pre_bill_details[$i]);
						}
					}

					$i++;
				}

				$this->Session->write("Bill.bill_details",$new_bill_details);
				
			}
		}

		public function confirm_bill($restaurant_code = null){
			$this->layout = 'customer';
			$bill_details = $this->Session->read("Bill.bill_details");
			$time_distance_delivery = $this->Session->read("Bill.ship_distance");
			$distance = $time_distance_delivery['distance'];
			$bill_total = $this->Session->read("Bill.bill_total");
			$receiver = $this->Session->read("Bill.receiver");
			$delivery_phone = $this->Session->read("Bill.delivery_phone");
			$delivery_address = $this->Session->read("Bill.delivery_address");
			$note = $this->Session->read("Bill.note");
			$bill_detail = array();
			$i = 0;
			foreach ($bill_details as $key => $value) {
				$bill_detail[$key]['no'] = $i+1;
				$bill_detail[$key]['food_name'] = $bill_details[$i]['name'];
				$bill_detail[$key]['quantity'] = $bill_details[$i]['quantity'];
				$bill_detail[$key]['price'] = $bill_details[$i]['price'];
				$i++;
			}

			$restaurant_db = $this->Restaurant->findByCode($restaurant_code);
			$free_delivery = $restaurant_db["Restaurant"]['free_delivery'];
			$delivery_fee = $restaurant_db['Restaurant']['delivery_fee'];

			if($free_delivery != null){
				if($bill_total < $free_delivery){
					$ship_fee = $delivery_fee * $distance;
				}else{
					$ship_fee = 0;
				}
			}else{
				$ship_fee = $delivery_fee * $distance;
			}

			$this->Session->write("Bill.ship_fee", $ship_fee);
			
			$this->set(compact("bill_detail","bill_total","delivery_address", "delivery_phone",
				"receiver","note","ship_fee","restaurant_code"));
		}

		
		public function add_bill_to_db($restaurant_code = null){

			$this->autoRender = false;

			date_default_timezone_set("Asia/Ho_Chi_Minh");
			$crrYear = date("Y");
			$crrMonth = date("m");

			$create_time = date("Y-m-d H:i:s");
			$bill_count = $this->Bill->find("count", 
				array(
					"conditions" => array("bill_code LIKE" => $restaurant_code.$crrYear.$crrMonth."%")
					)
				);
			$bill_code = $restaurant_code.$crrYear.$crrMonth;
			$bill_count = $bill_count + 1;
			if($bill_count > 0 && $bill_count <10){
				$bill_code = $bill_code."000".$bill_count;
			}elseif($bill_count >= 10 && $bill_count < 100){
				$bill_code = $bill_code."00".$bill_count;
			}elseif($bill_count >= 100 && $bill_count < 1000){
				$bill_code = $bill_code."0".$bill_count;
			}elseif($bill_count >= 1000 && $bill_count < 10000){
				$bill_code = $bill_code.$bill_count;
			}
			$bill_details = $this->Session->read("Bill.bill_details");
			$bill_total = $this->Session->read("Bill.bill_total");
			$receiver = $this->Session->read("Bill.receiver");
			$delivery_phone = $this->Session->read("Bill.delivery_phone");
			$delivery_address = $this->Session->read("Bill.delivery_address");
			$note = $this->Session->read("Bill.note");
			$ship_fee = $this->Session->read("Bill.ship_fee");
			$total = $bill_total + $ship_fee;
			// pr($bill_details);die;
			$bill_data = array(
				"bill_code" => $bill_code,
				"restaurant_code" => $restaurant_code,
				"total" => $total,
				"ship_price" => $ship_fee,
				"status" => 0,
				"receiver" => $receiver,
				"delivery_phone" => $delivery_phone,
				"delivery_address" => $delivery_address,
				"note" => $note,
				"create_time" => $create_time
				);
			
			if($this->Bill->save($bill_data)){
				$restaurant_db = $this->Restaurant->findByCode($restaurant_code);
				$number_order = $restaurant_db['Restaurant']['number_order']; 
				$number_order = $number_order + 1;

				$restaurant_data = array(
					'id' => $restaurant_db['Restaurant']['id'],
					'number_order' => $number_order
					);

				$this->Restaurant->save($restaurant_data);

				foreach ($bill_details as $key => $value) {
					$food_id = $value['id'];
					$quantity = $value['quantity'];
					$amount = $value['price'];

					$bill_detail_data = array(
						"id" => null,
						"food_id" => $food_id,
						"quantity" => $quantity,
						"bill_code" => $bill_code,
						"amount" => $amount
						);
					if($this->BillDetail->save($bill_detail_data)){
						$food_db = $this->Food->findById($food_id);
						$food_data = array(
							'id' => $food_id,
							'number_order' => $food_db['Food']['number_order'] + $quantity
						);
						$this->Food->save($food_data);
						
					}

					
				}
				// die;
				$this->Session->delete("Bill");
				$this->Session->delete("not_open");

			$this->redirect(array("controller" => "bills", "action" => "success_bill"));
			}
		}

		public function success_bill(){
			$this->layout = 'customer';
		}

		


		public function GetDrivingDistance($address1, $address2)
		{	
			$address1_after = str_replace(" ", "+", $address1);
			$address2_after = str_replace(" ", "+", $address2);
			$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$address1_after."&destinations=".$address2_after."&mode=driving&language=en-UK";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    $response = curl_exec($ch);
		    curl_close($ch);
		    $response_a = json_decode($response, true);
		    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
		    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

		    return array('distance' => $dist, 'time' => $time);
		}
	
		public function update_bill_status_cancel($id=null){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == '1'){
				$this->autoRender = false;
				$bill = $this->Bill->findByBill_code($id);
				$data = array(
					'id' => $bill['Bill']['id'],
					'status' => 2
					);
				if($this->Bill->save($data)){
					return $this->redirect(array('controller' => 'bills', 'action' => 'index'));
				}				
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin') );
			}
		}

		public function update_bill_status_success($id=null){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == '1'){
				$this->autoRender = false;
				$bill = $this->Bill->findByBill_code($id);
				$data = array(
					'id' => $bill['Bill']['id'],
					'status' => 1
					);
				if($this->Bill->save($data)){
					return $this->redirect(array('controller' => 'bills', 'action' => 'index'));
				}				
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function update_bill($id = null)
		{
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == '1'){
				$this->layout = 'admin';
				$bill = $this->Bill->findByBill_code($id);
				// pr($bill); 
				$bill_detail = $this->BillDetail->findAllByBill_code($id);
				if(!empty($bill_detail)){
					$bill_details = array();
					$total = 0;
					foreach ($bill_detail as $bill_detail) {
						// pr($bill_detail); die;
						$total = $total + $bill_detail['BillDetail']['amount'];
						$food = $this->Food->findById($bill_detail['BillDetail']['food_id']);
						$data = array(
							'id' => $bill_detail['BillDetail']['id'],
							'food_id' => $bill_detail['BillDetail']['food_id'],
							'food_name' => $food['Food']['name'],
							'food_price' => $food['Food']['price'],
							'quantity' => $bill_detail['BillDetail']['quantity'],
							'amount' => $bill_detail['BillDetail']['amount'],
							);
						array_push($bill_details, $data);
					}
				}
				// pr($bill_details); die;
				if($this->request->is('post')){
					// pr($this->request->data);
					$new_bill = $this->request->data;
					$total = 0;
					date_default_timezone_set('Asia/Ho_Chi_Minh');
					foreach ($new_bill['Bill']['quantity'] as $index=>$quantity) {
						if ($quantity>0) {
							$id = $new_bill['Bill']['id'][$index];
							$bill_detail_index = $this->BillDetail->findById($id);
							$amount = ($bill_detail_index['BillDetail']['amount']/$bill_detail_index['BillDetail']['quantity'])*$quantity;
							$data = array(
								'id' => $id,
								'quantity' => $quantity,
								'amount' => $amount,
								'update_time' => date("Y-m-d H:i:s")
								);
							$total = $total + $amount;
							$this->BillDetail->save($data);
						}
					}
					$total = $total + $bill['Bill']['ship_price'];	
					$data1 = array(
						'id' => $bill['Bill']['id'],
						'receiver' => $new_bill['Bill']['receiver'],
						'delivery_phone' => $new_bill['Bill']['delivery_phone'],
						'delivery_address' => $new_bill['Bill']['delivery_address'],
						'note' => $new_bill['Bill']['note'],
						'status' => $new_bill['Bill']['status'],
						'total' => $total,
						'update_time' => date("Y-m-d H:i:s")
						);
					if($this->Bill->save($data1)){
						return $this->redirect(array('controller' => 'bills', 'action' => 'index'));
					}
				}
				$this->set(compact('bill', 'bill_details', 'total'));
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function get_bill_detail($id = null){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == '1'){
				$this->layout = 'admin';
				$bill = $this->Bill->findByBill_code($id);
				// pr($bill); 
				$bill_detail = $this->BillDetail->findAllByBill_code($id);
				if(!empty($bill_detail)){
					$bill_details = array();
					$total = 0;
					foreach ($bill_detail as $bill_detail) {
						// pr($bill_detail); die;
						$total = $total + $bill_detail['BillDetail']['amount'];
						$food = $this->Food->findById($bill_detail['BillDetail']['food_id']);
						if(empty($food)){
							$food_name = 'Món ăn đã bị xóa';
							$food_price = ' ';
						} else {
							$food_name = $food['Food']['name'];
							$food_price = $food['Food']['price'];
						}
						$data = array(
							'id' => $bill_detail['BillDetail']['id'],
							'food_id' => $bill_detail['BillDetail']['food_id'],
							'food_name' => $food_name,
							'food_price' => $food_price,
							'quantity' => $bill_detail['BillDetail']['quantity'],
							'amount' => $bill_detail['BillDetail']['amount'],
							);
						array_push($bill_details, $data);
					}
				}
				$this->set(compact('bill', 'bill_details', 'total'));
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

	}

?>