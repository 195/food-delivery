<?php

	/**
	* 
	*/
	class CategoriesController extends AppController
	{
		public $uses = array('Category', 'RestaurantCategory', 'Restaurant');

		public function index(){
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0) {
				$this->layout = "Admin";
				$categories = $this->Category->find('list', array('conditions' => array('type' => '0')));
				$category_list = array();
				foreach ($categories as $key => $category) {
					$number_restaurant = $this->RestaurantCategory->find('count', array('conditions' => array('category_id' => $key)));
					$data = array(
						'id' => $key,
						'name' => $category,
						'number_restaurant' => $number_restaurant
						);
					array_push($category_list, $data);
				}
				// pr($category_list);
				$this->set('category_list', $category_list);
				$error = '';
				if($this->request->is('post')){
					$this->Category->set($this->request->data);
					if($this->Category->validates()){
						$category_exist = $this->Category->find('first', array('conditions' => array('name' => $this->request->data['Category']['name'], 'type' => '0')));
						if(empty($category_exist)){
							date_default_timezone_set('Asia/Ho_Chi_Minh');
							$data = array(
								'name' => $this->request->data['Category']['name'],
								'type' => 0,
								'create_time' => date("Y-m-d H:i:s"),
								);
							if($this->Category->save($data)){
								return $this->redirect(array('controller' => 'categories', 'action' => 'index'));
							}
						} else {
							$error = 'Danh mục đã tồn tại.';
						}
					}
				}
				$this->set('error', $error);
			} else {
				$this->redirect(array('controller' => 'users', 'action' => 'login'));
			}
		}

		public function get_category_detail($id = null){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0){
				$this->layout = "admin";
				$category_detail = $this->RestaurantCategory->find('all', array('conditions' => array('category_id' => $id)));
				$restaurant_list = array();
				if(!empty($category_detail)){
					// pr($category_detail);
					foreach ($category_detail as $category) {
						$restaurant_info = $this->Restaurant->findByCode($category['RestaurantCategory']['restaurant_code']);
						$restaurant = array(
							'id' => $category['RestaurantCategory']['id'],
							'name' => $restaurant_info['Restaurant']['name'],
							'address' => $restaurant_info['Restaurant']['address'],
							'tel1' => $restaurant_info['Restaurant']['tel1'],
							'status' => $restaurant_info['Restaurant']['status']
							);
						array_push($restaurant_list, $restaurant);
					}
				}
				$category = $this->Category->findById($id);
				$data = array(
					'name' => $category['Category']['name'],
					'restaurant_list' => $restaurant_list,
					);
				$this->set('data', $data);
			}
		}

		public function delete($id = null){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0){
				$this->autoRender = false;
				$category_id = $this->RestaurantCategory->findById($id);
				if($this->RestaurantCategory->delete($id)){
					return $this->redirect(array('controller' => 'categories', 'action' => 'get_category_detail', $category_id['RestaurantCategory']['category_id']));
				}
			}
		}
	}

?>