<?php

	/**
	* 
	*/
	class CommentsController extends AppController
	{
		public $uses = array('Comment', 'User', 'Restaurant');

		public function index(){
			if($this->Session->check('user.name') && $this->Session->check('user.master_id') == 1){
				$this->layout = 'admin';
				$restaurant = $this->Restaurant->findByManager($this->Session->read('user.id'));
				$comments = $this->Comment->find('all', array('conditions' => array('restaurant_code' => $restaurant['Restaurant']['code'], 'parent_id' => null)));
				// pr($comments); 
				$comment_list = array();
				foreach ($comments as $comment) {
					$user = $this->User->findById($comment['Comment']['user_id']);
					$comment_children = $this->Comment->findByParent_id($comment['Comment']['id']);
					if(!empty($comment_children)){
						$data_children = array(
							'id' => $comment_children['Comment']['id'],
							'content' => $comment_children['Comment']['content'],
							'create_time' => $comment_children['Comment']['create_time']
							);
					} else {
						$data_children = array();
					}
					$data = array(
						'id' => $comment['Comment']['id'],
						'user' => $user['User']['name'],
						'content' => $comment['Comment']['content'],
						'create_time' => $comment['Comment']['create_time'],
						'children_comment' => $data_children
						);
					array_push($comment_list, $data);

					
					// pr($comment_list);
				} 
				$this->set('comment_list', $comment_list);
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function reply_comment($id = null){
			if($this->Session->check('user.name') && $this->Session->check('user.master_id') == 1){
				$this->layout = 'admin';
				$comment = $this->Comment->findById($id);
				$comment_reply = $this->Comment->findByParent_id($comment['Comment']['id']);
				$this->set(compact('comment', 'comment_reply')); 
				date_default_timezone_set('Asia/Ho_Chi_Minh');
				if($this->request->is('post')){
					if(!empty($comment_reply)){
						$data = array(
							'id' => $comment_reply['Comment']['id'],
							'content' => $this->request->data['Comment']['content']
							);
					} else {
						$data = array(
							'user_id' => $comment['Comment']['restaurant_code'],
							'content' => $this->request->data['Comment']['content'],
							'restaurant_code' => $comment['Comment']['restaurant_code'],
							'parent_id' => $comment['Comment']['id'],
							'create_time' => date("Y-m-d H:i:s"),
							);
					}
					if($this->Comment->save($data)){
						return $this->redirect(array('controller' => 'comments', 'action' => 'index'));
					}
				}
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}
	}

?>