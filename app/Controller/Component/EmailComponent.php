<?php

App::uses('Component','Controller');

App::uses('CakeEmail', 'Network/Email');
/**
* 
*/
class EmailComponent extends Component
{
    public function send($to,$subject,$data,$template){


        $email = new CakeEmail('gmail');
        $email->emailFormat('html')
        ->template($template)
        ->to($to)
        ->subject($subject)
        ->from(array( 'thuctap.jvb.mailhost@gmail.com' => 'Reset Password Buy Online System'))
        ->viewVars($data);
        
        if($email->send()){
            return true;
        }else{
            return false;
        }
    }
}

?>