<?php
	
	/**
	* 
	*/
	class FoodsController extends AppController
	{
		public $uses = array('Food', 'Restaurant', 'Category', 'Image','Comment','User', 'FavoriteRestaurant', 'Advertise', 'AdvertiseDetail');

		public function get_host(){
	    	$host = null;
		   	if((isset($_SERVER['HTTPS']))&&($_SERVER['HTTPS'] != 'off')){
		    	$host = "Https://";
		   	}else{
		    	$host = "Http://";
		   	}
		   	$host.= $_SERVER['SERVER_NAME'];
		   	$host .= "/";
		   	$uri = $_SERVER['REQUEST_URI'];
			$uri_explode = explode("/", $uri);
		   	$host .= $uri_explode[1];
		   	return $host;
	    }

		public function index(){
			if($this->Session->check('user.name') && $this->Session->check('user.master_id') == 1){
				$this->layout = 'admin';
				$restaurant = $this->Restaurant->findByManager($this->Session->read('user.id'));
				// pr($restaurant);
				$food_list = $this->Food->find('all', array('conditions' => array('restaurant_code' => $restaurant['Restaurant']['code']), 'order' => array('name ASC')));
				$foods = array();
				foreach ($food_list as $food) {
					$category = $this->Category->findById($food['Food']['category']);
					$data = array(
						'id' => $food['Food']['id'],
						'name' => $food['Food']['name'],
						'price' => $food['Food']['price'],
						'number_order' => $food['Food']['number_order'],
						'is_sale' => $food['Food']['is_sale'],
						'category' => $category['Category']['name'],
						'status' => $food['Food']['status'],
						'description' => $food['Food']['description']
						);
					array_push($foods, $data);
				}
				$this->set('foods',$foods);
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function add_food(){
			if($this->Session->check('user.name') && $this->Session->check('user.master_id') == 1){
				$this->layout = 'admin';
				$restaurant = $this->Restaurant->findByManager($this->Session->read('user.id'));
				$select = $this->Category->find('list', array('conditions' => array('type' => 1), 'order' => 'name ASC'));
				$error = '';
				if($this->request->is('post')){
					// pr($this->request->data); die;
					$this->Food->set($this->request->data);
					if($this->Food->validates()){
						$food = $this->request->data;
						date_default_timezone_set('Asia/Ho_Chi_Minh');
						if(!empty($food['Food']['new_category'])){
							$new_category = array(
								'name' => $food['Food']['new_category'],
								'type' => '1',
								'create_time' => date("Y-m-d H:i:s")
								);
							if($this->Category->save($new_category)){
								$category_id = $this->Category->getLastInsertID();
							}
						} else {
							$category_id = $food['Food']['category'];
						} 
						if(empty($category_id)){
							$error = 'Phải chọn một category';
						} else {
							
							$data1 = array(
								'name' => $food['Food']['name'],
								'price' => $food['Food']['price'],
								'restaurant_code' => $restaurant['Restaurant']['code'],
								'number_order' => 0,
								'status' => $food['Food']['status'],
								'category' => $category_id,
								'description' => $food['Food']['description'],
								'create_time' => date("Y-m-d H:i:s"),
								);
							if($this->Food->save($data1)){
								$food_id = $this->Food->find('first', array('conditions' => array('name' => $food['Food']['name'], 'restaurant_code' => $restaurant['Restaurant']['code'])));
								if(!empty($food['Food']['image'])){
									$image = $food['Food']['image'];
									$imageTypes = array("image/jpeg","image/jpg", "image/png"); //định dạng ảnh cho phép
									$uploadFolder = "upload"; //cần tạo folder upload trước
									$uploadPath = WWW_ROOT.$uploadFolder; //link đầy đủ
									$host = $this->get_host();
									 //link đầy đủ
								}
								foreach ($imageTypes as $type) {
									//kiểm tra đúng định dạng
									if($type == $image['type']){ 
										//kiểm tra không có lỗi trong quá trình upload
										if($image['error'] == 0){
											$imageType = explode('/', $type);
											$imageName = $this->convert_vi_to_en($food['Food']['name']).'_'.$restaurant['Restaurant']['code'].'.'.$imageType['1'];
											$full_image_path = $uploadPath.'/'.$imageName;
											if(move_uploaded_file($image['tmp_name'], $full_image_path)){
												$data2 = array(
													'name' => $imageName,
													'food_id' => $food_id['Food']['id'],
													'url' => $host.'/'.$uploadFolder.'/'.$imageName,
													'create_time' => date("Y-m-d H:i:s"),
													);
												$this->Image->save($data2);
											}

										} 
									}		
								}

								return $this->redirect(array('controller' => 'foods', 'action' => 'index'));
							}
						}
					}
				}
				$this->set(compact('select', 'error'));
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function update_food($id = null){
			if($this->Session->check('user.name') && $this->Session->check('user.master_id') == 1){
				$this->layout = 'admin';
				$food = $this->Food->findById($id);
				$select = $this->Category->find('list', array('conditions' => array('type' => 1)));
				$error = '';
				if($this->request->is('post')){
					$this->Food->set($this->request->data);
					if($this->Food->validates()){
						$new_food = $this->request->data;
						// pr($new_food);
						date_default_timezone_set('Asia/Ho_Chi_Minh'); 
						if(!empty($new_food['Food']['new_category'])){
							$data = array(
								'name' => $new_food['Food']['new_category'],
								'type' => '1',
								'create_time' => date("Y-m-d H:i:s")
								);
							if($this->Category->save($data)){
								$category_id = $this->Category->getLastInsertID();
							}
						} else {
							$category_id = $new_food['Food']['category'];
						}
						if(empty($category_id)){
							$error = 'Phải chọn một category';
						} else {
							$data1 = array(
								'id' => $id,
								'name' => $new_food['Food']['name'],
								'price' => $new_food['Food']['price'],
								'status' => $new_food['Food']['status'],
								'category' => $category_id,
								'description' => $new_food['Food']['description'],
								'update_time' => date("Y-m-d H:i:s"),
								);
							// pr($data1);
							if($this->Food->save($data1)){
								if(!empty($new_food['Food']['image'])){
									$image = $new_food['Food']['image'];
									$imageTypes = array("image/jpeg","image/jpg", "image/png");
									$uploadFolder = "upload"; 
									$uploadPath = WWW_ROOT.$uploadFolder;
									$host = $this->get_host();
									foreach ($imageTypes as $type) {
										//kiểm tra đúng định dạng
										if($type == $image['type']){ 
											if($image['error'] == 0){
												$imageType = explode('/', $type);
												$imageName = $this->convert_vi_to_en($new_food['Food']['name']).'_'.$food['Food']['restaurant_code'].'.'.$imageType['1'];
												$full_image_path = $uploadPath.'/'.$imageName;
												if(move_uploaded_file($image['tmp_name'], $full_image_path)){
													$image_exist = $this->Image->findByFood_id($id);
													if(empty($image_exist)){
														$data2 = array(
															'name' => $imageName,
															'food_id' => $id,
															'url' => $host.'/'.$uploadFolder.'/'.$imageName,
															'create_time' => date("Y-m-d H:i:s"),
															);
													}
													$this->Image->save($data2);
												} 
											} 
										}		
									}
								}
								return $this->redirect(array('controller' => 'foods', 'action' => 'index'));
							}
						}
					}
				}
				$this->set(compact('select', 'error', 'food'));
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function delete_food($id = null){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 1){
				$this->autoRender = false;
				if($this->Food->delete($id)){
					$image = $this->Image->findByFood_id($id);
					if(!empty($image)){
						$this->Image->delete($image['Image']['id']);
						$full_image_path = WWW_ROOT."upload/".$image['Image']['name'];
						if(file_exists($full_image_path)){
							unlink($full_image_path);
						}
					}
					return $this->redirect(array('controller' => 'foods', 'action' => 'index'));
				}
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function get_food_list($id = null) {
			
			$host = $this->get_host();
			$this->layout = 'customer';
			$restaurant = $this->Restaurant->findByCode($id);
			$img = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
			$favorite_restaurant = $this->FavoriteRestaurant->find('first', array('conditions' => array('restaurant_code' => $id, 'user_id' => $this->Session->read('user.id'))));
			date_default_timezone_set("Asia/Ho_Chi_Minh");
			$time_start = strtotime($restaurant['Restaurant']['time_start']);
			$time_end = strtotime($restaurant['Restaurant']['time_end']);
			$time_current = strtotime(date('H:i:s'));
			if(($time_current - $time_start >= 0)&&($time_end - $time_current >= 0)){
				$open = 1;
			} else {
				$open = 0;
			}
			$food_category = $this->Food->find('all',array(
				'fields' => 'DISTINCT category',
				'conditions' => array(
					'restaurant_code' => $restaurant['Restaurant']['code']
					),
				'order' => array('name ASC')
				));	
			// pr($foods);
			$food_list = array();
			foreach ($food_category as $food_category) {
				$category = $this->Category->findById($food_category['Food']['category']);
				$foods = $this->Food->find('all', array('conditions' => array('category' => $category['Category']['id'], 'restaurant_code' => $restaurant['Restaurant']['code'])) );
				$food = array();
				foreach ($foods as $foods) {
					$image = $this->Image->findByFood_id($foods['Food']['id']);

					if(!empty($image)){
						$url = $image['Image']['url'];
					} else {
						$url = '';
					}
					$data = array(
						'id' => $foods['Food']['id'],
						'name' => $foods['Food']['name'],
						'price' => $foods['Food']['price'],
						'description' => $foods['Food']['description'],
						'number_order' => $foods['Food']['number_order'],
						'status' => $foods['Food']['status'],
						'image' => $url 
						);
					array_push($food, $data);
				}
				$data1 = array(
					'category_id' => $category['Category']['id'],
					'category_name' => $category['Category']['name'],
					'food' => $food
					);
				array_push($food_list, $data1);
			}
			// pr($food_list); die;

			// ADVERTISE
			$advertise = $this->Advertise->find('all', array('conditions' => array('restaurant_code' => $restaurant['Restaurant']['code'])));
			$advertise_list = array();
			foreach ($advertise as $advertise) {
				$sale_foods = $this->AdvertiseDetail->find('all', array('conditions' => array('advertise_id' => $advertise['Advertise']['id'])));
				$sale_food_list = array();
				foreach ($sale_foods as $sale_foods) {
					$sale_food = $this->Food->findById($sale_foods['AdvertiseDetail']['food_id']);
					$image = $this->Image->findByFood_id($sale_food['Food']['id']);

					if(!empty($image)){
						$url = $image['Image']['url'];
					} else {
						$url = '';
					}
					$data2 = array(
						'id' => $sale_food['Food']['id'],
						'name' => $sale_food['Food']['name'],
						'price' => $sale_food['Food']['price'],
						'price_sale' => $sale_foods['AdvertiseDetail']['sale_off_price'],
						'description' => $sale_food['Food']['description'],
						'number_order' => $sale_food['Food']['number_order'],
						'status' => $sale_food['Food']['status'],
						'image' => $url 
						);
					$sale_food_list[] = $data2;
				}
				if(strtotime(date('Y-m-d')) < strtotime($advertise['Advertise']['time_start'])){
					$start = 0;
				} else if(strtotime(date('Y-m-d')) > strtotime($advertise['Advertise']['time_end'])){
					$start = 2;
				} else {
					$start = 1;
				}

				$data3 = array(
					'name' => $advertise['Advertise']['name'],
					'time_start' => $advertise['Advertise']['time_start'],
					'time_end' => $advertise['Advertise']['time_end'],
					'status' => $start,
					'content' => $advertise['Advertise']['content'],
					'food' => $sale_food_list, 
					);
				$advertise_list[] = $data3;
			}
			// pr($advertise_list); die;

			//COMMENT
			$comment_db = $this->Comment->find('all',
				array('conditions' => array(
					'restaurant_code' => $restaurant['Restaurant']['code'],
					'parent_id' => null,
					)));
			$comment_list = array();
			// pr($comment_db);
			foreach ($comment_db as $value) {
				$comment_list_user_id = $value['Comment']['user_id'];
				$user_db = $this->User->findById($comment_list_user_id);
				$sub_comment_db =  $this->Comment->find('first',
				array('conditions' => array(
					'restaurant_code' => $restaurant['Restaurant']['code'],
					'parent_id' => $value['Comment']['id'],
					)));				
				$data4 = array(
					'id' => $value['Comment']['id'],
					'content' => $value['Comment']['content'],
					'username' => $user_db['User']['name'],
					'subcomment' => $sub_comment_db,
					);
				$comment_list[] = $data4;
			}
			// pr($comment_list);
			// pr($food_list);die;
			// pr($foods);
			
			$error = null;
			if($this->request->isPost()){
				$data = $this->request->data;
				// $user_master_id = $this->Session->read("user.master_id");

				if($this->Session->check('user.name')){
					date_default_timezone_set("Asia/Ho_Chi_Minh");
					$create_time = date("Y-m-d H:i:s");
					$comment_data = array(
						'content' => $data['Comments']['content'],
						'user_id' => $this->Session->read("user.id"),
						'restaurant_code' => $restaurant['Restaurant']['code'],
						'create_time' => $create_time
						);
					if($this->Comment->save($comment_data)){
						return $this->redirect(array("controller" => "foods", "action" => "get_food_list", $restaurant['Restaurant']['code']));
					}
				}else{
					$error = "Bạn vui lòng đăng nhập để sử dụng tính năng này.";
				}
			} 
			$this->set(compact('restaurant','img', 'food_list', 'error','comment_list', 'favorite_restaurant', 'open', 'advertise_list'));
		}

		function convert_vi_to_en($str) {
	        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'a', $str);
	        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'e', $str);
	        $str = preg_replace("/(ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ)/", 'i', $str);
	        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'o', $str);
	        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'u', $str);
	        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'y', $str);
	        $str = preg_replace("/(đ|Đ)/", 'd', $str);
	        $str = html_entity_decode ($str);
	        $str = str_replace(array(' ','_',':',',','?'), '-', $str);
	        $str = str_replace("/","-",$str);
	        $str = str_replace(array('(',')','&'), '', $str);
	        $str = str_replace( "%", "percent", $str);
	        return $str;
	    }

	}

?>