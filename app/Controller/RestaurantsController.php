<?php

	/**
	* 
	*/
	class RestaurantsController extends AppController
	{
		public $uses = array('User','Restaurant', 'Category', 'RestaurantCategory','Image','FavoriteRestaurant');

		public function get_host(){
	    	$host = null;
		   	if((isset($_SERVER['HTTPS']))&&($_SERVER['HTTPS'] != 'off')){
		    	$host = "Https://";
		   	}else{
		    	$host = "Http://";
		   	}
		   	$host.= $_SERVER['SERVER_NAME'];
		   	$host .= "/";
		   	$uri = $_SERVER['REQUEST_URI'];
			$uri_explode = explode("/", $uri);
		   	$host .= $uri_explode[1];
		   	return $host;
	    }

		public function index(){
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0) {
				$this->layout = "Admin";
				$restaurant_processing = $this->Restaurant->find('all', array('conditions' => array('status' => '2')));
				$restaurants_processing = array();
				foreach ($restaurant_processing as $res) {
					$user = $this->User->findById($res['Restaurant']['manager']);
					if(!empty($user)){
						$data = array(
							'id' => $res['Restaurant']['id'],
							'code' => $res['Restaurant']['code'],
							'name' => $res['Restaurant']['name'],
							'address' => $res['Restaurant']['address'],
							'tel' => $res['Restaurant']['tel1'],
							'manager' => $user['User']['name'],
							);
					} else {
						$data = array(
							'id' => $res['Restaurant']['id'],
							'code' => $res['Restaurant']['code'],
							'name' => $res['Restaurant']['name'],
							'address' => $res['Restaurant']['address'],
							'tel' => $res['Restaurant']['tel1'],
							'manager' => '-1',
							);
					}
					array_push($restaurants_processing, $data);
				}
				$restaurant = $this->Restaurant->find('all', array('conditions' => array('not' => array('status' => '2'))));
				$restaurants = array();
				foreach ($restaurant as $res) {
					$user = $this->User->findById($res['Restaurant']['manager']);
					$data = array(
						'id' => $res['Restaurant']['id'],
						'code' => $res['Restaurant']['code'],
						'name' => $res['Restaurant']['name'],
						'address' => $res['Restaurant']['address'],
						'tel' => $res['Restaurant']['tel1'],
						'manager' => $user['User']['name'],
						'status' => $res['Restaurant']['status']
						);
					array_push($restaurants, $data);
				}
				// pr($restaurants);

				$this->set(compact('restaurants', 'restaurants_processing'));
			} else { 
				$this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function add_restaurant(){
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0) {
				$this->layout = "Admin";
				$this->Restaurant->create();
				$error = '';
				$select = $this->User->find('list', array('conditions' => array('master_id' => '3')));
				$select2 = $this->Category->find('list', array('conditions' => array('type' => '0')));
				// pr($select2);
				$this->set(compact('select', 'error', 'select2'));
				if($this->request->is('post')){
					// pr($this->request->data); 
					$this->Restaurant->set($this->request->data);
					if ($this->Restaurant->validates()) {	
						$restaurant = $this->request->data;
						$price_zone = $restaurant['Restaurant']['price_min'].' - '.$restaurant['Restaurant']['price_max'];
						date_default_timezone_set('Asia/Ho_Chi_Minh'); 
						$data = array(
							'name' => $restaurant['Restaurant']['name'],
							'code' => strtoupper($restaurant['Restaurant']['code']),
							'address' => $restaurant['Restaurant']['address'],
							'tel1' => $restaurant['Restaurant']['tel1'],
							'status' => $restaurant['Restaurant']['status'],
							'price_zone' => $price_zone,
							'manager' => $restaurant['Restaurant']['manager'],
							'minimum_order' => $restaurant['Restaurant']['minimum_order'],
							'time_start' => $restaurant['Restaurant']['time_start'],
							'time_end' => $restaurant['Restaurant']['time_end'],
							'delivery_fee' => $restaurant['Restaurant']['delivery_fee'],
							'create_time' => date("Y-m-d H:i:s"),
							);
						if($this->Restaurant->save($data)){
							if(!empty($restaurant['Restaurant']['category'])){
								foreach ($restaurant['Restaurant']['category'] as $category) {
									$data1 = array(
										'restaurant_code' => $restaurant['Restaurant']['code'],
										'category_id' => $category,
										'create_time' => date("Y-m-d H:i:s"), 
										);	
									$this->RestaurantCategory->create();
									$this->RestaurantCategory->save($data1);
								}
							}
							$data2 = array(
								'id' => $restaurant['Restaurant']['manager'],
								'master_id' => '1',
								'update_time' => date("Y-m-d H:i:s"),
								); 
							if($this->User->save($data2)){
								return $this->redirect(array('controller' => 'restaurants', 'action' => 'index'));
							}
						}	
					}
				}
				// $this->set('error', $error);
			} else {
				$this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function update_restaurant($id = null){
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0) {
				$this->layout = "Admin";
				if($id != null){
					$restaurant = $this->Restaurant->findById($id);
					if($restaurant != null){
						$user_processing = $this->User->find('list', array('conditions' => array('master_id' => '3')));
						$user = $this->User->findById($restaurant['Restaurant']['manager']);
						if(!empty($user)){
							$manager = array($user['User']['id'] => $user['User']['name']);
							$select = array_merge($user_processing,$manager);
						} else {
							$select = $user_processing;
						}
						$select2 = $this->Category->find('list', array('conditions' => array('type' => '0')));
						$category = $this->RestaurantCategory->find('all', array('conditions' => array('restaurant_code' => $restaurant['Restaurant']['code'])));
						$category_list = array();
						foreach ($category as $category) {
							array_push($category_list, $category['RestaurantCategory']['category_id']);
						}
						// pr($category_list);
						$this->set(compact('select','restaurant','error','category_list','select2'));
					} else {
						return $this->redirect(array('controller' => 'restaurants', 'action' => 'index'));
					}
					if($this->request->is('post')){
						$this->Restaurant->set($this->request->data);
						if($this->Restaurant->validates()){
							$restaurant_update = $this->request->data;
							// pr($restaurant_update); die;							
							date_default_timezone_set('Asia/Ho_Chi_Minh'); 
							$data = array(
								'id' => $id,
								'name' => $restaurant_update['Restaurant']['name'],
								'address' => $restaurant_update['Restaurant']['address'],
								'tel1' => $restaurant_update['Restaurant']['tel1'],
								'status' => $restaurant_update['Restaurant']['status'],
								'price_zone' => $restaurant_update['Restaurant']['price_zone'] ,
								'manager' => $restaurant_update['Restaurant']['manager'],
								'minimum_order' => $restaurant_update['Restaurant']['minimum_order'],
								'time_start' => $restaurant_update['Restaurant']['time_start'],
								'time_end' => $restaurant_update['Restaurant']['time_end'],
								'delivery_fee' => $restaurant_update['Restaurant']['delivery_fee'],
								'update_time' => date("Y-m-d H:i:s")
								);
							// pr($data);
							if($this->Restaurant->save($data)){
								if(!empty($restaurant_update['Restaurant']['category'] && $restaurant_update['Restaurant']['category'] != $category_list)){
									$this->RestaurantCategory->deleteAll(array('restaurant_code' => $restaurant['Restaurant']['code']));
									foreach ($restaurant_update['Restaurant']['category'] as $category) {
										$data1 = array(
											'restaurant_code' => $restaurant['Restaurant']['code'],
											'category_id' => $category,
											'create_time' => date("Y-m-d H:i:s"), 
											);	
										// pr($data1);
										$this->RestaurantCategory->create();
										$this->RestaurantCategory->save($data1);
									// die;
									}
								}

								if($restaurant['Restaurant']['manager'] != $restaurant_update['Restaurant']['manager']){
									if($restaurant['Restaurant']['manager'] != '-1'){
										$data1 = array(
											'id' => $restaurant['Restaurant']['manager'],
											'master_id' => 3,
											'update_time' => date("Y-m-d H:i:s"),
											);
										$this->User->save($data1);
									}
									$data2 = array(
										'id' => $restaurant_update['Restaurant']['manager'],
										'master_id' => 1,
										'update_time' => date("Y-m-d H:i:s"),
										);
									$this->User->save($data2);
								}
								return $this->redirect(array('controller' => 'restaurants', 'action' => 'index'));
							}
						}
					}
				} else {
					return $this->redirect(array('controller' => 'restaurants', 'action' => 'index'));
				}	
			} else {
				$this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function get_restaurant_info(){
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == 1) {
				$this->layout = "Admin";
				$restaurant = $this->Restaurant->find('first', array('conditions' => array('manager' => $this->Session->read('user.id'))));
				// pr($restaurant);
				$image = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
				$this->set(compact('restaurant', 'image'));
			} else {
				$this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function update_restaurant_info($id = null){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 1){
				$this->layout = "admin";
				$restaurant = $this->Restaurant->findById($id);
				// pr($restaurant);
				$this->set('restaurant', $restaurant);
				if($this->request->is('post')){
					// pr($this->request->data); die;
					$this->Restaurant->set($this->request->data);
					if($this->Restaurant->validates()){
						$restaurant = $this->request->data;
						if(!empty($restaurant['Restaurant']['image'])){
							$image = $restaurant['Restaurant']['image'];
							$imageTypes = array("image/jpeg","image/jpg", "image/png");
							$uploadFolder = "restaurant_img"; 
							$uploadPath = WWW_ROOT.$uploadFolder;
							$host = $this->get_host();
							foreach ($imageTypes as $type) {
								//kiểm tra đúng định dạng
								if($type == $image['type']){ 
									if($image['error'] == 0){
										$imageType = explode('/', $type);
										$imageName = $this->convert_vi_to_en($restaurant['Restaurant']['name']).'.'.$imageType['1'];
										$full_image_path = $uploadPath.'/'.$imageName;
										if(move_uploaded_file($image['tmp_name'], $full_image_path)){
											$image_exist = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
											if(empty($image_exist)){
												$data2 = array(
													'name' => $imageName,
													'restaurant_code' => $restaurant['Restaurant']['code'],
													'url' => $host.'/'.$uploadFolder.'/'.$imageName,
													'create_time' => date("Y-m-d H:i:s"),
												);
												$this->Image->save($data2);
											}
										} 
									} 
								}		
							}
											
						}
						date_default_timezone_set('Asia/Ho_Chi_Minh'); 
						$data = array(
							'id' => $id,
							'name' => $restaurant['Restaurant']['name'],
							'address' => $restaurant['Restaurant']['address'],
							'tel1' => $restaurant['Restaurant']['tel1'],
							'status' => $restaurant['Restaurant']['status'],
							'price_zone' => $restaurant['Restaurant']['price_zone'] ,
							'minimum_order' => $restaurant['Restaurant']['minimum_order'],
							'time_start' => $restaurant['Restaurant']['time_start'],
							'time_end' => $restaurant['Restaurant']['time_end'],
							'delivery_fee' => $restaurant['Restaurant']['delivery_fee'],
							'website' => $restaurant['Restaurant']['website'],
							'email' => $restaurant['Restaurant']['email'],
							'free_fee' => $restaurant['Restaurant']['free_fee'],
							'update_time' => date("Y-m-d H:i:s")
							);

						if($this->Restaurant->save($data)){
							$this->redirect(array('controller' => 'restaurants', 'action' => 'get_restaurant_info'));
						}
					}
				}
			} else {
				$this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function confirm_restaurant($id = null){
			$this->autoRender = 'false';
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0){
				$restaurant = $this->Restaurant->findById($id);
				$data = array(
					'id' => $restaurant['Restaurant']['manager'],
					'master_id' => 1,
					);
				$data2 = array(
					'id' => $id,
					'status' => 0
					);
				if($this->User->save($data)){
					if($this->Restaurant->save($data2)){
						return $this->redirect(array('controller' => 'restaurants', 'action' => 'index'));
					}
				}
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function register_restaurant(){
			$this->layout = 'customer';
			$select = $this->Category->find('list', array('conditions' => array('type' => '0')));
			$success = '';
			$exist_restaurant = $this->Restaurant->findByManager($this->Session->read('user.id'));
			if(!empty($exist_restaurant)){
				$success = 'false';
				return $this->redirect(array('controller' => 'restaurants', 'action' => 'register_restaurant'));
			}
			if($this->request->is('post')){
				// pr($this->request->data); die;
				$this->Restaurant->set($this->request->data);
				if($this->Restaurant->validates()){
					$restaurant = $this->request->data;
					$price_zone = $restaurant['Restaurant']['price_min'].' - '.$restaurant['Restaurant']['price_max'];
					date_default_timezone_set('Asia/Ho_Chi_Minh');
					if(!empty($restaurant['Restaurant']['category'])){
						foreach ($restaurant['Restaurant']['category'] as $category) {
							$data1 = array(
								'restaurant_code' => $restaurant['Restaurant']['code'],
								'category_id' => $category,
								'create_time' => date("Y-m-d H:i:s"), 
								);	
							$this->RestaurantCategory->save($data1);
						}
					}
					if(!empty($restaurant['Restaurant']['other_category'])) {
						$data2 = array(
							'name' => $restaurant['Restaurant']['other_category'],
							'type' => 0,
							'create_time' => date("Y-m-d H:i:s"), 
							);
						if($this->Category->save($data2)){
							$category_id = $this->Category->getLastInsertID();
							$data3 = array(
								'restaurant_code' => $restaurant['Restaurant']['code'],
								'category_id' => $category_id,
								'create_time' => date("Y-m-d H:i:s"), 
								);
							$this->RestaurantCategory->save($data3);
						}
					}
					if(!empty($restaurant['Restaurant']['image'])){
						$image = $restaurant['Restaurant']['image'];
						$imageTypes = array("image/jpeg","image/jpg", "image/png");
						$uploadFolder = "restaurant_img"; 
						$uploadPath = WWW_ROOT.$uploadFolder;
						$host = $this->get_host();
						foreach ($imageTypes as $type) {
							//kiểm tra đúng định dạng
							if($type == $image['type']){ 
								if($image['error'] == 0){
									$imageType = explode('/', $type);
									$imageName = $this->convert_vi_to_en($restaurant['Restaurant']['name']).'.'.$imageType['1'];
									$full_image_path = $uploadPath.'/'.$imageName;
									if(move_uploaded_file($image['tmp_name'], $full_image_path)){
										$image_exist = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
										if(empty($image_exist)){
											$data4 = array(
												'name' => $imageName,
												'restaurant_code' => $restaurant['Restaurant']['code'],
												'url' => $host.'/'.$uploadFolder.'/'.$imageName,
												'create_time' => date("Y-m-d H:i:s"),
											);
											$this->Image->save($data4);
										}
									} 
								} 
							}		
						}			
					}
					$data = array(
						'name' => $restaurant['Restaurant']['name'],
						'code' => strtoupper($restaurant['Restaurant']['code']),
						'address' => $restaurant['Restaurant']['address'],
						'tel1' => $restaurant['Restaurant']['tel1'],
						'status' => 2,
						'price_zone' => $price_zone,
						'manager' => $this->Session->read('user.id') ,
						'minimum_order' => $restaurant['Restaurant']['minimum_order'],
						'time_start' => $restaurant['Restaurant']['time_start'] ,
						'time_end' => $restaurant['Restaurant']['time_end'] ,
						'delivery_fee' => $restaurant['Restaurant']['delivery_fee'],
						'free_delivery' => $restaurant['Restaurant']['free_delivery'],
						'email' => $restaurant['Restaurant']['email'],
						'website' => $restaurant['Restaurant']['website'],
						'create_time' => date("Y-m-d H:i:s"),
						);
					if($this->Restaurant->save($data)){
						$success = 'true';
					} else {
						$success = 'false';
					}
				}
			}
			$this->set(compact('select', 'success'));
		}

		public function get_restaurant_list($id = null){
			$this->layout = 'customer';
			$category_restaurant = $this->RestaurantCategory->find('all', array('fields' => 'DISTINCT category_id'));
			$category_array = array();
			foreach ($category_restaurant as $category) {
				$categories = $this->Category->findById($category['RestaurantCategory']['category_id']);
				$data = array(
					'id' => $categories['Category']['id'],
					'name' => $categories['Category']['name']
					);
				array_push($category_array, $data);
			}
			$restaurant_list = array();
			date_default_timezone_set('Asia/Ho_Chi_Minh'); 
			if(empty($id)){
				$restaurant = $this->Restaurant->find('all');
				foreach ($restaurant as $restaurant) { 
				$time_start = strtotime($restaurant['Restaurant']['time_start']);
				$time_end = strtotime($restaurant['Restaurant']['time_end']);
				$time_current = strtotime(date('H:i:s'));
				if(($time_current - $time_start >= 0)&&($time_end - $time_current >= 0)){
					$open = 1;
				} else {
					$open = 0;
				}
					$image = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
					if(!empty($image)){
						$url = $image['Image']['url'];
					} else {
						$url = '';
					}
					$data = array(
						'id' => $restaurant['Restaurant']['id'],
						'code' => $restaurant['Restaurant']['code'],
						'name' => $restaurant['Restaurant']['name'],
						'address' =>$restaurant['Restaurant']['address'],
						'time_start' => $restaurant['Restaurant']['time_start'],
						'time_end' => $restaurant['Restaurant']['time_end'],
						'minimum_order' => $restaurant['Restaurant']['minimum_order'],
						'price_zone' => $restaurant['Restaurant']['price_zone'],
						'open' => $open,
						'url' => $url,	
						);
					array_push($restaurant_list, $data);
				}
			} else {
				$restaurant_code = $this->RestaurantCategory->findAllByCategory_id($id);
				// pr($restaurant_code); die;
				foreach ($restaurant_code as $code) {
					$restaurant = $this->Restaurant->findByCode($code['RestaurantCategory']['restaurant_code']);
					$time_start = strtotime($restaurant['Restaurant']['time_start']);
					$time_end = strtotime($restaurant['Restaurant']['time_end']);
					$time_current = strtotime(date('H:i:s'));
					if(($time_current - $time_start >= 0)&&($time_end - $time_current >= 0)){
						$open = 1;
					} else {
						$open = 0;
					}
					$image = $this->Image->findByRestaurant_code($code['RestaurantCategory']['restaurant_code']);
					if(!empty($image)){
						$url = $image['Image']['url'];
					} else {
						$url = '';
					}
					$data = array(
						'id' => $restaurant['Restaurant']['id'],
						'code' => $restaurant['Restaurant']['code'] ,
						'name' => $restaurant['Restaurant']['name'],
						'address' =>$restaurant['Restaurant']['address'],
						'time_start' => $restaurant['Restaurant']['time_start'],
						'time_end' => $restaurant['Restaurant']['time_end'],
						'minimum_order' => $restaurant['Restaurant']['minimum_order'],
						'price_zone' => $restaurant['Restaurant']['price_zone'],
						'open' => $open,
						'url' => $url,	
						);
					array_push($restaurant_list, $data);
				}
			}
			$this->set(compact('category_array', 'restaurant_list'));
			// pr($restaurant_list);
		}

		public function get_restaurant_search($id = null){
			$this->layout = 'customer';
			if($id == 1){
				$restaurant = $this->Session->read('Search.restaurant');
			} else if($id == 2) {
				$restaurant = $this->Session->read('Search.food');
			}
			$restaurant_list = array();
			// pr($restaurant); die;
			foreach ($restaurant as $restaurant) {
				date_default_timezone_set('Asia/Ho_Chi_Minh'); 
				$time_start = strtotime($restaurant['Restaurant']['time_start']);
				$time_end = strtotime($restaurant['Restaurant']['time_end']);
				$time_current = strtotime(date('H:i:s'));
				if(($time_current - $time_start >= 0)&&($time_end - $time_current >= 0)){
					$open = 1;
				} else {
					$open = 0;
				}
				
				$image = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
				if(!empty($image)){
					$url = $image['Image']['url'];
				} else {
					$url = '';
				}
				$data = array(
					'id' => $restaurant['Restaurant']['id'],
					'code' => $restaurant['Restaurant']['code'],
					'name' => $restaurant['Restaurant']['name'],
					'address' =>$restaurant['Restaurant']['address'],
					'time_start' => $restaurant['Restaurant']['time_start'],
					'time_end' => $restaurant['Restaurant']['time_end'],
					'minimum_order' => $restaurant['Restaurant']['minimum_order'],
					'price_zone' => $restaurant['Restaurant']['price_zone'],
					'open' => $open,
					'url' => $url,	
					);
				$restaurant_list[] = $data;
			}
			
			$this->set(compact('category_array', 'restaurant_list'));
		}

		public function add_to_favorite($id = null){
			$this->autoRender = false;
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == '2') {
				date_default_timezone_set('Asia/Ho_Chi_Minh'); 
				$data = array(
					'user_id' => $this->Session->read('user.id'),
					'restaurant_code' => $id,
					'create_time' => date("Y-m-d H:i:s")
					);
				if($this->FavoriteRestaurant->save($data)){
					return $this->redirect(array('controller' => 'foods', 'action' => 'get_food_list', $id));
				}
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_customer'));
			}
		}

		public function get_favorite_restaurant($id = null){
			$this->layout = 'customer';
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == '2') {
				$favorite_restaurant = $this->FavoriteRestaurant->findAllByUser_id($this->Session->read('user.id'));
				date_default_timezone_set('Asia/Ho_Chi_Minh'); 
				$cateogry_list = array();
				$restaurant_list = array();
				// pr($favorite_restaurant);
				if(!empty($favorite_restaurant)){
					$favorite_restaurant_code = array();
					$category_restaurant_code = array();
					foreach ($favorite_restaurant as $favorite_restaurant) {
						$restaurant_category = $this->RestaurantCategory->findAllByRestaurant_code($favorite_restaurant['FavoriteRestaurant']['restaurant_code']);
						if(!empty($restaurant_category)){
							$category = array();
							foreach ($restaurant_category as $restaurant_category) {
								// pr($restaurant_category);
									$category[] = $restaurant_category['RestaurantCategory']['category_id'];
							}
									// pr($category);
							$category_unique = array_unique($category);	
							foreach ($category_unique as $category_unique) {
								$category_detail = $this->Category->findById($category_unique);
								$category_list[] = $category_detail;
							}
						}
						// pr($restaurant_category);
						if(empty($id)){
				// pr($favorite_restaurant);
							$restaurant = $this->Restaurant->findByCode($favorite_restaurant['FavoriteRestaurant']['restaurant_code']);
							$time_start = strtotime($restaurant['Restaurant']['time_start']);
							$time_end = strtotime($restaurant['Restaurant']['time_end']);
							$time_current = strtotime(date('H:i:s'));
							if(($time_current - $time_start >= 0)&&($time_end - $time_current >= 0)){
								$open = 1;
							} else {
								$open = 0;
							}
							$image = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
							if(!empty($image)){
								$url = $image['Image']['url'];
							} else {
								$url = '';
							}
							$data = array(
								'id' => $restaurant['Restaurant']['id'],
								'code' => $restaurant['Restaurant']['code'] ,
								'name' => $restaurant['Restaurant']['name'],
								'address' =>$restaurant['Restaurant']['address'],
								'time_start' => $restaurant['Restaurant']['time_start'],
								'time_end' => $restaurant['Restaurant']['time_end'],
								'minimum_order' => $restaurant['Restaurant']['minimum_order'],
								'price_zone' => $restaurant['Restaurant']['price_zone'],
								'open' => $open,
								'url' => $url,	
								);
							$restaurant_list[] = $data;		
									
						} else {
							$favorite_restaurant_code[] = $favorite_restaurant['FavoriteRestaurant']['restaurant_code'];
							$restaurant_category = $this->RestaurantCategory->find('all', array('conditions' => array('category_id' => $id), 'fields' => array('restaurant_code')));
							if(!empty($restaurant_category)){
								foreach ($restaurant_category as $restaurant_category) {
									$category_restaurant_code[] = $restaurant_category['RestaurantCategory']['restaurant_code'];
								}								
							}
						}
					}
					$restaurant_arr = array_intersect($favorite_restaurant_code, $category_restaurant_code);
					if(!empty($restaurant_arr)){
						foreach ($restaurant_arr as $res) {
							$restaurant = $this->Restaurant->findByCode($res);
							$time_start = strtotime($restaurant['Restaurant']['time_start']);
							$time_end = strtotime($restaurant['Restaurant']['time_end']);
							$time_current = strtotime(date('H:i:s'));
							if(($time_current - $time_start >= 0)&&($time_end - $time_current >= 0)){
								$open = 1;
							} else {
								$open = 0;
							}
							$image = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
							if(!empty($image)){
								$url = $image['Image']['url'];
							} else {
								$url = '';
							}
							$data = array(
								'id' => $restaurant['Restaurant']['id'],
								'code' => $restaurant['Restaurant']['code'] ,
								'name' => $restaurant['Restaurant']['name'],
								'address' =>$restaurant['Restaurant']['address'],
								'time_start' => $restaurant['Restaurant']['time_start'],
								'time_end' => $restaurant['Restaurant']['time_end'],
								'minimum_order' => $restaurant['Restaurant']['minimum_order'],
								'price_zone' => $restaurant['Restaurant']['price_zone'],
								'open' => $open,
								'url' => $url,	
								);
							$restaurant_list[] = $data;
						}
					}
				}
				$this->set(compact('restaurant_list', 'category_list'));

			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_customer'));
			}
		}

		public function delete_favorite_restaurant($id = null){
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == '2') {
				$this->autoRender = 'false';
				$favorite_restaurant = $this->FavoriteRestaurant->find('first', array('conditions' => array('restaurant_code' => $id, 'user_id' => $this->Session->read('user.id'))));
				if(!empty($favorite_restaurant)){
					if($this->FavoriteRestaurant->delete($favorite_restaurant['FavoriteRestaurant']['id'])){
						return $this->redirect(array('controller' => 'restaurants', 'action' => 'get_favorite_restaurant'));
					}
				}
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_customer'));
			}
		}

		function convert_vi_to_en($str) {
	        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
	        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
	        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
	        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
	        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
	        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
	        $str = preg_replace("/(đ)/", 'd', $str);
	        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
	        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
	        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
	        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
	        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
	        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
	        $str = preg_replace("/(Đ)/", 'D', $str);
	        $str = html_entity_decode ($str);
	        $str = str_replace(array(' ','_',':',',','?'), '-', $str);
	        $str = str_replace("/","-",$str);
	        $str = str_replace(array('(',')','&'), '', $str);
	        $str = str_replace( "%", "percent", $str);
	        return $str;
	    }
	}

?>