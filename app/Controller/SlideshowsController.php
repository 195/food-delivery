<?php 
	/**
	* 
	*/
	class SlideshowsController extends AppController
	{
		public $uses = array('Slideshow', 'Restaurant');

		public function get_host(){
	    	$host = null;
		   	if((isset($_SERVER['HTTPS']))&&($_SERVER['HTTPS'] != 'off')){
		    	$host = "Https://";
		   	}else{
		    	$host = "Http://";
		   	}
		   	$host.= $_SERVER['SERVER_NAME'];
		   	$host .= "/";
		   	$uri = $_SERVER['REQUEST_URI'];
			$uri_explode = explode("/", $uri);
		   	$host .= $uri_explode[1];
		   	return $host;
	    }
		
		public function index()
		{
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0){
				$this->layout = 'admin';
				$restaurant = $this->Restaurant->find('all');
				$array_key = array();
				$array_value = array();
				foreach ($restaurant as $restaurant) {
					$array_key[] = $restaurant['Restaurant']['code'];
					$array_value[] = $restaurant['Restaurant']['name'];
				}
				$select = array_combine($array_key, $array_value);
				$image_slideshow = $this->Slideshow->find('all');
				$image_arr = array();
				foreach ($image_slideshow as $images) {
					$restaurant_name = $this->Restaurant->findByCode($images['Slideshow']['restaurant_code']);
					$data = array(
						'id' => $images['Slideshow']['id'],
						'image_link' => $images['Slideshow']['image_link'],
						'restaurant' => $restaurant_name['Restaurant']['name'],
						);
					$image_arr[] = $data;
				}
				// pr($image_arr);
				$this->set(compact('select', 'image_arr'));
				if($this->request->is('post')){
					$this->Slideshow->set($this->request->data);
					if($this->Slideshow->validates()){
						$slideshow = $this->request->data;
						// pr($slideshow);die;
						if(!empty($slideshow['Slideshow']['image'])){
							$image = $slideshow['Slideshow']['image'];
							$imageTypes = array("image/jpeg","image/jpg", "image/png");
							$uploadFolder = "restaurant_img"; 
							$uploadPath = WWW_ROOT.$uploadFolder;
							$host = $this->get_host();
							date_default_timezone_set('Asia/Ho_Chi_Minh'); 
							foreach ($imageTypes as $type) {
								//kiểm tra đúng định dạng
								if($type == $image['type']){ 
									if($image['error'] == 0){
										$imageType = explode('/', $type);
										$imageName = $this->convert_vi_to_en($image['name']);
										$full_image_path = $uploadPath.'/'.$imageName;
										if(move_uploaded_file($image['tmp_name'], $full_image_path)){
											$data2 = array(
												'image_name' => $imageName,
												'image_link' =>  $host.'/'.$uploadFolder.'/'.$imageName,
												'restaurant_code' => $slideshow['Slideshow']['restaurant_code'],
												'create_time' => date("Y-m-d H:i:s"),
											);
											if($this->Slideshow->save($data2)){
												return $this->redirect(array('controller' => 'slideshows', 'action' => 'index'));

											}
										} 
									} 
								}		
							}
											
						}
						
					}
				}
			} else{
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		public function delete_image($id = null){
			if($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0){
				$this->autoRender = false;
				$image = $this->Slideshow->findById($id);
				if(!empty($image)){
					$this->Slideshow->delete($image['Slideshow']['id']);
					$full_image_path = WWW_ROOT."restaurant_img/".$image['Slideshow']['image_name'];
					if(file_exists($full_image_path)){
						if(unlink($full_image_path)){
							$this->redirect(array('controller' => 'slideshows', 'action' => 'index'));
						}
					}
				}
			} else {
				return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
			}
		}

		function convert_vi_to_en($str) {
	        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'a', $str);
	        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'e', $str);
	        $str = preg_replace("/(ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ)/", 'i', $str);
	        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'o', $str);
	        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'u', $str);
	        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'y', $str);
	        $str = preg_replace("/(đ|Đ)/", 'd', $str);
	        $str = html_entity_decode ($str);
	        $str = str_replace(array(' ','_',':',',','?'), '-', $str);
	        $str = str_replace("/","-",$str);
	        $str = str_replace(array('(',')','&'), '', $str);
	        $str = str_replace( "%", "percent", $str);
	        return $str;
	    }
	}
?>