<?php
	
	/**
	* 
	*/
	class UsersController extends AppController
	{
		public $uses = array('User', 'Restaurant', 'Image', 'Food', 'Slideshow');
		public $components = array('Email');

		private function login(){
			$error = '';
			if($this->request->is('post')){
				// $this->User->set($this->request->data);
				$user = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'])));
				// pr($user); 
				if(!empty($user) && $user['User']['password'] == hash('sha256', $this->request->data['User']['password'] )){
					$this->Session->write('user.name',$user['User']['name']);
					$this->Session->write('user.master_id',$user['User']['master_id']);
					$this->Session->write('user.id', $user['User']['id']);
					
					switch ($user['User']['master_id']) {
						case '0':
							return $this->redirect(array('controller' => 'users', 'action' => 'get_user'));
							break;
						case '1':
							return $this->redirect(array('controller' => 'bills', 'action' => 'index'));
							break;
						case '2':
							return $this->redirect(array('controller' => 'users', 'action' => 'home'));
							break;
						
						default:
							$error = "Bạn chưa có quyền truy cập vào trang này.";
							break;
					}
				} else {
					$error = "Sai email hoặc mật khẩu.";
				}
				
			}
			$this->set('error', $error);
		}

		public function login_admin(){
			$this->layout = "Login";
			$this->login();
		}

		public function login_customer(){
			$this->layout = 'customer';
			$this->login();
		}

		public function logout(){
			$this->autoRender = false;
			$master_id = $this->Session->read('user.master_id');
			if($this->Session->delete('user')){
				if($master_id == 2){
					return $this->redirect(array('controller' => 'users', 'action' => 'home'));
				} else{
					return $this->redirect(array('controller' => 'users', 'action' => 'login_admin'));
				}
			}
		}

		public function reset_password_admin(){
			$this->layout = "login";
			$this->reset_password(1);
		}

		public function reset_password_customer(){
			$this->layout = "customer";
			$this->reset_password(2);
		}

		public function reset_password($type){
			
			$result = '';
			if($this->request->isPost()){
				$data = $this->request->data;
				$email = $data['User']['email'];

				$user = $this->User->find("first", array("conditions" => array("email" => $email)));

				if($user != null){
					$characters = '0123456789';
					$charactersLength = strlen($characters);
					$randomString = '';
					for ($i = 0; $i < 8; $i++){
						$randomString .= $characters[rand(0, $charactersLength - 1)];
					}
					$password = hash('sha256', $randomString);
					$data = array(
						'id' => $user['User']['id'],
						'password' => $password
						);

					if($this->User->save($data)){
						$data = array(
							'username' => $user['User']['username'],
							'new_password' => $randomString,
							'message' => 'Please change your account password immidately when you log in our system'
						);
						$mail = $this->Email->send($email,'Forget password email', $data ,'reset_password');

						if($mail != false){
							$result ='Please check your email to get new password';
							//Redirect to login view
							if($type == 1){
								$this->redirect(array("controller" => "users","action" => "login_admin"));
							}else{
								$this->redirect(array("controller" => "users","action" => "login_customer"));
							}
						}else{
							$resutl = 'Cannot send email to your email';
						}

					}else{
						$result = 'Please wait for a momment and reset password again';
					}
				}else{
					$result = 'Wrong email';
				}
			}

			$this->set(compact("result"));
		}

		public function add_user(){
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0) {
				$this->layout = "Admin";
				$this->User->create();
				$error = '';
				if($this->request->is('post')){
					$this->User->set($this->request->data);
					if($this->User->validates()){
						$new_user = $this->request->data;
						if($new_user['User']['password'] == $new_user['User']['re_password']){	
							date_default_timezone_set('Asia/Ho_Chi_Minh'); 
							$data = array(
								'password' =>  $new_user['User']['password'],
								'email' => $new_user['User']['email'],
								'phone' => $new_user['User']['phone'],
								'name' => $new_user['User']['name'],
								'address' => $new_user['User']['address'],
								'master_id' => $new_user['User']['master_id'],
								'create_time' => date("Y-m-d H:i:s")
								);
							// pr($data);
							if ($this->User->save($data)) {
								$last_id = $this->User->findByEmail($new_user['User']['email']);
								$password = hash('sha256', $new_user['User']['password']);
								$data2 = array(
									'id' => $last_id['User']['id'] ,
									'password' => $password
									);
								if($this->User->save($data2)){
									return $this->redirect(array('controller' => 'users', 'action' => 'get_user'));
								}
							}
						} else $error = "Mật khẩu nhập lại không đúng.";
					}	
				} 
				$this->set('error', $error);
			} else {
				$this->redirect(array('action' => 'login_admin'));
			}
		}

		public function get_user(){
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == 0) {
				$this->layout = "Admin";
				$customer = $this->User->findAllByMaster_id('2');
				$manager = $this->User->findAllByMaster_id('1');
				$admin = $this->User->findAllByMaster_id('0');
				$this->set(compact('customer', 'manager', 'admin'));

			} else {
				$this->redirect(array('action' => 'login_admin'));
			}
		}

		public function update_to_customer($id = null){
			$this->Session->delete('noti');
			
			$this->autoRender = 'false';
			$user = $this->User->findById($id);
			$user_restaurant = $this->Restaurant->findByManager($id);
			if(!empty($user_restaurant)){
				$data = array(
					'id' => $user_restaurant['Restaurant']['id'],
					'manager' => '-1',
					'status' => '2',
					);
				$this->Restaurant->save($data);
			}
			$data2 = array(
				'id' => $id,
				'master_id' => '2',
				);
			if($this->User->save($data2)){
				return $this->redirect(array('controller' => 'users', 'action' => 'get_user'));
			}
		}

		public function update_to_admin($id = null){
			$this->Session->delete('noti');

			$this->autoRender = 'false';
			$user = $this->User->findById($id);
			$user_restaurant = $this->Restaurant->findByManager($id);
			if(!empty($user_restaurant)){
				$data = array(
					'id' => $user_restaurant['Restaurant']['id'],
					'manager' => '-1',
					'status' => '2',
					);
				$this->Restaurant->save($data);
			}
			$data2 = array(
				'id' => $id,
				'master_id' => '0',
				);
			if($this->User->save($data2)){
				return $this->redirect(array('controller' => 'users', 'action' => 'get_user'));
			}
		}

		public function update_to_manager($id = null){
			$this->Session->delete('noti');
			$this->autoRender = 'false';
			$user = $this->User->findById($id);
			$user_restaurant = $this->Restaurant->findByManager($id);
			$data = array(
				'id' => $id,
				'master_id' => '3',
				);
			if($this->User->save($data)){
				$this->Session->write('noti',"Vui lòng vào mục Quản lý nhà hàng để chọn nhà hàng thành viên quản lý." );
				return $this->redirect(array('controller' => 'users', 'action' => 'get_user'));
			}
		}

		public function home(){
			$this->layout = 'customer';
			$slideshow = $this->Slideshow->find('all');
			$new_restaurants = array();
			$new_restaurant = $this->Restaurant->find('all', array('order' => array('create_time DESC'), 'limit' => '5'));
			foreach ($new_restaurant as $restaurant) {
				$image = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
				if(!empty($image)){
					array_push($restaurant['Restaurant'], $image['Image']['url']);
				}
				array_push($new_restaurants, $restaurant);
			}
			$special_restaurants = array();
			$special_restaurant = $this->Restaurant->find('all', array('order' => array('number_order DESC'), 'limit' => '5'));
			foreach ($special_restaurant as $restaurant) {
				$image = $this->Image->findByRestaurant_code($restaurant['Restaurant']['code']);
				if(!empty($image)){
					array_push($restaurant['Restaurant'], $image['Image']['url']);
				}
				array_push($special_restaurants, $restaurant);
			}
			$foods = $this->Food->find('all', array('order' => array('number_order DESC'), 'limit' => '9'));
			$food_list = array();
			foreach ($foods as $food) {
				$image = $this->Image->findByFood_id($food['Food']['id']);
				if(!empty($image)){
					$url = $image['Image']['url'];
				} else {
					$url = '';
				}
				$data1 = array(
					'id' => $food['Food']['id'],
					'name' => $food['Food']['name'],
					'restaurant_code' => $food['Food']['restaurant_code'],
					'url' => $url,
					'price'=> $food['Food']['price']
					);
				array_push($food_list, $data1);
			}
			// pr($food_list);
			$this->set(compact('new_restaurants','special_restaurants','food_list', 'slideshow'));
			if($this->request->is('post')){
				$this->Session->delete('Search');
				$data = $this->request->data;
				$restaurants = array();
				$restaurant = $this->Restaurant->find('all', array('conditions' => array('name LIKE' => '%'.$data['Restaurant']['name'].'%')));
				// pr($restaurant);
				$food = $this->Food->find('all', array('conditions' => array('name LIKE' => '%'.$data['Restaurant']['name'].'%')));
				// pr($food);
				if(!empty($food)){
					$food_restaurant = array();
					foreach ($food as $food) {
						$food_restaurant[] = $food['Food']['restaurant_code']; //bo sung loai bo thanh phan trung
					}
					$restaurants = array();
					$food_restaurant_unique = array_unique($food_restaurant);
					foreach ($food_restaurant_unique as $food_restaurant) {
						$restaurant_name = $this->Restaurant->findByCode($food_restaurant);
						$restaurants[] = $restaurant_name;
					}
				}
				$this->Session->write('Search.restaurant', $restaurant);
				$this->Session->write('Search.food', $restaurants);
				return $this->redirect(array('controller' => 'restaurants', 'action' => 'get_restaurant_search', '1'));
				// pr($restaurants); die;
			}
		}

		public function register(){
			$this->layout = 'customer';
			$error = '';
				if($this->request->is('post')){
					$this->User->set($this->request->data);
					if($this->User->validates()){
						$new_user = $this->request->data;
						if($new_user['User']['password'] == $new_user['User']['re_password']){	
							date_default_timezone_set('Asia/Ho_Chi_Minh'); 
							$data = array(
								'password' =>  $new_user['User']['password'],
								'email' => $new_user['User']['email'],
								'phone' => $new_user['User']['phone'],
								'name' => $new_user['User']['name'],
								'address' => $new_user['User']['address'],
								'master_id' => 2,
								'create_time' => date("Y-m-d H:i:s")
								);
							// pr($data);
							if ($this->User->save($data)) {
								$last_id = $this->User->findByEmail($new_user['User']['email']);
								$password = hash('sha256', $new_user['User']['password']);
								$data2 = array(
									'id' => $last_id['User']['id'],
									'password' => $password
									);
								if($this->User->save($data2)){
									$this->Session->write('user.name',$last_id['User']['name']);
									$this->Session->write('user.master_id',$last_id['User']['master_id']);
									$this->Session->write('user.id', $last_id['User']['id']);
									return $this->redirect(array('controller' => 'users', 'action' => 'home'));
								}
							}
						} else $error = "Mật khẩu xác nhận không đúng.";
					}	
				} 
				$this->set('error', $error);
		}


		public function update_user_info(){
			$this->layout = 'customer';
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == '2') {
				$user = $this->User->findById($this->Session->read('user.id'));
				if($this->request->is('post')){
					$this->Session->delete('noti');
					$this->User->set($this->request->data);
					if($this->User->validates()){
						if($this->User->save($this->request->data)){
							$this->Session->write('noti', 'Thông tin của bạn đã được cập nhật');
							return $this->redirect(array('controller' => 'users', 'action' => 'update_user_info'));
						}
					}
				}
				$this->set('user', $user);
			} else {
				return $this->redirect(array('action' => 'login_customer'));
			}
		}

		public function update_user_admin(){
			$this->layout = 'admin';
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') != '2') {
				$user = $this->User->findById($this->Session->read('user.id'));
				if($this->request->is('post')){
					$this->Session->delete('noti');
					if($this->User->save($this->request->data)){
						$this->Session->write('noti', 'Thông tin của bạn đã được cập nhật');
						return $this->redirect(array('controller' => 'users', 'action' => 'update_user_admin'));
					}
				}
				$this->set('user', $user);
			} else {
				return $this->redirect(array('action' => 'login_customer'));
			}
		}

		public function update_password(){
			
			$user = $this->User->findById($this->Session->read('user.id'));
			$error = '';
			if($this->request->is('post')){
				$this->Session->delete('success');
				$new_user = $this->request->data;
				if(!empty($new_user['User']['current_password'])&&!empty($new_user['User']['new_password'])&&!empty($new_user['User']['re_password'])){
					if($user['User']['password'] == hash('sha256', $new_user['User']['current_password'] )){
						if($new_user['User']['new_password'] == $new_user['User']['re_password']){
							$new_password = hash('sha256', $new_user['User']['new_password']);
							$data = array(
								'id' => $user['User']['id'],
								'password' => $new_password,
								);
							if($this->User->save($data)){
								$this->Session->write('success', 'Đổi mật khẩu thành công');
								if($this->Session->read('user.master_id') == '2'){
									return $this->redirect(array('controller' => 'users', 'action' => 'update_password'));
								} else {
									return $this->redirect(array('controller' => 'users', 'action' => 'update_password_admin'));
								}
							}
						} else {
							$error = "Mật khẩu nhập lại không đúng.";
						}
					} else {
						$error = "Mật khẩu hiện tại không đúng.";
					}
				} else {
					$error = "Vui lòng nhập vào tất cả các trường.";
				}
			}
			$this->set('error', $error);
		}

		public function update_password_customer(){
			$this->layout = 'customer';
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') == '2') {
				$this->update_password();
			} else {
				return $this->redirect(array('action' => 'login_customer'));
			}
		}

		public function update_password_admin(){
			$this->layout = 'admin';
			if ($this->Session->check('user.name') && $this->Session->read('user.master_id') != '2') {
				$this->update_password();
			} else {
				return $this->redirect(array('action' => 'login_admin'));
			}
		}
	}

?>