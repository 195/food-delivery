<?php

/**
* 
*/
class Bill extends AppModel
{
	
	public $validate = array(
		'receiver' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Xin vui lòng nhập tên',
				),
			),
		'delivery_phone' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Xin vui lòng nhập số điện thoại',
				),
			'number' => array(
				'rule' => '/^[0-9]{9,11}$/i',
    			'message' => 'Không đúng định dạng số điện thoại',
				),
			),
		'delivery_address' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Xin vui lòng nhập địa chỉ',
				),
			),

	);
}

?>