<?php 

	/**
	* 
	*/
	class Food extends AppModel
	{
		public $validate = array(
			'name' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Trường này là bắt buộc',
					),
				),
			'price' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Trường này là bắt buộc',
					),
				),
			);	
	}

?>