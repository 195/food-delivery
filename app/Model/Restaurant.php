<?php

	/**
	* 
	*/
	class Restaurant extends AppModel
	{
		public $validate = array(
			'name' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập tên nhà hàng.'
					),
				),
			'code' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập mã nhà hàng'
					),
				'ubique' => array(
					'rule' => array('isUnique'),
					'message' => 'Mã nhà hàng đã được sử dụng.'

					)
				),
			'address' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập địa chỉ'
					),
				),
			'tel1' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập số điện thoại'
					),
				'number' => array(
					'rule' => '/^[0-9]{7,11}$/i',
        			'message' => 'Không đúng định dạng số điện thoại'
					),
				),
			'price_zone' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập khoảng giá.'
					),
				),
			'manager' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng chọn người quản lý'
					),
				),
			'minimum_order' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập giá đặt tối thiểu.'
					),
				),
			'time_start' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập giở mở cửa'
					),
				),
			'time_end' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập giở đóng cửa'
					),
				),
			'delivery_fee' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập phí giao hàng'
					),
				),
			);
	}

?>