<?php 

	/**
	* 
	*/
	class User extends AppModel
	{
		public $validate = array(
			
			'password' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập mật khẩu.'
					),
				// 'between' => array(
		  //           'rule' => array('lengthBetween', 8, 20),
		  //           'message' => 'Between 8 to 20 characters'
		  //       	)
				),
			'email' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập email'
					),
				'unique' => array(
	              'rule' => array('isUnique'),
	              'message' => 'Email này đã được sử dụng.'
					),
				),
			'phone' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập số điện thoại'
					),
				'number' => array(
					'rule' => '/^[0-9]{9,11}$/i',
        			'message' => 'Không đúng định dạng số điện thoại'
					),
				),
			'name' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Xin vui lòng nhập tên.'
					),
				),
		);
	}

?>