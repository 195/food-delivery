
<?php 
	echo $this->element('top_admin'); 
	echo $this->element('left_manager');
?>
<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Thêm chương trình khuyến mại</p>
			<?php 
				echo $this->Form->create("Advertise",array("id" => "add", "novalidate" => true,"inputDefaults" => array(
	"label" => false))); 
			?>
			<table class="addTable">
				<?php 
					$advertise = $this->Session->read("advertise");
					if($advertise != null){
						$advertise_name = $advertise['Advertise']['name'];
						$advertise_time_start = $advertise['Advertise']['time_start'];
						$advertise_time_end = $advertise['Advertise']['time_end'];
						$advertise_food_arr = $advertise['Advertise']['Food'];
						$advertise_price_arr = $advertise['Advertise']['price_sale'];
						$advertise_content = $advertise['Advertise']['content'];

						?>
						<tr>
							<td width="25%">Tên chương trình <span style="color: red">*</span></td>
							<td colspan="3"><?php echo $this->Form->input('name',array("value" => $advertise_name)) ?></td>
						</tr>
						<?php
							if($error_time != null){?>
								<tr>
									<td colspan = "4">
										<span class="error-message"><?php echo $error_time; ?></span>
									</td>
								</tr>
						<?php
						}

						 ?>
						<tr class="select_time">
							<td>Thời gian <span style="color: red">*</span></td>
							<td ><?php echo $this->Form->input('time_start', array('type' => 'date',"value" => $advertise_time_start)) ?></td>
							<td> - </td>
							<td ><?php echo $this->Form->input('time_end', array('type' => 'date',"value" => $advertise_time_end)) ?></td>
						</tr>

						<tr>
							<td>Nội dung</td>
							<td colspan="3"><?php echo $this->Form->input('content', array('type' => 'textarea',
							"value" => $advertise_content)) ?></td>
						</tr>
						<tr>
							<td colspan="4">Món giảm giá</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="3">
								<table class="select_food">
									<?php if($error_duplicate != null){?>
										<tr><td colspan = "3"><span class="error-message"><?php echo $error_duplicate?></span></td></tr>
										<?php
									} ?>
									<?php 
										if($error_check_required_price != null){?>
											<tr><td colspan = "3"><span class="error-message"><?php echo $error_check_required_price?></span></td></tr>
										<?php
										}

									?>
									<?php
										if($error_food_choice != null){?>
											<tr><td colspan = "3"><span><?php echo $error_food_choice?></span></td></tr>
											<?php
										}
									?>
									<?php 
										$i = 0;
										foreach ($advertise_food_arr as $key => $value) {
											$food_id = $advertise_food_arr[$i];
											$price = $advertise_price_arr[$i];
											

											?>
											<tr>
												<td width="15%">Món <?php echo $i+1; ?></td>
												<td width="45%"><?php echo $this->Form->input("Food.", array('options' => $food_select,"default" => $food_id,"empty" => "----")) ?></td>
												<td width="30%"><?php echo $this->Form->input('price_sale.', array('placeholder' => 'giá khuyến mại',"default" => $price)) ?></td>
											</tr>
											<?php
											$i++;
										}

									?>
								</table>						
							</td>
						</tr>
						<?php
					}else{
						?>
						<tr>
							<td width="25%">Tên chương trình <span style="color: red">*</span></td>
							<td colspan="3"><?php echo $this->Form->input('name') ?></td>
						</tr>
						<tr class="select_time">
							<td>Thời gian <span style="color: red">*</span></td>
							<td ><?php echo $this->Form->input('time_start', array('type' => 'date')) ?></td>
							<td> - </td>
							<td ><?php echo $this->Form->input('time_end', array('type' => 'date')) ?></td>
						</tr>
						<tr>
							<td>Nội dung</td>
							<td colspan="3"><?php echo $this->Form->input('content', array('type' => 'textarea')) ?></td>
						</tr>
						<tr>
							<td colspan="4">Món giảm giá</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="3">
								<table class="select_food">
									
									<tr>
										<td width="15%">Món 1</td>
										<td width="45%"><?php echo $this->Form->input("Food.", array('options' => $food_select,"empty" => "-----","default" => null)) ?></td>
										<td width="30%"><?php echo $this->Form->input('price_sale.', array('placeholder' => 'giá khuyến mại')) ?></td>
									</tr>
									<tr>
										<td width="15%">Món 2</td>
										<td width="45%"><?php echo $this->Form->input("Food.", array('options' => $food_select, 'empty' => '---',"default" => null)) ?></td>
										<td width="30%"><?php echo $this->Form->input('price_sale.', array('placeholder' => 'giá khuyến mại')) ?></td>
									</tr>
									<tr>
										<td width="15%">Món 3</td>
										<td width="45%"><?php echo $this->Form->input("Food.", array('options' => $food_select, 'empty' => '---')) ?></td>
										<td width="30%"><?php echo $this->Form->input('price_sale.', array('placeholder' => 'giá khuyến mại')) ?></td>
									</tr>
									<tr>
										<td width="15%">Món 4</td>
										<td width="45%"><?php echo $this->Form->input("Food.", array('options' => $food_select, 'empty' => '---')) ?></td>
										<td width="30%"><?php echo $this->Form->input('price_sale.', array('placeholder' => 'giá khuyến mại')) ?></td>
									</tr>
									<tr>
										<td width="15%">Món 5</td>
										<td width="45%"><?php echo $this->Form->input("Food.", array('options' => $food_select, 'empty' => '---')) ?></td>
										<td width="30%"><?php echo $this->Form->input('price_sale.', array('placeholder' => 'giá khuyến mại')) ?></td>
									</tr>
								</table>						
							</td>
						</tr>
						<?php
					}

				?>
			</table>
			<table>
				<tr>
					<td><?php echo $this->Html->link('Quay lại', array('controller' => 'advertises', 'action' => 'index'), array('id' => 'button'))?> </td>
					<td><?php echo $this->Form->submit('Lưu', array('id' => 'button')); ?></td>
				</tr>	
			</table>
		</div>
	</div>
</div>
<?php 
	echo $this->element('bottom')
?>