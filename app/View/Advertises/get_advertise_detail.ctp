
<?php 
	echo $this->element('top_admin'); 
	echo $this->element('left_manager');
?>
<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Thêm chương trình khuyến mại</p>
			<?php 
				echo $this->Form->create("Advertise",array("id" => "add", "novalidate" => true,"inputDefaults" => array(
	"label" => false))); 
			?>
			<table class="addTable">										
				<?php 
					$advertise = $this->Session->read("advertise");
					if($advertise != null){
						
						$advertise_name = $advertise['Advertise']['name'];
						$advertise_time_start = $advertise['Advertise']['time_start'];
						$advertise_time_end = $advertise['Advertise']['time_end'];
						$advertise_content = $advertise['Advertise']['content'];
						$advertise_adv_id = $advertise['Advertise']['adv_id'];
						$advertise_food = $advertise['Advertise']['Food'];
						$advertise_price_sale = $advertise['Advertise']['price_sale'];
						?>	
						<tr>
							<td width="25%">Tên chương trình <span style="color: red">*</span></td>
							<td colspan="3"><?php echo $this->Form->input('name',array("value" => $advertise_name)) ?></td>
						</tr>
						<?php
							if($error_time != null){?>
								<tr>
									<td colspan = "4">
										<span style = "color:red;font-size:14px"><?php echo $error_time; ?></span>
									</td>
								</tr>
						<?php
						}

						 ?>
						<tr class="select_time">
							<td>Thời gian <span style="color: red">*</span></td>
							<td ><?php echo $this->Form->input('time_start', array('type' => 'date',"value" => $advertise_time_start)) ?></td>
							<td> - </td>
							<td ><?php echo $this->Form->input('time_end', array('type' => 'date',"value" => $advertise_time_end)) ?></td>
						</tr>
						<tr>
							<td>Nội dung</td>
							<td colspan="3"><?php echo $this->Form->input('content', array('type' => 'textarea', "value" => $advertise_content)) ?></td>
						</tr>
						<tr>
							<td colspan="4">Món giảm giá</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="3">
								<table class="select_food">
									<?php if($error_duplicate != null){?>
										<tr><td colspan = "3"><span><?php echo $error_duplicate?></span></td></tr>
										<?php
									} ?>
									<?php 
										if($error_check_required_price != null){?>
											<tr><td colspan = "3"><span><?php echo $error_check_required_price?></span></td></tr>
										<?php
										}

									?>
									<?php
										if($error_food_choice != null){?>
											<tr><td colspan = "3"><span><?php echo $error_food_choice?></span></td></tr>
											<?php
										}
									?>
									<?php 
										for ($i=0; $i < sizeof($advertise_adv_id) ; $i++){
										
											?>
											<tr>
												<td width="15%">Món <?php echo $i+1; 
												echo $this->Form->input("adv_id.", array("type" => "hidden", 'value' => $advertise_adv_id[$i])); ?></td>
												<td width="45%"><?php echo $this->Form->input("Food.", array('options' => $food_select,"empty" => "-----","default" => $advertise_food[$i])) ?></td>
												<td width="30%"><?php echo $this->Form->input('price_sale.', array('placeholder' => 'giá khuyến mại', 'default' => $advertise_price_sale[$i])) ?></td>
											</tr>

											<?php
										}
									?>
									
								</table>						
							</td>
						</tr>

						<?php

					}else{?>
						<tr>
							<td width="25%">Tên chương trình <span style="color: red">*</span></td>
							<td colspan="3"><?php echo $this->Form->input('name',array("value" => $name)) ?></td>
						</tr>
						<tr class="select_time">
							<td>Thời gian <span style="color: red">*</span></td>
							<td ><?php echo $this->Form->input('time_start', array('type' => 'date',"value" => $time_start)) ?></td>
							<td> - </td>
							<td ><?php echo $this->Form->input('time_end', array('type' => 'date',"value" => $time_end)) ?></td>
						</tr>
						<tr>
							<td>Nội dung</td>
							<td colspan="3"><?php echo $this->Form->input('content', array('type' => 'textarea', "value" => $content)) ?></td>
						</tr>
						<tr>
							<td colspan="4">Món giảm giá</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="3">
								<table class="select_food">
									
									<?php 
										foreach ($advertise_updated_details as $key => $value) {
											?>
											<tr>
												<td width="15%">Món <?php echo $value['no']; 
												echo $this->Form->input("adv_id.", array("type" => "hidden", 'value' => $value['id'])); ?></td>
												<td width="45%"><?php echo $this->Form->input("Food.", array('options' => $food_select,"empty" => "-----","default" => $value['food_id'])) ?></td>
												<td width="30%"><?php echo $this->Form->input('price_sale.', array('placeholder' => 'giá khuyến mại', 'default' => $value['sale_off_price'])) ?></td>
											</tr>

											<?php
										}
									?>
									
								</table>						
							</td>
						</tr>
					<?php 
					}
				?>		
			</table>
			<table>
				<tr>
					<td><?php echo $this->Html->link('Hủy', array('controller' => 'advertises', 'action' => 'index'), array('id' => 'button'))?> </td>
					<td><?php echo $this->Form->submit('Lưu', array('id' => 'button')); ?></td>
				</tr>	
			</table>
		</div>
	</div>
</div>
<?php 
	echo $this->element('bottom')
?>