
<?php 
	echo $this->element('top_admin'); 
	echo $this->element('left_manager');
?>

<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Danh sách các chương trình khuyến mại</p>
			<table class="get_table">
				<tr class="row_header">
					<td>STT</td>
					<td>Tên chương trình</td>
					<td>Thời gian bắt đầu</td>
					<td>Thời gian kết thúc</td>
					<td>Nội dung</td>
					<td>Trạng thái</td>
					<td></td>
					<td class="add_button"><?php echo $this->Html->image('add-button.png', array('url' => array('controller' => 'advertises', 'action' => 'add_advertise'))) ?></td>
				</tr>
				<?php if (!empty($advertises)) {
					$i = 1;
					foreach ($advertises as $adv) {?>
						<tr class="row">
							<td><?php echo $i++ ?></td>
							<td><?php echo $adv['name'] ?></td>
							<td><?php echo date('d-m-Y', strtotime($adv['time_start']))  ?></td>
							<td><?php echo date('d-m-Y', strtotime($adv['time_end'])) ?></td>
							<td><?php echo $adv['content'] ?></td>
							<td><?php echo $adv['status'] ?></td>
							<td><?php echo $this->Html->link('Xem chi tiết', array('controller' => 'advertises', 'action' => 'get_advertise_detail', $adv['id']))?> </td>
							<td><?php echo $this->Html->link('Xóa', array('controller' => 'advertises', 'action' => 'delete_advertise', $adv['id'])) ?></td>
						</tr>
					<?php }
				} else {?>
					<tr>
						<td>Chưa có chương trình khuyến mại nào</td>
					</tr>
				<?php }?>
			</table>
		</div>
	</div>
</div>
<?php 
	echo $this->element('bottom')
?>

