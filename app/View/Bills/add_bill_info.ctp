<?php echo $this->element('top_customer') ?>

<div class="highlight">
	<div class="main">
		<div class="col-md-11 bill-confirm" style="margin: 35px 95px">
			<h2>Thông tin giao hàng</h2>
			<?php echo $this->Form->create('Bill', array("method" => "post","novalidate" => true, "inputDefaults" => array("label" => false))) ?>		
			<div class="col-md-5" style="float: left;">
				<p class="input-label">Tên người nhận <span style="color: red">*</span></p>
				<?php echo $this->Form->input('receiver', array("value" => $receiver)) ?>
			</div>
			<div class="col-md-5" style="float: right;">
				<p class="input-label">Số điện thoại người nhận <span style="color: red">*</span></p>
				<?php echo $this->Form->input('delivery_phone',array("value" => $delivery_phone)) ?>
			</div>
			<div class="col-md-5" style="float: left;">
				<p class="input-label">Địa chỉ người nhận <span style="color: red">*</span></p>
				<?php echo $this->Form->input('delivery_address', array('type' => 'textarea', "value" => $address)) ?>
			</div>
			<div class="col-md-5" style="float: right;">
				<p class="input-label">Ghi chú </p>
				<?php echo $this->Form->input('note', array('type' => 'textarea')) ?>
			</div>
				<div class="btn">
					<?php echo $this->Html->link('Quay lại', array('controller' => 'foods', 'action' => 'get_food_list', $this->Session->read('Bill.restaurant_code')), array('class' => 'button button-left')) ?>
					<?php echo $this->Form->submit('Hoàn tất', array('class' => 'button button-right')) ?>
				</div>
			<?php echo $this->Form->end() ?>
		</div>
	</div>
</div>

<?php echo $this->element('bottom') ?>