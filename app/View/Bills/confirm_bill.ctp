<?php echo $this->element('top_customer') ?>

<div class="highlight">
		<div class="container">
			<div class=" register">
				<h2>Thông tin giao hàng</h2>
				
				<div class="bill-info">
					<p><b>Tên người nhận: </b><?php echo $receiver ?></p>
					<p><b>Địa chỉ người nhận: </b><?php echo $delivery_address?></p>
					<p><b>Số điện thoại người nhận: </b><?php echo $delivery_phone ?></p>
					<p><b>Ghi chú: </b><?php echo $note ?></p>
				</div>
				
				<table class="bill-detail">
					<tr class="bill-header">
						<td colspan="4">Hóa đơn</td>
					</tr>
					<tr class ="tittle">
						<td>STT</td>
						<td>Tên món</td>
						<td>Số lượng</td>
						<td>Thành tiền</td>
					</tr>
					<?php 
						foreach ($bill_detail as $key => $value) {
							?>
							<tr class="detail">
								<td><?php echo $value['no'] ?></td>
								<td><?php echo $value['food_name']?></td>
								<td><?php echo $value['quantity']?></td>
								<td><?php echo $value['price']?></td>
							</tr>
							<?php
						}

					?>
					<tr class="pay-info">
						<td colspan="2"></td>
						<td class="pay-info-tittle">Cộng</td>
						<td><?php echo $bill_total; ?></td>
					</tr>
					<tr class="pay-info">
						<td colspan="2"></td>
						<td  class="pay-info-tittle">Phí vận chuyển</td>
						<td><?php echo $ship_fee;?></td>
					</tr>
					<tr class="pay-info">
						<td colspan="2"></td>
						<td  class="pay-info-tittle">TỔNG CỘNG</td>
						<td><?php echo ($ship_fee + $bill_total) ?></td>
					</tr>
				</table>
				<p class="btn">
					<?php echo $this->Html->link('Quay lại', array('controller' => 'bills', 'action' => 'add_bill_info'), array('class' => 'button button-left')) ?>
					<?php echo $this->Html->link('Gửi đơn hàng', array("controller" => "bills", "action" => "add_bill_to_db",$restaurant_code), array('class' => 'button button-right')) ?>
				</p>
			</div>
		</div>
</div>

<?php 
	echo $this->element('bottom')
?>