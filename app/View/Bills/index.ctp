<?php 
	echo $this->element('top_admin');
	echo $this->element('left_manager');
?>

<!-- tab beginner -->
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
 <script>
 	$(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
	    });
	});
 </script> 
 <!-- tab end -->

<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Danh sách hóa đơn</p>
			<div class="tab-container">
				<ul class="tabs-menu">
			        <li class="current"><a href="#tab-1">Chưa xử lý</a></li>
			        <li><a href="#tab-2">Đã giao</a></li>
			        <li><a href="#tab-3">Hủy</a></li>
			    </ul>
			</div>
			<div class="tab">
				<div id="tab-1" class="tab-content">
					<table class="get_table">
						<tr class="row_header">
							<td>STT</td>
							<td>Mã hóa đơn</td>
							<td>Người nhận</td>
							<td>Số điện thoại</td>
							<td>Ghi chú</td>
							<td>Tổng tiền</td>
							<td>Thời gian gửi</td>
							<td></td>
						</tr>
						<?php if(!empty($bill_processing)){
							$i = 1;
							foreach ($bill_processing as $bill) {?>
								<tr class="row">
									<td><?php echo $i++ ?></td>
									<td><?php echo $bill['Bill']['bill_code'] ?></td>
									<td><?php echo $bill['Bill']['receiver'] ?></td>
									<td><?php echo $bill['Bill']['delivery_phone'] ?></td>
									<td><?php echo $bill['Bill']['note'] ?></td>
									<td><?php echo $bill['Bill']['total'] ?></td>
									<td><?php echo $bill['Bill']['create_time'] ?></td>
									<td>
										<span class="icon"><?php echo $this->Html->image("success.png", array('url' => array('controller' => 'bills', 'action' => 'update_bill_status_success',$bill['Bill']['bill_code']))) ?></span>
								    	<span class="icon"><?php echo $this->Html->image("cancel.png", array('url' => array('controller' => 'bills', 'action' => 'update_bill_status_cancel',$bill['Bill']['bill_code']))) ?></span>
							    		<span class="icon"><?php echo $this->Html->image("view_detail.png", array('url' => array('controller' => 'bills', 'action' => 'update_bill',$bill['Bill']['bill_code']))) ?></span>
									</td>
								</tr>
							<?php }
						} ?>
					</table>
				</div>
				<div id="tab-2" class="tab-content">
					<table class="get_table">
						<tr class="row_header">
							<td>STT</td>
							<td>Mã hóa đơn</td>
							<td>Người nhận</td>
							<td>Số điện thoại</td>
							<td>Ghi chú</td>
							<td>Tổng tiền</td>
							<td>Thời gian xử lý</td>
							<td></td>
						</tr>
						<?php if(!empty($bill_success)){
							$i = 1;
							foreach ($bill_success as $bill) {?>
								<tr class="row">
									<td><?php echo $i++ ?></td>
									<td><?php echo $bill['Bill']['bill_code'] ?></td>
									<td><?php echo $bill['Bill']['receiver'] ?></td>
									<td><?php echo $bill['Bill']['delivery_phone'] ?></td>
									<td><?php echo $bill['Bill']['note'] ?></td>
									<td><?php echo $bill['Bill']['total'] ?></td>
									<td><?php echo $bill['Bill']['create_time'] ?></td>
									<td><span class="icon"><?php echo $this->Html->image("view_detail.png", array('url' => array('controller' => 'bills', 'action' => 'get_bill_detail',$bill['Bill']['bill_code']))) ?></span></td>
								</tr>
							<?php }
						} else {?>
							<tr>
								<td>Chưa có hóa đơn nào</td>
							</tr>
						<?php } ?>
					</table>
				</div>
				<div id="tab-3" class="tab-content">
					<table class="get_table">
						<tr class="row_header">
							<td>STT</td>
							<td>Mã hóa đơn</td>
							<td>Người nhận</td>
							<td>Số điện thoại</td>
							<td>Ghi chú</td>
							<td>Tổng tiền</td>
							<td>Thời gian xử lý</td>
							<td></td>
						</tr>
						<?php if(!empty($bill_cancel)){
							$i = 1;
							foreach ($bill_cancel as $bill) {?>
								<tr class="row">
									<td><?php echo $i++ ?></td>
									<td><?php echo $bill['Bill']['bill_code'] ?></td>
									<td><?php echo $bill['Bill']['receiver'] ?></td>
									<td><?php echo $bill['Bill']['delivery_phone'] ?></td>
									<td><?php echo $bill['Bill']['note'] ?></td>
									<td><?php echo $bill['Bill']['total'] ?></td>
									<td><?php echo $bill['Bill']['create_time'] ?></td>
									<td><span class="icon"><?php echo $this->Html->image("view_detail.png", array('url' => array('controller' => 'bills', 'action' => 'view_bill_detail',$bill['Bill']['bill_code']))) ?></span></td>
								</tr>
							<?php }
						} else {?>
							<tr>
								<td>Chưa có hóa đơn nào</td>
							</tr>
						<?php  } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div
