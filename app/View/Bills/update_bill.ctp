<?php 
	echo $this->element('top_admin');
	echo $this->element('left_manager');
?>

<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Chi tiết hóa đơn</p>
			<?php echo $this->Form->create('Bill', array("id" => "add", "novalidate" => true,"inputDefaults" => array("label" => false)));
			 ?>
			<table class="bill-info">
				<tr>
					<td>Tên người nhận</td>
					<td><?php echo $this->Form->input('receiver', array('value' => $bill['Bill']['receiver'])) ?></td>
				</tr>
				<tr>
					<td>Số điện thoại người nhận</td>
					<td><?php echo $this->Form->input('delivery_phone', array('value' => $bill['Bill']['delivery_phone'])) ?></td>
				</tr>
				<tr>
					<td>Địa chỉ người nhận</td>
					<td><?php echo $this->Form->input('delivery_address', array('value' => $bill['Bill']['delivery_address'])) ?></td>
				</tr>
				<tr>
					<td>Ghi chú</td>
					<td><?php echo $this->Form->input('note', array('value' => $bill['Bill']['note'])) ?></td>
				</tr>
				<tr>
					<td>Status:</td>
					<td>
						<?php
							$select = array('0' => 'Đang xử lý', '1' => 'Đã giao', '2' => 'Hủy');
	  						echo $this->Form->input("status",array(
		  							"options" => $select,
		  							"default" => $bill['Bill']['status']
  								)); 
	  					?>
					</td>
				</tr>
			</table>
			
			<table class="bill-detail">
				<tr class ="tittle">
					<td>STT</td>
					<td>Tên món</td>
					<td>Giá tiền</td>
					<td>Số lượng</td>
					<td>Thành tiền</td>
				</tr>
				<?php 
					if(!empty($bill_details)){
						$i = 1;
						foreach ($bill_details as $bill_detail) {
						?>
						<tr class="detail">
							<td><?php echo $i++ ?></td>
							<td><?php echo $bill_detail['food_name'] ?></td>
							<td><?php echo $bill_detail['food_price'] ?></td>
							<td>
								<?php 
									echo $this->Form->hidden('id.', array('value' => $bill_detail['id']));
									echo $this->Form->input('quantity.', array('value' => $bill_detail['quantity'])); 
								?>
							</td>
							<td><?php echo $bill_detail['amount']?></td>
						</tr>
						<?php
						}
					}

				?>
				<tr class="pay-info">
					<td colspan="3"></td>
					<td class="pay-info-tittle">Cộng</td>
					<td><?php echo $total ?></td>
				</tr>
				<tr class="pay-info">
					<td colspan="3"></td>
					<td  class="pay-info-tittle">Phí vận chuyển</td>
					<td><?php echo $bill['Bill']['ship_price']; ?></td>
				</tr>
				<tr class="pay-info">
					<td colspan="3"></td>
					<td  class="pay-info-tittle">TỔNG CỘNG</td>
					<td><?php echo $bill['Bill']['total'] ?></td>
				</tr>
			</table>
			<table>
				<tr>
					<td><?php echo $this->Html->link('Back', array('controller' => 'bills', 'action' => 'index'), array('id' => 'button'))?> </td>
					<td><?php echo $this->Form->submit('Lưu', array('id' => 'button')); ?></td>
				</tr>	
			</table>
			<?php echo $this->Form->end();	 ?>
		</div>
	</div>
</div>