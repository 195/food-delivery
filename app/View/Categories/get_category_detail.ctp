<?php 
	echo $this->element('top_admin'); 
	echo $this->element('left_admin');
?>
<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Danh sách nhà hàng trong category</p>
			<p style="margin-left: 55px"><?php echo $this->Html->link('Quay lại', array('controller' => 'categories', 'action' => 'index')) ?></p>
			<table class="get_table">
				<tr class="row_tittle">
					<td colspan="6"><?php echo 'Danh mục: '.$data['name'] ?></td>
				</tr>
				<tr class="row_header"> 
					<td>STT</td>
					<td>Tên</td>
					<td>Địa chỉ</td>
					<td>Số điện thoại</td>
					<td>Trạng thái</td>
					<td></td>
				</tr>
				<?php 
					$i = '1';
					if(!empty($data['restaurant_list'])){
						foreach ($data['restaurant_list'] as $restaurant) { ?>
						<tr class="row">
							<td><?php echo $i++ ?></td>
							<td><?php echo $restaurant['name'] ?></td>
							<td><?php echo $restaurant['address'] ?></td>
							<td><?php echo $restaurant['tel1']?></td>
							<td><?php switch ($restaurant['status']) {
								case '0':
									echo 'Hoạt động';
									break;
								case '1':
									echo 'Đóng cửa';
									break;
								default:
									echo 'Đang xử lý';
									break;
							} ?></td>
							<td><?php echo $this->Html->link('Delete', array('controller' => 'categories', 'action' => 'delete', $restaurant['id'])) ?></td>
						</tr>	
					<?php } 
					}?>
			</table>
		</div>
	</div>
</div>
<?php 
	echo $this->element('bottom')
?>