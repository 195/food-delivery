<?php echo $this->element('top_admin'); ?>
<?php echo $this->element('left_admin') ?>

<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Danh mục nhà hàng</p>
			<table class="get_table">
				<tr class="row">
					<td width="15%">
						<?php 
							echo $this->Form->create('Category', array("id" => "add", "novalidate" => true,"inputDefaults" => array("label" => false)));
							echo 'Thêm Danh mục:' ;
						?>
					</td>
					<td width="24%"><?php echo  $this->Form->input('name'); echo $error?></td>
					<td>
						<?php 
							echo  $this->Form->submit('Thêm');
							echo $this->Form->end();
						?>
					</td>
					<td width="45%"></td>
				</tr>
			</table>
			<table class="get_table">
				<tr class="row_header"> 
					<td>STT</td>
					<td>Tên danh mục</td>
					<td>Số lượng nhà hàng</td>
					<td></td>
				</tr>
				<?php 
					$i = '1';
					if(!empty($category_list)){
						foreach ($category_list as $category) {?>
							<tr class="row">
								<td><?php echo $i++ ?></td>
								<td><?php echo $category['name'] ?></td>
								<td><?php echo $category['number_restaurant'] ?></td>
								<td><?php echo $this->Html->link('Xem chi tiết', array('controller' => 'categories', 'action' => 'get_category_detail', $category['id'])) ?></td>
							</tr>
						<?php }
					}
				?>
			</table>
		</div>
	</div>
</div>
<?php 
	echo $this->element('bottom')
?>