<?php 
	echo $this->element('top_admin');
	echo $this->element('left_manager');
?>

<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Bình luận về nhà hàng</p>
			<table class="get_table">
				<tr class="row_header">
					<td>STT</td>
					<td>Người gửi</td>
					<td width="45%">Nội dung</td>
					<td>Thời gian gửi</td>
					<td></td>
				</tr>
				<?php $i = 1;
				if(!empty($comment_list)){
					foreach ($comment_list as $comment) {?>
						<tr class="row">
							<td><?php echo $i++ ?></td>
							<td><?php echo $comment['user'] ?></td>
							<td>
								<?php echo $comment['content'];
								if(!empty($comment['children_comment'])){?>
									<table>
										<tr>
											<td><i>Trả lời:</i></td>
											<td><?php echo $comment['children_comment']['content'] ?></td>
										</tr>
									</table>
								<?php } ?>
							</td>
							<td><?php echo $comment['create_time'] ?></td>
							<td>
								<?php if(!empty($comment['children_comment'])){
									echo $this->Html->link('Sửa', array('controller' => 'comments', 'action' => 'reply_comment', $comment['id']));
								} else {
									echo $this->Html->link('Trả lời', array('controller' => 'comments', 'action' => 'reply_comment',$comment['id'])) ;?>
							</td>
							<?php } ?>
						</tr>
					<?php }
				} else {?>
					<tr>
						<td>Chưa có bình luận nào</td>
					</tr>
				<?php } ?>
			</table>
		</div>
	</div>
</div> 	
