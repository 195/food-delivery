<?php 
	echo $this->element('top_admin');
	echo $this->element('left_manager');
?>

<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Trả lời bình luận</p>
			<?php echo $this->Form->create("Comment",array("id" => "add", "novalidate" => true,"inputDefaults" => array("label" => false))); ?>
			<table class="addTable">
				<tr>
					<td>Bình luận</td>
					<td style="font-weight: normal;"><?php echo $comment['Comment']['content'] ?></td>
				</tr>
				<tr>
					<td>Trả lời</td>
					<td>
						<?php
							if(!empty($comment_reply)){
								echo $this->Form->input("content", array("type" => "textarea", "maxlength" => 1000, 'default' => $comment_reply['Comment']['content']));
							} else {
								echo $this->Form->input("content",array("type"=> "textarea","maxlength" => 1000)); 
							}
						?>
					</td>
				</tr>
			</table>
			<table class="button">
				<tr>
					<td width="75%"><?php echo $this->Html->link('Back', array('controller' => 'comments', 'action' => 'index'))?> </td>
					<td><?php echo $this->Form->submit('Trả lời'); ?></td>
				</tr>	
			</table>
		</div>
	</div>
</div>

<?php 
	echo $this->element('bottom')
?>