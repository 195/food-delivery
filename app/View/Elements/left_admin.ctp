 <aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
        
        	  <h5 class="centered"><?php echo $this->Html->link($this->Session->read('user.name'),array('controller' => 'users', 'action' => 'update_user_admin')) ?></h5>
        	  	
            <li class="sub-menu">
                <?php echo $this->Html->link('Thành viên', array('controller' => 'users', 'action' => 'get_user')) ?>
            </li>

            <li class="sub-menu">
                <?php echo $this->Html->link('Nhà hàng', array('controller' => 'restaurants', 'action' => 'index')) ?>
            </li>
            <li class="sub-menu">
                <?php echo $this->Html->link('Danh mục', array('controller' => 'categories', 'action' => 'index')) ?>
            </li>
            <li class="sub-menu">
                <?php echo $this->Html->link('Slideshow', array('controller' => 'slideshows', 'action' => 'index')) ?>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>