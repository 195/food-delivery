 <aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
        
        	  <h5 class="centered"><?php echo $this->Html->link($this->Session->read('user.name'),array('controller' => 'users', 'action' => 'update_user_admin')) ?></h5>
        	  	
            <li class="sub-menu">
                <?php echo $this->Html->link('Thông tin nhà hàng', array('controller' => 'restaurants', 'action' => 'get_restaurant_info')) ?>
            </li>

            <li class="sub-menu">
                <?php echo $this->Html->link('Món ăn', array('controller' => 'foods', 'action' => 'index')) ?>
            </li>
            <li class="sub-menu">
                <?php echo $this->Html->link('Thông tin khuyến mại', array('controller' => 'advertises', 'action' => 'index')) ?>
            </li>
            <li class="sub-menu">
                <?php echo $this->Html->link('Bình luận', array('controller' => 'comments', 'action' => 'index')) ?>
            </li>
            <li class="sub-menu">
                <?php echo $this->Html->link('Hóa đơn', array('controller' => 'bills', 'action' => 'index')) ?>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>