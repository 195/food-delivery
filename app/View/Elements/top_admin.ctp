<header class="header black-bg">
    <!--logo start-->
    <b><?php echo $this->Html->link('Buy online', array('controller' => 'users', 'action' => 'index_manager'), array('class' => 'logo')) ?></b>
    <!--logo end-->
    <div class="top-menu">
    	<ul class="nav pull-right top-menu">
            <li><?php echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'), array('class' => 'logout')) ?></li>
    	</ul>
    </div>
</header>