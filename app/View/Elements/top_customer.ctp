<div class="wrapper">
	<div class="top">
		<div class="main">
			<ul class="topnav">
				<?php if($this->Session->check('user.name')){?>
					<li><?php echo $this->Html->link('Thoát', array('controller' => 'users', 'action' => 'logout')) ?></li>
					<li><?php echo $this->Html->link('Nhà hàng yêu thích',array('controller' => 'restaurants', 'action' => 'get_favorite_restaurant'))?><span>|</span></li>
					<li><?php echo $this->Html->link('Đổi mật khẩu', array('controller' => 'users', 'action' => 'update_password_customer'))?><span>|</span></li>
					<li><?php echo $this->Html->link('Thông tin cá nhân', array('controller' => 'users', 'action' => 'update_user_info'))?><span>|</span></li>
					<li style="color: #fff"><?php echo 'Xin chào '.$this->Session->read('user.name')?><span>|</span></li>
				<?php } else {?>
					<li><?php echo $this->Html->link('Đăng kí thành viên', array('controller' => 'users', 'action' => 'register')) ?></li>
					<li><?php echo $this->Html->link('Đăng nhập', array('controller' => 'users', 'action' => 'login_customer')) ?><span>|</span></li>
				<?php } ?>
			</ul>
		</div> 
	</div>
	<div class="header">
		<div class="main">
			<div class="left-header">
				<h1 class="logo">
					<?php echo $this->Html->image('logo.png') ?>
				</h1>
				<p class="place">Hà Nội</p>
				<p class="slogan">Đặt món ăn trực tuyến - giao tận nơi</p>
			</div>
			<div class="right-header">
				<p style="float: right; padding-top: 15px; height: 89px"></p>
				<div class="nav">
					<ul>
						<li><?php echo $this->Html->link('Trang chủ', array('controller' => 'users', 'action' => 'home')) ?></li>
						<li><?php echo $this->Html->link('Chọn nhà hàng', array('controller' => 'restaurants', 'action' => 'get_restaurant_list')) ?></li>
						<li><?php echo $this->Html->link('Đăng kí nhà hàng', array('controller' => 'restaurants', 'action' => 'register_restaurant')) ?></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

</div>