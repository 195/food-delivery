<header class="header black-bg">
    <!--logo start-->
    <b><?php echo $this->Html->link('Food delivery', array('controller' => 'users', 'action' => 'index_manager'), array('class' => 'logo')) ?></b>
    <!--logo end-->
    <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav top-menu">
            <!-- inbox dropdown start-->
            <li id="header_inbox_bar" class="dropdown">

                <a data-toggle="dropdown" class="dropdown-toggle" href="../users/index">
                    <i class="fa fa-envelope-o">Notification</i>
                    <span class="badge bg-theme">5</span>
                </a>
            </li>
            <!-- inbox dropdown end -->
        </ul>
        <!--  notification end -->
    </div>
    <div class="top-menu">
    	<ul class="nav pull-right top-menu">
            <li><?php echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'), array('class' => 'logout')) ?></li>
    	</ul>
    </div>
</header>