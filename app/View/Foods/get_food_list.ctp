<?php echo $this->element('top_customer') ?>
<?php //pr($this->Session->read('Bill')) ?>

<!-- popup beginner -->
 	<!-- Include jQuery -->
   	<script src="https://code.jquery.com/jquery-1.8.2.min.js"></script>
 	<!-- Include jQuery Popup Overlay -->
   	<script src="https://cdn.rawgit.com/vast-engineering/jquery-popup-overlay/1.7.13/jquery.popupoverlay.js"></script>
 	<script>
 		$(document).ready(function() {
 
 		  // Initialize the plugin
 		  $('#my_popup').popup();
 
 		});
	
   	</script>
   	<script src="jquery.js" type="text/javascript"></script>
 	<script src="animate.js" type="text/javascript"></script>
 <!-- popup end -->
 
 <!-- tab beginner -->
 <script>
 	$(document).ready(function() {
     $(".restaurant-tab-info a").click(function(event) {
         event.preventDefault();
         $(this).parent().addClass("current");
         $(this).parent().siblings().removeClass("current");
         var tab = $(this).attr("href");
         $(".tab-content").not(tab).css("display", "none");
         $(tab).fadeIn();
     });
 });
 </script> 
 <!-- tab end -->

<div class="highlight">
	<div class="main">
		<div class="col-md-3">
			<div class="menu-categories">
				<div class="image">
					<?php if(!empty($img['Image']['url'])){?>
						<img src="<?php echo $img['Image']['url'] ?>" width="245">
					<?php } else{
						echo $this->Html->image('nophoto.jpg');
					} ?>
				</div>
				<h2>Danh mục</h2>
 				<ul>
	
 					<?php foreach ($food_list as $category) {?>
 						<li><a href="#<?php echo $category['category_id'] ?>"><?php echo $category['category_name'] ?></a></li>
 						<span></span>
 					<?php } ?>
 				</ul>
			</div>
		</div>
		<div class="col-md-8">
			<div class="basket">
				<a href="#" class="my_popup_open">
					<p id = "item_count" class="count">
						<?php
							if($this->Session->check('Bill.bill_item_count')){
					 			echo $this->Session->read('Bill.bill_item_count') ;
							} else {
								echo '0';
							}
					 	?>
		 			</p>
					<p id = "basket_price" class="price">
						<?php 
							if($this->Session->check('Bill.bill_total')){
								echo $this->Session->read('Bill.bill_total').'đ';
							} else {
								echo '0đ';
							}
						?> 
					</p>
				</a>
			</div>
			<div id="my_popup" class="opened">
			    <div class="basket-tittle"> Đơn đặt hàng</div>
			    <div class="basket_close">
			      	<span class="my_popup_close">X</span>
			    </div>
			    <div class="basket-detail">
			    	<?php if($this->Session->check('Bill')){?>
			    		<div class="product">
			    			<?php 
			    				$i = 0;
			    				$bill_details = $this->Session->read('Bill.bill_details');
			    				foreach ($bill_details as $bill) {?>
			    					<div id = "basket_entry_<?php echo $i; ?>" class="basket-entry">
			    						<div class="basket-product-quantity">
				    						<form>
				    							<div class="plus-minus-button">
				    								<button id = "minus_<?php echo $i ?>" class="button-white-minus" >-</button>
				    								<span id = "quantity_<?php echo $i?>"><?php echo $bill['quantity'] ?></span>
				    								<button id = 'plus_<?php echo $i ?>' class="button-white-plus">+</button>
				    							</div>
				    						</form>
			    						</div>
			    						<div class="basket-product-name">
			    							<?php echo $bill['name'] ?>
			    						</div>
			    						<div class="basket-product-delete">
			    							<form>
			    								<button id = 'delete_<?php echo $i; ?>' class = "button-delete">X</button>
			    							</form>	
			    						</div>
			    						<div id = "product_price_<?php echo $i ?>"class="basket-product-price"><?php echo $bill['price'].' đ' ?>
			    						</div>
			    					</div>	
			    			<?php $i++; }	?>
			    		</div>
			    		<div class="basket-total">
			    			<div id = "basket_total_price"class="basket-total-price"><?php echo $this->Session->read('Bill.bill_total').'đ' ?></div>
			    			<div class="basket-total-tittle">Tổng tiền</div>
			    		</div>
			    		<div class="basket-detail">
			    			<?php echo $this->Form->hidden("minimum_order", array("value" => $restaurant['Restaurant']['minimum_order'])) ?>
			    			<?php if($this->Session->read('Bill.bill_total') >= $restaurant['Restaurant']['minimum_order']){
			    					 echo $this->Html->link('Gửi đơn hàng', array('controller' => 'bills', 'action' => 'add_bill_info',$restaurant['Restaurant']['code']), array('class' => 'button-confirm'));
			    				?>
			    				<div class="basket-message">Bạn đã đạt mức đặt tối thiểu <b><?php echo $restaurant['Restaurant']['minimum_order'] ?>đ</b> để thanh toán</div>
			    			<?php } else {?>
			    				<?php echo $this->Html->link('Gửi đơn hàng', array('controller' => 'bills', 'action' => 'add_bill_info',$restaurant['Restaurant']['code']), array('class' => 'button-confirm', "style" => "display:none")); ?>
								<div class="basket-message">Xin lỗi bạn chưa thể đặt món. Nhà hàng yêu cầu đặt tối thiểu <b><?php echo $restaurant['Restaurant']['minimum_order'] ?>đ</b>.</div>
			    			<?php } ?>
			    		</div>
			    		<?php } else {?>
			    		<?php echo $this->Html->link('Gửi đơn hàng', array('controller' => 'bills', 'action' => 'add_bill_info',$restaurant['Restaurant']['code']), array('class' => 'button-confirm', "style" => "display:none")); ?>
			    		<div class="basket-message">Đơn hàng trống. Thêm món bằng cách nhấp vào thực đơn.</div>
			    	<?php } ?>


			    </div>
		    </div>
		    
			<div class="restaurant-info-panel">
				<h1 class="restaurant-info-name"><?php echo $restaurant['Restaurant']['name'] ?></h1>
				<div class="restaurant-info-panel-info">
						<?php
						echo $this->Html->image('address.png'); 
						echo ' '.$restaurant['Restaurant']['address']; ?>
						</br></br>
					<div class="restaurant-more-info">
						<?php echo $this->Html->image('working-time.png');?>
						<span style="position: relative; top: -2px;"><?php echo ' '.$restaurant['Restaurant']['time_start'].' - '.$restaurant['Restaurant']['time_end'].' | ';?></span>
						<?php echo $this->Html->image('price-zone.png');?>
						<span style="position: relative; top: -2px;"><?php echo ' '.$restaurant['Restaurant']['price_zone'];?></span>
						
					</div>
					</br>
					<div class="add-to-favorite">
						<?php if(!empty($favorite_restaurant)){
							echo '<span class="added">'.$this->Html->image('favorited.png').' Bạn đã thêm nhà hàng này vào danh sách yêu thích.</span>';
						} else {
							echo $this->Html->image('favorite.png', array('controller' => 'restaurants', 'action' => 'add_to_favorite', $restaurant['Restaurant']['code'])); echo $this->Html->link('Thêm vào yêu thích', array('controller' => 'restaurants', 'action' => 'add_to_favorite', $restaurant['Restaurant']['code']));	
						}?>
					</div>
					<div class="restaurant-info-minimum">
						
					</div>
				</div>
			</div>
			<?php 
			if($open == 0){?>
				<div class="note-restaurant">
					<i>Chúng tôi hiện tại đã ngưng nhận đơn hàng cho nhà hàng này. </br> Xin vui lòng quay lại vào lúc khác.</i>		
				</div>
			<?php } ?>

			<div class="restaurant-tab">
				<div class="restaurant-tab-info">
					<div class="current">
						<a href="#tab1" >Thực đơn</a>
					</div>
					<div>
						<a href="#tab2">Khuyến mại</a>
					</div>
					<div>
						<a href="#tab3">Nhận xét</a>
					</div>
				</div>
			</div>
			<div class="tab">
				<div class="tab-content" id="tab1">
					<div class="menu-list">
					<?php //pr($food_list) ?>
					<?php foreach ($food_list as $category) {
						$category_id = $category['category_id'];
						?>
						<div class="kind-food" id=<?php echo $category_id ?>>
							<?php echo $category['category_name'] ?>
						</div>
						<?php foreach ($category['food'] as $food) {?>
							<div class="menu-item"> 
								<div class="menu-item-add">
									<?php if($open == 0||$food['status'] == 1){
										echo $this->Html->image('button-not-order.png') ;
									} else {
										echo $this->Html->link('Đặt món', array('controller' => 'bills', 'action' => 'add_to_cart', $food['id']), array('class' => 'button-order'));
									}  ?>
								</div>
								<div class="menu-item-name">
									<?php if(!empty($food['image'])){?>
										<img src="<?php echo $food['image'] ?>">
									<?php } else {
										echo $this->Html->image('nophoto.jpg');
									}?>
									<b style="font-size: 14px"><?php echo $food['name']; ?></b></br>
									<i><?php echo $food['description'] ?></i></br>
									<?php if($food['status'] == 1) echo '<p style="color: red">Hết hàng</p>' ?>
									</br></br>
									<?php echo 'Đã được đặt <b>'.$food['number_order'].'</b> lần' ?>
								</div>
								<div class="menu-item-price">
								<?php echo $food['price'].' đ' ?>
									
								</div>
							</div>
							
						<?php } ?>

					<?php }?>
					
					</div>
				</div>
				<div id="tab2" class="tab-content">
					<?php foreach ($advertise_list as $adv) {
						echo '<h3>'.$adv['name'].'</h3>';
						switch ($adv['status']) {
							case '1':
								$status = 'Đang diễn ra';
								break;
							case '2':
								$status = "Đã kết thúc";
								break;
							default:
								$status = "Chưa bắt đầu";
								break;
						}
						echo '<p style="color: red">'.$status.'</p></br>';
						echo 'Thời gian: '.date('d/m/Y', strtotime($adv['time_start'])).' đến '.date('d/m/Y', strtotime($adv['time_end'])).'</br>';
						echo $adv['content'].'</br>';
						foreach ($adv['food'] as $food) {?>
							<div class="menu-item"> 
								<div class="menu-item-add">
									<?php if($adv['status'] != 1||$food['status'] == 1||$open == 0){
										echo $this->Html->image('button-not-order.png') ;
									} else {
										echo $this->Html->link('Đặt món', array('controller' => 'bills', 'action' => 'add_to_cart', $food['id']), array('class' => 'button-order'));
									}  ?>
								</div>
								<div class="menu-item-name">
									<?php if(!empty($food['image'])){?>
										<img src="<?php echo $food['image'] ?>">
									<?php } else {
										echo $this->Html->image('nophoto.jpg');
									}?>
									<b style="font-size: 14px"><?php echo $food['name']; ?></b></br>
									<i><?php echo $food['description'] ?></i></br>
									<?php if($food['status'] == 1) echo '<p style="color: red">Hết hàng</p>' ?>
									</br></br>
									<?php echo 'Đã được đặt <b>'.$food['number_order'].'</b> lần' ?>
								</div>
								<div class="menu-item-price">
									<p style="font-size: 16px; color: red"><?php echo $food['price_sale'].' đ' ?></p>
									<p style="text-decoration: line-through;"><?php echo $food['price'].' đ' ?></p>
									
								</div>
							</div>
						<?php }
					} ?>
				</div>
				<div id="tab3" class="tab-content">
					<table>
						<tr>
							<?php echo $this->Form->create("Comments", array("inputDefaults" => array("label" => false))) ?>

							<td>
								<table>
									<tr>
										<td style = "width: 625px;height: 90px;">
											<?php
												if($error == null){
													echo $this->Form->input("content", array("type" => "textarea"));


												}else{
													echo '<div class = "error-message">'.$error.'</div>';
													echo '</br>';
													echo $this->Form->input("content", array("type" => "textarea",'disabled' => 'disabled', "placeholder" => $error));
												}
											 	?>
										</td>

									</tr>
									<tr><td class="submit-comment"><?php echo $this->Form->submit("Bình luận") ?></td></tr>
								 
								</table>
								
							</td>

						
							<?php echo $this->Form->end(); ?>
						</tr>
						<tr>
							<table width="100%" class="comment">
								<?php
							foreach ($comment_list as $value) {?>
								<tr >
									<td >
									<span class="name"><?php echo $value['username'] ?>: </span><?php echo $value['content']?>
									<?php if(!empty($value['subcomment'])){ ?>
										
										<table class="subcomment" width="100%">
											<?php 
												$name = $restaurant['Restaurant']['name'];
												$content = $value['subcomment']['Comment']['content'];
												$id = $value['subcomment']['Comment']['id'];
												?>
												<tr>
													<td><span class="name"><?php echo $name ?>: </span><?php echo $content?>
													</td>
												</tr>
										</table>
											
									<?php } ?>
									</td>
								</tr>
							<?php } ?>
							</table>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

 <script src="https://code.jquery.com/jquery-1.8.2.min.js"></script>

  <!-- Include jQuery Popup Overlay -->
  <script src="https://cdn.rawgit.com/vast-engineering/jquery-popup-overlay/1.7.13/jquery.popupoverlay.js"></script>
  <script>
    $(document).ready(function() {
      $('#my_popup').popup();
    });
  </script>

  <script type="text/javascript">
		
  		$(".button-delete").click(function(e){
  			var id = this.id;
  			var res = id.split("_");
  			id = res[1];

  			var item_count = document.getElementById("item_count").innerHTML;
  			item_count = parseInt(item_count.toString());

  			var quantity = document.getElementById("quantity_" + id).innerHTML;
  			quantity = parseInt(quantity.toString());

  			var basket_total_price = document.getElementById("basket_total_price").innerHTML;
  			basket_total_price = parseInt(basket_total_price.toString());

  			var price = document.getElementById("product_price_" + id).innerHTML;
  			price = parseInt(price.toString());

  			basket_total_price = basket_total_price - price;
  			
  			
  			// alert(minimum_order);

  			item_count = item_count - quantity;
  			

  			var host = window.location.host;
			var pathArr = window.location.pathname.split('/');
			var name = 'http://';
		  	var url = name.concat(host);
		  	var url = url.concat('/');
		  	var url = url.concat(pathArr[1]);
		  	var url = url.concat('/bills/delete_element');

 			$.ajax({ url: url,
		         data: {
		         	position:id,
		         	total_price:basket_total_price,
		         	item_count:item_count,
		         },
		         type: 'post',
		        success: function() {
		         
			   }
			});
  			var minimum_order = document.getElementById("minimum_order").value;

 			if(basket_total_price != 0){
 				if(basket_total_price >= minimum_order){
 					$('.basket-message').text("Bạn đã đạt mức đặt tối thiểu <?php echo $restaurant['Restaurant']['minimum_order'] ?>đ.");
 					$('.button-confirm').show();
 				}else{
 					$('.basket-message').text("Xin lỗi bạn chưa thể đặt món. Nhà hàng yêu cầu đặt tối thiểu <?php echo $restaurant['Restaurant']['minimum_order'] ?>đ.");
 					$('.button-confirm').hide();
 				}
 			}else{
 				$('.basket-message').text("Đơn hàng trống. Thêm món bằng cách nhấp vào thực đơn."); 
 					$('.button-confirm').hide();

 			}

 			basket_total_price = basket_total_price.toString();
  			basket_total_price = basket_total_price.concat("đ");
  			item_count = item_count.toString();

  			document.getElementById("item_count").value = item_count;
 			document.getElementById("item_count").innerHTML = item_count;
 			document.getElementById("basket_total_price").innerHTML = basket_total_price;
 			document.getElementById("basket_total_price").value = basket_total_price;
 			document.getElementById("basket_price").value = basket_total_price;
 			document.getElementById("basket_price").innerHTML = basket_total_price;

  			var element = document.getElementById("basket_entry_" + id);
  			element.parentNode.removeChild(element);



  			e.preventDefault();
 			e.stopPropagation();
			e.stopImmediatePropagation();

  		});



		$(".button-white-plus").click(function(e) {
			var id = this.id;
			var res = id.split("_");
			id = res[1];
			var quantity = document.getElementById("quantity_" + id).innerHTML;
			quantity = parseInt(quantity.toString());
			var price = document.getElementById("product_price_" + id).innerHTML;
			var res_price = price.split(" ");
			price = res_price[0];

			var basket_total_price = document.getElementById("basket_total_price").innerHTML;
			
			
			var item_count = document.getElementById("item_count").innerHTML;

			price = parseInt(price.toString());
			price = price/quantity;
			basket_total_price = parseInt(basket_total_price.toString());
			basket_total_price = basket_total_price + price;
			
			
			quantity = quantity + 1;
			item_count = parseInt(item_count.toString());
			item_count = item_count + 1;
			price = price * quantity;
			
 			var host = window.location.host;
			var pathArr = window.location.pathname.split('/');
			var name = 'http://';
		  	var url = name.concat(host);
		  	var url = url.concat('/');
		  	var url = url.concat(pathArr[1]);
		  	var url = url.concat('/bills/update_quantity');

 			$.ajax({ url: url,
		         data: {
		         	position:id,
		         	quantity:quantity,
		         	total_price:basket_total_price,
		         	price:price,
		         	item_count:item_count,
		         },
		         type: 'post',
		        success: function() {
		         	
			   }
			});

 			var minimum_order = document.getElementById("minimum_order").value;
 			
 			if(basket_total_price != 0){
 				if(basket_total_price >= minimum_order){
 					$('.basket-message').text("Bạn đã đạt mức đặt tối thiểu <?php echo $restaurant['Restaurant']['minimum_order'] ?>đ.");
 					$('.button-confirm').show();
 				}else{
 					$('.basket-message').text("Xin lỗi bạn chưa thể đặt món. Nhà hàng yêu cầu đặt tối thiểu <?php echo $restaurant['Restaurant']['minimum_order'] ?>đ.");
 					$('.button-confirm').hide();
 				}
 			}else{
 				$('.basket-message').text("Đơn hàng trống. Thêm món bằng cách nhấp vào thực đơn."); 
 					$('.button-confirm').hide();

 			}

 			price = price.toString();
			price = price.concat("đ");

			basket_total_price = basket_total_price.toString();
			basket_total_price = basket_total_price.concat("đ");

			document.getElementById("quantity_" + id).innerHTML = quantity;
			document.getElementById("quantity_" + id).value = quantity;
 			document.getElementById("product_price_" + id).innerHTML = price;
 			document.getElementById("product_price_" + id).value = price;
 			document.getElementById("item_count").value = item_count;
 			document.getElementById("item_count").innerHTML = item_count;
 			document.getElementById("basket_total_price").innerHTML = basket_total_price;
 			document.getElementById("basket_price").value = basket_total_price;
 			document.getElementById("basket_price").innerHTML = basket_total_price;


 			e.preventDefault();
 			e.stopPropagation();
			e.stopImmediatePropagation();

		});
    	

    	$(".button-white-minus").click(function(e) {
			var id = this.id;
			var res = id.split("_");
			id = res[1];
			var quantity = document.getElementById("quantity_" + id).innerHTML;
			quantity = parseInt(quantity.toString());
			var price = document.getElementById("product_price_" + id).innerHTML;
			var res_price = price.split(" ");
			var item_count = document.getElementById("item_count").innerHTML;
			var basket_total_price = document.getElementById("basket_total_price").innerHTML;

			
			price = res_price[0];
			basket_total_price = parseInt(basket_total_price.toString());
			
			
			price = parseInt(price.toString());
			price = price/quantity;

			quantity = quantity - 1;
			if(quantity == 0){
				quantity = 1;
			}else if(quantity > 0){
				basket_total_price = basket_total_price - price;
			
				item_count = parseInt(item_count.toString());
				item_count = item_count - 1;

			}
			
			price = price * quantity;
			
			var host = window.location.host;
			var pathArr = window.location.pathname.split('/');
			var name = 'http://';
		  	var url = name.concat(host);
		  	var url = url.concat('/');
		  	var url = url.concat(pathArr[1]);
		  	var url = url.concat('/bills/update_quantity');

 			$.ajax({ url: url,
		         data: {
		         	position:id,
		         	quantity:quantity,
		         	total_price:basket_total_price,
		         	price:price,
		         	item_count:item_count,
		         },
		         type: 'post',
		        success: function() {
		         
			   }
			});

 			var minimum_order = document.getElementById("minimum_order").value;
 			
 			if(basket_total_price != 0){
 				if(basket_total_price >= minimum_order){
 					$('.basket-message').text("Bạn đã đạt mức đặt tối thiểu <?php echo $restaurant['Restaurant']['minimum_order'] ?>đ.");
 					$('.button-confirm').show();

 				}else{
 					$('.basket-message').text("Xin lỗi bạn chưa thể đặt món. Nhà hàng yêu cầu đặt tối thiểu <?php echo $restaurant['Restaurant']['minimum_order'] ?>đ.");
 					$('.button-confirm').hide();

 				}
 			}else{
 				$('.basket-message').text("Đơn hàng trống. Thêm món bằng cách nhấp vào thực đơn."); 
 					$('.button-confirm').hide();
 				
 			}
 			
 			basket_total_price = basket_total_price.toString();
			basket_total_price = basket_total_price.concat("đ");

			price = price.toString();
			price = price.concat("đ");

			document.getElementById("quantity_" + id).innerHTML = quantity;
			document.getElementById("quantity_" + id).value = quantity;
 			document.getElementById("product_price_" + id).innerHTML = price;
 			document.getElementById("product_price_" + id).value = price;
 			document.getElementById("item_count").value = item_count;
 			document.getElementById("item_count").innerHTML = item_count;
 			document.getElementById("basket_total_price").value = basket_total_price;
 			document.getElementById("basket_total_price").innerHTML = basket_total_price;
 			document.getElementById("basket_price").value = basket_total_price;
 			document.getElementById("basket_price").innerHTML = basket_total_price;

		  
 			e.preventDefault();
 			e.stopPropagation();
			e.stopImmediatePropagation();
		});
  </script>

<?php 
	echo $this->element('bottom')
?>