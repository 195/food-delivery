
<?php 
	echo $this->element('top_admin'); 
	echo $this->element('left_manager');
?>

<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Thực đơn</p>
			<table class="get_table">
				<tr class="row_header">
					<td>STT</td>
					<td>Tên</td>
					<td>Giá</td>
					<td>Danh mục</td>
					<td>Lượt đặt món</td>
					<td>Khuyến mại</td>
					<td>Trạng thái</td>
					<td>Mô tả</td>
					<td></td>
					<td class="add_button"><?php echo $this->Html->image('add-button.png', array('url' => array('controller' => 'foods', 'action' => 'add_food'))) ?></td>
				</tr>
				<?php 
					if(!empty($foods)){
						$i = 1;
						foreach ($foods as $food) {?>
							<tr class="row">
								<td><?php echo $i++ ?></td>
								<td><?php echo $food['name']?> </td>
								<td><?php echo $food['price'] ?></td>
								<td><?php echo $food['category'] ?></td>
								<td><?php echo $food['number_order'] ?></td>
								<td><?php if($food['is_sale'] == 0) echo 'Không'; else echo 'Có' ?></td>
								<td><?php if($food['status'] == 0) echo 'Còn hàng'; else echo 'Hết hàng'; ?></td>
								<td><?php echo $food['description'] ?></td>
								<td><?php echo $this->Html->link('sửa', array('controller' => 'foods', 'action' => 'update_food', $food['id'])) ?></td>
								<td>
								<?php echo $this->Html->link('Xóa',array('controller' => 'foods', 'action' => 'delete_food', $food['id'])) ?>
								</td>
							</tr>
						<?php }
					}?>	
			</table>
		</div>
	</div>
</div>


