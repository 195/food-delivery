<?php 
	echo $this->element('top_admin');
	echo $this->element('left_manager');
?>
<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Sửa món ăn</p>
				<?php echo $this->Form->create("Food",array("id" => "add", 'enctype' => 'multipart/form-data', "novalidate" => true,"inputDefaults" => array(
					"label" => false))); ?> 
					<table class="addTable">
				  		<tr width="" >
						    <td>Tên món <span style="color: red">*</span></td>
						    <td>
							    <?php echo $this->Form->input('name', array('default' => $food['Food']['name'])) ?>
						    </td>
						</tr>
						<tr>
							<td>Ảnh</td>
							<td>
								<?php echo $this->Form->input('image', array('type' => 'file')); ?>
     						</td>
						</tr>
						<tr>
						    <td>Giá tiền <span style="color: red">*</span></td>
						    <td>
							    <?php echo $this->Form->input('price', array('type' => 'text', 'default' => $food['Food']['price'])) ?>
						    </td>
						</tr>
						<tr>
							<td >Danh mục <span style="color: red">*</span></td>
							<td>
								<?php 
									echo $this->Form->input("category", array(
										'options' => $select,
				  						"empty" => "---",
				  						"default" => $food['Food']['category'],
										));
								?>
							</td>
						<tr  class="many_input">
							<td>(Danh mục khác)</td>
							<td>
								<?php echo $this->Form->input("new_category");?>
								<div class ="error-message"><?php echo $error ?></div>
							</td>
						</tr>
						<tr>
							<td>Trạng thái <span style="color: red">*</span></td>
							<td>
			  					<?php
			  						$select = array('0' => 'Còn món', '1' => 'Hết món');
			  						echo $this->Form->input("status",array(
				  							"options" => $select,
				  							"default" => $food['Food']['status']
											)); 
			  					?>
			  				</td>
						</tr>
						<tr>
							<td>Mô tả</td>
							<td>
								<?php echo $this->Form->input('description', array('type' => 'textarea', 'default' => $food['Food']['description']));?>
							</td>
						</tr>
						
					</table>
					<table>
						<tr>
							<td><?php echo $this->Html->link('Quay lại', array('controller' => 'foods', 'action' => 'index'), array('id' => 'button'))?> </td>
							<td><?php echo $this->Form->submit('Lưu', array('id' => 'button')); ?></td>
						</tr>	
					</table>
					
				<?php echo $this->Form->end(); ?>
			</table>
		</div>
	</div>
</div>

<?php 
	echo $this->element('bottom')
?>