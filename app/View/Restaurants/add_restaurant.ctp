<?php echo $this->element('top_admin'); ?>
<?php echo $this->element('left_admin') ?>
<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Thêm nhà hàng</p>
	
	<?php echo $this->Form->create("Restaurant",array("id" => "add", "novalidate" => true,"inputDefaults" => array(
		"label" => false))); ?> 
		<table class="addTable">
			<tr>
			    <td width="30%">Quản lý</td>
			    <td width="70%">
				    <?php 
				    	echo $this->Form->input('manager',array(
				    		'empty' => '---',
				    		'options' => $select
				    		)); 
				    ?>
			    </td>
			</tr>
	  		<tr>
			    <td>Mã nhà hàng</td>
			    <td>
				    <?php echo $this->Form->input('code',array("placeholder" => "Example: MCF(Mood cafe)")) ?>
			    </td>
			</tr>
			<tr>
			    <td>Tên nhà hàng</td>
			    <td>
				    <?php echo $this->Form->input('name',array("placeholder" => "Name")) ?>
			    </td>
			</tr>
			<tr>
			    <td>Địa chỉ</td>
			    <td>
				    <?php echo $this->Form->input('address',array("placeholder" => "Address")) ?>
			    </td>
			</tr>
			<tr>
			    <td>Số điện thoại</td>
			    <td>
				    <?php echo $this->Form->input('tel1',array("placeholder" => "Example: 0435561235", 'type' => 'text')) ?>
			    </td>
			</tr>
			<tr>
				<td>Trạng thái </td>
				<td>
  					<?php
  						$select = array('0' => 'Đang hoạt động', '1' => 'Đóng cửa');
  						echo $this->Form->input("status",array(
	  							"options" => $select,
	  							"default" => 0,
								)); 
  					?>
  				</td>
			</tr>
			<tr>
				<td>Loại nhà hàng (giữ phím Ctrl để chọn nhiều mục)</td>
				<td>
					<?php 
						echo $this->Form->input("category", array(
							'options' => $select2,
	  						"multiple" => "multiple",
							));
					?>
				</td>
			</tr>
			<tr>
				<td colspan="2">Khoảng giá</td>
			</tr>
			<tr>
				<td  class="many_input" colspan="2">
					<table>
						<tr>
							<td width="23%">Giá nhỏ nhất</td>
							<td><?php echo $this->Form->input('price_min', array('type' => 'text', 'default' => '0'));?></td>
							<td width="23%">Giá lớn nhất</td>
							<td><?php echo $this->Form->input('price_max', array('type' => 'text', 'default' => '0')); ?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">Giờ làm việc</td>
			</tr>
			<tr>
				<td class="many_input">Giờ mở cửa</td>
				<td class="many_input">
					<?php echo $this->Form->input('time_start', array('type' => 'time','value' => '8:00')) ?>
				</td>
			</tr>
			<tr>
				<td class="many_input">Giờ đóng cửa</td>
				<td class="many_input">
					<?php echo $this->Form->input('time_end', array('type' => 'time', 'value' => '17:00'));?>
				</td>
			</tr>
			<tr>
				<td>Phí vận chuyển/km</td>
				<td class="many_input">
					<?php 
						echo $this->Form->input('delivery_fee', array('type' => 'text', 'default' => '0'));
					?>
				</td>
			</tr>
			<tr>
				<td>Mức phí tối thiểu</td>
				<td class="many_input">
					<?php 
						echo $this->Form->input('minimum_order', array('type' => 'text', 'default' => '0'));
					?>
				</td>
			</tr>
			
		</table>
		<table>
			<tr>
				<td><?php echo $this->Html->link('Back', array('controller' => 'restaurants', 'action' => 'index'), array('id' => 'button'))?> </td>
				<td><?php echo $this->Form->submit('Thêm', array('id' => 'button')); ?></td>
			</tr>	
		</table>
		
	<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
<?php 
	echo $this->element('bottom')
?>
		
