
<?php 
	echo $this->element('top_admin'); 
	echo $this->element('left_manager');
?>

<div id="main-contend">
	<div class="wrapper">
		<div class="col-md-12">
			<div class="showback">
				<p class="table_tittle">Thông tin nhà hàng</p>
				<?php if(!empty($restaurant)){ ?>
				<table class="get-info">
					<tr>
						<td width="35%">
							<?php if(!empty($image)){?>
								<img src="<?php echo $image['Image']['url'] ?>" width=auto height="99"></td>
							<?php } else {
								echo $this->Html->image('nophoto.jpg', array('width' =>'auto', 'height' => '99'));
							}?>
						<td>
							<table class="sub-get-info">
								<tr>
									<td width="25%" class="tittle">Mã Code: </td>
									<td><b><?php echo $restaurant['Restaurant']['code'] ?></b></td>
								</tr>
								<tr>
									<td class="tittle">Tên: </td>
									<td><?php echo $restaurant['Restaurant']['name'] ?></td>
								</tr>
							</table>
						</td>
					</tr>
					
					<tr>
						<td class="tittle">Địa chỉ</td>
						<td><?php echo $restaurant['Restaurant']['address'] ?></td>
					</tr>
					<tr>
						<td class="tittle">Số điện thoại</td>
						<td><?php echo $restaurant['Restaurant']['tel1'] ?></td>
					</tr>
					<tr>
						<td class="tittle">Website</td>
						<td><?php echo $restaurant['Restaurant']['website'] ?></td>
					</tr>
					<tr>
						<td class="tittle">Email</td>
						<td><?php echo $restaurant['Restaurant']['email'] ?></td>
					</tr>
					<tr>
						<td class="tittle">Trạng thái</td>
						<td>
							<?php  
								switch ($restaurant['Restaurant']['status']) {
									case '0':
										echo 'Đang hoạt động';
										break;
									case '1':
										echo 'Đóng cửa';
										break;
									default:
										break;
								}
							?>
						</td>
					</tr>
					<tr>
						<td class="tittle">Giờ làm việc</td>
						<td><?php echo $restaurant['Restaurant']['time_start'].' - '.$restaurant['Restaurant']['time_end'] ?></td>
					</tr>
					<tr>
						<td class="tittle">Khoảng giá</td>
						<td><?php echo $restaurant['Restaurant']['price_zone'] ?></td>
					</tr>
					<tr>
						<td class="tittle">Mức phí tối thiểu</td>
						<td><?php echo $restaurant['Restaurant']['minimum_order'] ?></td>
					</tr>
					<tr>
						<td class="tittle">Mức phí miễn phí vận chuyển</td>
						<td>
							<?php echo $restaurant['Restaurant']['free_delivery'] ?>
						</td>
					</tr>
					<tr>
						<td class="tittle">Phí vận chuyển/km</td>
						<td><?php echo $restaurant['Restaurant']['delivery_fee'] ?></td>
					</tr>
				</table>
				<?php } else {
					echo 'Nhà hàng không tồn tại';
				}?>
				<table>
					<tr>
						<td width="75%"></td>
						<td><?php echo $this->Html->link('Sửa', array('controller' => 'restaurants', 'action' => 'update_restaurant_info',$restaurant['Restaurant']['id']), array('id' => 'button'));	?></td>
					</tr>
				</table>
				
			</div>
		</div>
	</div>
</div>

<?php 
	echo $this->element('bottom')
?>

