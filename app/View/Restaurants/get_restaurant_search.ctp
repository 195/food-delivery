<?php echo $this->element('top_customer') ?>

<div class="highligh">
	<div class="main">
		
		<div class="col-md-3" style="margin-top: 45px;">
			<div class="block-left">
				<h3>Hà Nội</h3>
				<div class="left-menu">
					<ul class="left-menu-list">
						<li><?php echo $this->Html->link('Tìm theo nhà hàng', array('controller' => 'restaurants', 'action' => 'get_restaurant_search', 1)) ?></li>
						<li><?php echo $this->Html->link('Tìm theo món ăn', array('controller' => 'restaurants', 'action' => 'get_restaurant_search', 2)) ?></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-9" style="margin-top: 45px;">
			<div class="res-list-wrap">
				<div class="res-list">
					<?php if(!empty($restaurant_list)){
						foreach ($restaurant_list as $restaurant) {?>
						<div class="res-item">
							<div class="res-item-img">
								<?php if(!empty($restaurant['url'])) echo $this->Html->image($restaurant['url'], array('width' => '135px', 'height' => '125px')); else echo $this->Html->image('nophoto.jpg', array('width' => '135px', 'height' => '125px' )); ?>
							</div>
							<div class="res-item-name">
								<h3><?php echo $this->Html->link($restaurant['name'], array('controller' => 'foods', 'action' => 'get_food_list', $restaurant['code']))?></h3>
								<div class="res-item-contact">
									<?php echo $restaurant['address'] ?>
								</div>
								<div class="res-item-delivery">
									<b>Giờ mở cửa:</b> <?php echo $restaurant['time_start'].' - '.$restaurant['time_end'] ?>
								</div>
								<div class="res-item-delivery">
									<b>Khoảng giá: </b> <?php echo $restaurant['price_zone'] ?>
								</div>
								<div class="res-item-delivery">
									<b>Đặt tối thiểu:</b> <?php echo $restaurant['minimum_order'].'đ' ?>
								</div>
							</div>
							<div class="res-item-status">
								<p><?php if($open = 1) echo "<p style='color: #008000'>Đang hoạt động</p>"; else echo "Ngưng giao"; ?></p>
								<p><span><?php echo $this->Html->link('Xem thực đơn', array('controller' => 'foods', 'action' => 'get_food_list', $restaurant['code'])) ?></span></p>
							</div>
						</div>
					<?php } 
					} else {
						echo 'Không tìm thấy kết quả.';
					}?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
	echo $this->element('bottom')
?>