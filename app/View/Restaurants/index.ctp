<?php echo $this->element('top_admin'); ?>
<div class="clear"></div>
<div class="page">
	<p class="table_tittle">Danh sách nhà hàng</p>
	<table class="get_table">
		<tr class="row_tittle">
			<td colspan="7">DANH SÁCH NHÀ HÀNG ĐANG CHỜ XỬ LÝ</td>
		</tr>
		<tr class="row_header"> 
			<td>STT</td>
			<td>Mã nhà hàng</td>
			<td>Tên</td>
			<td>Địa chỉ</td>
			<td>Số điện thoại</td>
			<td>Quản lý</td>
			<td></td>
		</tr>
		<?php 
			$i = 1;
			if(isset($restaurants_processing) && !empty($restaurants_processing)){
				foreach ($restaurants_processing as $res) {?>
					<tr class="row">
						<td><?php echo $i++ ?></td>
						<td><?php echo $res['code'] ?></td>
						<td><?php echo $res['name'] ?></td>
						<td><?php echo $res['address'] ?></td>
						<td><?php echo $res['tel'] ?></td>
						<td><?php if($res['manager'] == -1) echo ''; else echo $res['manager']?></td>
						<td><?php echo $this->Html->link('Thêm', array('controller' => 'restaurants', 'action' => 'confirm_restaurant',$res['id'])) ?></td>
					</tr>
				<?php }
			} else { ?>
			<tr class="row">
				<td colspan="6">Không có nhà hàng đang chờ xử lý</td>
			</tr>
			<?php }
		?>
	</table>

	<table class="get_table">
		<tr class="row_tittle">
			<td colspan="7">DANH SÁCH NHÀ HÀNG</td>
			<td class="add_button"><?php echo $this->Html->image('add-button.png', array('url' => array('controller' => 'restaurants', 'action' => 'add_restaurant'))) ?></td>
		</tr>
		<tr class="row_header"> 
			<td>STT</td>
			<td>Mã nhà hàng</td>
			<td>Tên</td>
			<td>Địa chỉ</td>
			<td>Số điện thoại</td>
			<td>Quản lý</td>
			<td>Trạng thái</td>
			<td></td>
		</tr>
		<?php 
			$i = 1;
			if(isset($restaurants) && !empty($restaurants)){
				foreach ($restaurants as $res) {?>
					<tr class="row">
						<td><?php echo $i++ ?></td>
						<td><?php echo $res['code']?></td>
						<td><?php echo $res['name'] ?></td>
						<td><?php echo $res['address'] ?></td>
						<td><?php echo $res['tel'] ?></td>
						<td><?php echo $res['manager']?></td>
						<td>
							<?php
								if($res['status'] == 0){
									echo 'Hoạt động';
								} elseif ($res['status'] == 1) {
									echo 'Đóng cửa'	;
								} else {
									echo 'Không tồn tại';
								}
							?>
						</td>
						<td><?php echo $this->Html->link('Update', array('controller' => 'restaurants', 'action' => 'update_restaurant',$res['id'])) ?></td>
					</tr>
				<?php }
			} else { ?>
			<tr class="row">
				<td colspan="6">Không có nhà hàng trong hệ thống</td>
			</tr>
			<?php }
		?>
	</table>
</div>