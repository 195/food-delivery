<?php echo $this->element('top_customer') ?>

<div class="highlight">
<?php if($this->Session->check('user.name')){

?>

	<div class="container">
		<div class="register">
			<h2>Đăng kí nhà hàng</h2>
			<?php if($success == ''){ ?>
				<p>
					Hợp tác với chúng tôi để tăng doanh thu cho nhà hàng của bạn. Chúng tôi sẽ mang thương hiệu của bạn đến với nhiều khách hàng hơn mà hoàn toàn không mất phí.</br></br>
					<i><b>Lưu ý:</b></br>
					- Điền đầy đủ các trường có đánh dấu sao. </br>
					- Mã nhà hàng có 3 kí tự do bạn tự đặt, mã nhà hàng của các nhà hàng là khác nhau.</i>
				</p>
				<?php echo $this->Form->create("Restaurant",array("id" => "add", 'enctype' => 'multipart/form-data', "novalidate" => true,"inputDefaults" => array("label" => false))); ?>
				<table class='register-table' width="100%">
					<tr>
						<td width="30%"><label>Tên nhà hàng <span style="color: red">*</span></label></td>
						<td><?php echo $this->Form->input('name') ?></td>
					</tr>
					<tr>
						<td><label>Mã nhà hàng <span style="color: red">*</span></label></td>
						<td><?php echo $this->Form->input('code') ?></td>
					</tr>
					<tr>
						<td><label>Ảnh đại diện</label></td>
						<td><?php echo $this->Form->input('image', array('type' => 'file')) ?></td>
					</tr>
					<tr>
						<td><label>Địa chỉ nhà hàng<span style="color: red">*</span></label></td>
						<td><?php echo $this->Form->input('address') ?></td>
					</tr>
					<tr>
						<td><label>Số điện thoại nhận đơn hàng <span style="color: red">*</span></label></td>
						<td><?php echo $this->Form->input('tel1') ?></td>
					</tr>
					<tr>
						<td>Loại nhà hàng (giữ phím Ctrl để chọn nhiều mục)</td>
						<td>
							<?php 
								echo $this->Form->input("category", array(
									'options' => $select,
			  						"multiple" => "multiple",
									));
							?>
						</td>
					</tr>
					<tr>
						<td>Loại khác</td>
						<td><?php echo $this->Form->input("other_category") ?></td>
					</tr>
					<tr>
						<td colspan="2">						
							<table>
								<tr>
									<td width="23%">Giá nhỏ nhất <span style="color: red">*</span></td>
									<td><?php echo $this->Form->input('price_min', array('default' => 0));?></td>
									<td width="23%">Giá lớn nhất <span style="color: red">*</span></td>
									<td><?php echo $this->Form->input('price_max', array('default' => 0)); ?></td>
								</tr>
								<tr>
									<td>Giờ mở cửa <span style="color: red">*</span></td>
									<td> <input type="time" name="data[Restaurant][time_start]"></input></td>
									<td>Giờ đóng cửa <span style="color: red">*</span></td>
									<td><input type="time" name="data[Restaurant][time_end]"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>Phí vận chuyển/km <span style="color: red">*</span></td>
						<td>
							<?php 
								echo $this->Form->input('delivery_fee', array('type' => 'text', 'default' => '0'));
							?>
						</td>
					</tr>
					<tr>
						<td>Mức phí tối thiểu <span style="color: red">*</span></td>
						<td>
							<?php 
								echo $this->Form->input('minimum_order', array('type' => 'text', 'default' => '0'));
							?>
						</td>
					</tr>
					<tr>
						<td>Mức miễn phí vận chuyển</td>
						<td>
							<?php echo $this->Form->input('free_delivery') ?>
						</td>
					</tr>
					<tr>
						<td>Email</td>
						<td><?php echo $this->Form->input('email') ?></td>
					</tr>
					<tr>
						<td>Website</td>
						<td><?php echo $this->Form->input('website') ?></td>
					</tr>
				</table>
				<p class="btn">
					<?php echo $this->Form->submit('Đăng ký nhà hàng', array('class' => 'button')) ?>
				</p>
				<?php echo $this->Form->end() ;
			} else if($success == 'true'){
				echo '<p>Thông tin của bạn đã được gửi lên hệ thống. Vui lòng đợi hồi âm của chúng tôi gửi vào email.</p>';
			} else if($success == 'false'){
				echo '<p>Đã có lỗi xảy ra. Vui lòng thử lại sau vài phút.</p>';
			} ?>
		</div>
	</div>
<?php } else {?>
	<div class="container-less">
		<div class="register">
			<?php echo '<p>Bạn cần '.$this->Html->link('Đăng ký', array('controller' => 'users', 'action' => 'register')).' hoặc '.$this->Html->link('Đăng nhập', array('controller' => 'users', 'action' => 'login_customer')).' để thực hiện chức năng này.</p>';?>
		</div>
	</div>
	<?php }?>
</div>