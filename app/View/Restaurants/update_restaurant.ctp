<?php echo $this->element('top_admin'); ?>
<div class="clear"></div>
<div class="page">
<p class="table_tittle">Sửa thông tin nhà hàng</p>
	
	<?php echo $this->Form->create("Restaurant",array("id" => "update", "novalidate" => true,"inputDefaults" => array(
		"label" => false))); ?> 
		<table class="addTable">
			<tr>
			    <td width="30%">Quản lý</td>
			    <td width="70%">
				    <?php 
				    	echo $this->Form->input('manager',array(
				    		'empty' => '---',
				    		'options' => $select,
				    		'default' => $restaurant['Restaurant']['manager']
				    		)); 
				    ?>
			    </td>
			</tr>
	  		<tr>
			    <td>Mã nhà hàng</td>
			    <td>
				    <?php echo $restaurant['Restaurant']['code'] ?>
			    </td>
			</tr>
			<tr>
			    <td>Tên nhà hàng</td>
			    <td>
				    <?php echo $this->Form->input('name',array("placeholder" => "Name", "value" => $restaurant['Restaurant']['name'])) ?>
			    </td>
			</tr>
			<tr>
			    <td>Địa chỉ</td>
			    <td>
				    <?php echo $this->Form->input('address',array("placeholder" => "Address", "value" => $restaurant['Restaurant']['address'])) ?>
			    </td>
			</tr>
			<tr>
			    <td>Số điện thoại</td>
			    <td>
				    <?php echo $this->Form->input('tel1',array("placeholder" => "Example: 0435561235", "value" => $restaurant['Restaurant']['tel1'])) ?>
			    </td>
			</tr>
			<tr>
				<td>Trạng thái</td>
				<td>
  					<?php
  						$select = array('0' => 'Đang hoạt động', '1' => 'Đóng cửa');
  						echo $this->Form->input("status",array(
	  							"options" => $select,
	  							"default" => $restaurant['Restaurant']['status']
								)); 
  					?>
  				</td>
			</tr>
			<tr>
				<td>Category</td>
				<td>
					<?php 
						echo $this->Form->input("category", array(
							'options' => $select2,
	  						"multiple" => "multiple",
							'default' => $category_list 
							));
					?>
				</td>
			</tr>
			<tr>
				<td>Khoảng giá</td>
				<td>
					<?php 
						echo $this->Form->input('price_zone', array('default' => '0', 'value' => $restaurant['Restaurant']['price_zone']));
					?>
				</td>
			</tr>
			<tr>
				<td colspan="2">Giờ làm việc</td>
			</tr>
			<tr>
				<td class="many_input">Giờ mở cửa</td>
				<td class="many_input">
					<?php echo $this->Form->input('time_start', array('type' => 'time','value' => $restaurant['Restaurant']['time_start'])) ?>
				</td>
			</tr>
			<tr>
				<td class="many_input">Giờ đóng cửa</td>
				<td class="many_input">
					<?php echo $this->Form->input('time_end', array('type' => 'time', 'value' => $restaurant['Restaurant']['time_end']));?>
				</td>
			</tr>
			<tr>
				<td>Phí vận chuyển/km</td>
				<td>
					<?php 
						echo $this->Form->input('delivery_fee', array('type' => 'number', 'default' => '0'));
					?>
				</td>
			</tr>
			<tr>
				<td>Mức phí tối thiểu</td>
				<td>
					<?php 
						echo $this->Form->input('minimum_order', array('type' => 'number', 'default' => '0'));
					?>
				</td>
			</tr>
			
		</table>
		<table class="button">
			<tr>
				<td width="75%"><?php echo $this->Html->link('Back', array('controller' => 'restaurants', 'action' => 'index'))?> </td>
				<td><?php echo $this->Form->submit('submit'); ?></td>
			</tr>	
		</table>
		
	<?php echo $this->Form->end(); ?>
		
</div>