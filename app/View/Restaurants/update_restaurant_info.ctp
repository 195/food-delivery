<?php 
	echo $this->element('top_manager'); 
	echo $this->element('left_manager');
?>

<div id="main-contend">
	<div class="wrapper">
		<div class="col-md-12">
			<div class="showback">
				<p class="table_tittle">Thay đổi thông tin nhà hàng</p>
				<?php 
					echo $this->Form->create("Restaurant",array("id" => "update", 'enctype' => 'multipart/form-data',"novalidate" => true,"inputDefaults" => array("label" => false))); 
					echo $this->Form->hidden('id', array('value' => $restaurant['Restaurant']['id'])); 
				?> 
				<table class="addTable">
			  		<tr>
					    <td width="35%">Mã nhà hàng</td>
					    <td>
						    <?php 
						    	echo $this->Form->hidden('code', array('value' => $restaurant['Restaurant']['code']));
						    	echo $restaurant['Restaurant']['code'] ;
						    ?>
					    </td>
					</tr>
					<tr>
					    <td>Tên nhà hàng <span style="color: red">*</span></td>
					    <td>
						    <?php echo $this->Form->input('name',array("placeholder" => "Name", "value" => $restaurant['Restaurant']['name'])) ?>
					    </td>
					</tr>
					<tr>
						<td>Ảnh đại diện</td>
						<td>
							<?php echo $this->Form->input('image', array('type' => 'file')); ?>
 						</td>
					</tr>
					<tr>
					    <td>Địa chỉ <span style="color: red">*</span></td>
					    <td>
						    <?php echo $this->Form->input('address',array("placeholder" => "Address", "value" => $restaurant['Restaurant']['address'])) ?>
					    </td>
					</tr>
					<tr>
					    <td>Số điện thoại <span style="color: red">*</span></td>
					    <td>
						    <?php echo $this->Form->input('tel1',array("placeholder" => "Example: 0435561235", "value" => $restaurant['Restaurant']['tel1'])) ?>
					    </td>
					</tr>
					<tr>
						<td>Website </td>
						<td>
							<?php echo $this->Form->input('website', array("placeholder" => "website", "value" => $restaurant['Restaurant']['website'])); ?>
						</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>
							<?php echo $this->Form->input('email', array("placeholder" => "email", "value" => $restaurant['Restaurant']['email'])); ?>
						</td>
					</tr>
					<tr>
						<td>Trạng thái <span style="color: red">*</span></td>
						<td>
		  					<?php
		  						$select = array('0' => 'Đang hoạt động', '1' => 'Đóng cửa');
		  						echo $this->Form->input("status",array(
			  							"options" => $select,
			  							"default" => $restaurant['Restaurant']['status']
										)); 
		  					?>
		  				</td>
					</tr>
					<tr>
						<td colspan="2">Giờ làm việc <span style="color: red">*</span></td>
					</tr>
					<tr>
						<td class="many_input">Giờ mở cửa</td>
						<td class="many_input">
							<?php echo $this->Form->input('time_start', array('type' => 'time','value' => $restaurant['Restaurant']['time_start'])) ?>
						</td>
					</tr>
					<tr>
						<td class="many_input">Giờ đóng cửa</td>
						<td class="many_input">
							<?php echo $this->Form->input('time_end', array('type' => 'time', 'value' => $restaurant['Restaurant']['time_end']));?>
						</td>
					</tr>
					<tr>
						<td>Khoảng giá <span style="color: red">*</span></td>
						<td>
							<?php 
								echo $this->Form->input('price_zone', array('default' => '0', 'value' => $restaurant['Restaurant']['price_zone']));
							?>
						</td>
					</tr>
					<tr>
						<td>Mức phí tối thiểu <span style="color: red">*</span></td>
						<td>
							<?php 
								echo $this->Form->input('minimum_order', array('type' => 'number', 'default' => '0'));
							?>
						</td>
					</tr>
					<tr>
						<td>Mức phí miễn phí vận chuyển</td>
						<td>
							<?php echo $this->Form->input('free_fee', array("placeholder" => "100000", "value" => $restaurant['Restaurant']['free_delivery'])); ?>
						</td>
					</tr>
					<tr>
						<td>Phí vận chuyển/km <span style="color: red">*</span></td>
						<td>
							<?php 
								echo $this->Form->input('delivery_fee', array('type' => 'number', 'default' => '0'));
							?>
						</td>
					</tr>
					
				</table>
				<table class="button">
					<tr>
						<td width="75%"><?php echo $this->Html->link('Back', array('controller' => 'restaurants', 'action' => 'get_restaurant_info'))?> </td>
						<td><?php echo $this->Form->submit('submit'); ?></td>
					</tr>	
				</table>
				
			<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>