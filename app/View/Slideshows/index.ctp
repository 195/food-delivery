<?php echo $this->element('top_admin') ?>
<?php echo $this->element('left_admin') ?>

<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Danh sách ảnh slideshow</p>
			<table class="get_table">
				<tr></tr>
				<tr class="row">
					<td width="10%">
						<?php 
							echo $this->Form->create("Slideshow",array("id" => "add", 'enctype' => 'multipart/form-data', "novalidate" => true,"inputDefaults" => array("label" => false)));
							echo 'Thêm ảnh:' ;
						?>
					</td>
					<td ><?php echo $this->Form->input('image', array('type' => 'file')) ?></td>
					<td width="13%">Thuộc nhà hàng</td>
					<td><?php echo $this->Form->input('restaurant_code', array('options' => $select, 'empty' => '---')) ?></td>
					<td>
						<?php 
							echo  $this->Form->submit('Thêm');
							echo $this->Form->end();
						?>
					</td>
					<td width="15%"></td>
				</tr>
			</table>
			
			<table class="slideshow">
				<?php if(!empty($image_arr)){
					foreach ($image_arr as $image) {?>
						<tr>
							<td><img src="<?php echo $image['image_link'] ?>" width="355px" height="195px"></td>
							<td>
								<h2><?php echo $image['restaurant'] ?></h2>
								<?php echo $this->Html->link('Xóa', array('controller' => 'slideshows', 'action' => 'delete_image', $image['id'])) ?>
							</td>
						</tr>
					<?php }
				}?>
			</table>
		</div>
	</div>
</div>

<?php echo $this->element('bottom') ?>