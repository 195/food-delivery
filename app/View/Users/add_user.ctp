<?php echo $this->element('top_admin'); ?>
<div class="clear"></div>
<div class="page">
	<p class="table_tittle">Thêm thành viên</p>
	<?php echo $this->Form->create("User",array("method" => "post","novalidate" => true, "inputDefaults" => array(
						"label" => false
					))); ?>
	<table class="addTable">
		<tr>
			<td>Loại <span style="color: #ff0000;">*</span></td>
			<td>
				<?php 
					$select = array(0 => 'Admin', 3 => 'Manager', 2 => 'Customer');
					echo $this->Form->input('master_id',array(
						'options' => $select,
						'default' => 4
						));
				?>
			</td>
		</tr>
		<tr>
			<td width="35%">Email <span style="color: #ff0000;">*</span></td>
			<td>
				<?php echo $this->Form->input("email",array("placeholder" => " Email", "type" => "text")); ?>
			</td>
		</tr>
		<tr>
			<td>Mật khẩu <span style="color: #ff0000;">*</span></td>
			<td>
				<?php echo $this->Form->input('password',array("placeholder" => " Password", "type" => "password")) ?>
			</td>
		</tr>
		<tr>
			<td>Nhập lại mật khẩu <span style="color: #ff0000;">*</span></td>
			<td>
				<?php 
					echo $this->Form->input('re_password',array("placeholder" => " Repeat password", "type" => "password"));
					echo '<p class="error-message">'.$error.'</p>';
				?>
			</td>
		</tr>
		<tr>
			<td>Điện thoại <span style="color: #ff0000;">*</span></td>
			<td>
				<?php echo $this->Form->input("phone",array("id" => "phone","placeholder" => "Phone", 'type' => 'text')); ?>
			</td>
		</tr>
		<tr>
			<td>Tên <span style="color: #ff0000;">*</span></td>
			<td>
				<?php echo $this->Form->input("name",array("id" => "name", "placeholder" => "Name")); ?>
			</td>
		</tr>
		<tr>
			<td>Địa chỉ</td>
			<td>
			<?php echo $this->Form->input("address",array("id" => "address","placeholder" => " Address")); ?>
			</td>
		</tr>

	</table>
	<table class="button">
		<tr>
			<td width="75%"><?php echo $this->Html->link('Quay lại', array('controller' => 'users', 'action' => 'get_user'))?> </td>
			<td><?php echo $this->Form->submit('Thêm'); ?></td>
		</tr>	
	</table>
	<?php $this->Form->end ?>
</div>