<?php echo $this->element('top_admin') ?>
<div class="clear"></div>
<div class="page">
<p class="table_tittle">Danh sách thành viên</p>
	<table class="get_table">
		<tr class="row_header">
			<td>STT</td>
			<td>Tên</td>
			<td>Email</td>
			<td>Số điện thoại</td>
			<td>Loại</td>
			<td colspan="3"><?php echo $this->Html->link('Add', array('controller' => 'users', 'action' => 'add_user')) ?></td>
		</tr>
		<?php
			$i = 1;
			if(isset($users) && !empty($users)){	
				foreach ($users as $user) {?>
					<tr class="row">
						<td><?php echo $i++ ?></td>
						<td><?php echo $user['User']['name'] ?></td>
						<td><?php echo $user['User']['email'] ?></td>
						<td><?php echo $user['User']['phone'] ?></td>
						<td>
							<?php  
							echo $this->Form->create('User',array("method" => "post","novalidate" => true, "inputDefaults" => array(
						"label" => false))); 
							echo $this->Form->hidden('id', array('value' => $user['User']['id']));
							$select = array(0 => 'Quản trị', 1 => 'Quản lý', 2 => 'Khách hàng', 3 => 'Chờ xử lý');
							echo $this->Form->input('type',array(
								'options' => $select,
								'value' => $user['User']['master_id']
								));
							?>
						</td>
						<td>
							<?php 
								echo $this->Form->submit('update-button.png');
								echo $this->Form->end();
						 	?>
						</td>
					</tr>
				<?php }
			}
				
			
		?>
	</table>	
</div>