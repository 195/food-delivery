<?php echo $this->element('top_customer') ?>

<script type="text/javascript" src = "http://code.jquery.com/jquery-1.5.min.js"></script>
<script type="text/javascript" src = "http://cdn.wideskyhosting.com/js/jquery.cycle.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script src="http://malsup.github.io/jquery.cycle2.js"></script>

<div class="highlight">
		<div class="main">
			<div class="highlight-inner">
				<p>Thưởng thức món ngon tại nhà đơn dạng - đa dạng</p>
				<div class="form-search">
					<?php echo $this->Form->create('Restaurant', array("method" => "post","novalidate" => true, "inputDefaults" => array(
						"label" => false))) ?>
					<h3>Chọn món ngay</h3>
					
						<label for="name">Nhập tên món ăn hoặc nhà hàng bạn cần tìm</label>
						<?php 
							echo $this->Form->input('name');
							echo $this->Form->submit('Tìm món ngay', array('class' => 'btn'));
							echo $this->Form->end();
						?>

				</div>
			</div>
			<div class="slideshow">
				<div class="cycle-slideshow" data-cycle-fx=scrollHorz data-cycle-timeout=2000 data-cycle-pager=".example-pager">
				    <!-- empty element for pager links -->
				<?php foreach ($slideshow as $image) {

					echo $this->Html->image($image['Slideshow']['image_link'],array("id" => $image['Slideshow']['restaurant_code'],"onclick" => "goToRestaurant(this.id)"));
				} ?>
				    
				</div>
				<div class="example-pager">
				</div>
			</div>
			<br class="clear">
		</div>
	</div>
	<div class="container">
		<div class="main">
			<div class="home-content">
				<div class="home-content-inner">
					<h2>Nhà hàng mới</h2>
					<?php 
					if(!empty($new_restaurants)){
						foreach ($new_restaurants as $restaurant) {?>
							<div class="info-type">
								<?php if(isset($restaurant['Restaurant']['0'])){
									$link = $restaurant['Restaurant']['0'];
								?>
									<img src='<?php echo $link ?>' width="71" height="69">
								<?php } else {
									echo $this->Html->image('nophoto.jpg', array('width' =>'71', 'height' => '69'));
									} ?>		
								<h3><?php $name = $restaurant['Restaurant']['name']; echo $this->Html->link("$name", array('controller' => 'foods','action' => 'get_food_list', $restaurant['Restaurant']['code'])) ?></h3>
								<p><?php echo $restaurant['Restaurant']['address'] ?></p>
								<p><?php echo $restaurant['Restaurant']['tel1'] ?></p>
								<p><?php echo $restaurant['Restaurant']['time_start'].' - '.$restaurant['Restaurant']['time_end'] ?></p>
							</div>
					<?php }?>
						<div class="viewmore"><?php echo $this->Html->link('Xem thêm', array()) ?></div>
					<?php } ?>
				</div>
				<div class="home-content-inner">
					<h2>Nhà hàng nổi bật</h2>
					<?php 
					if(!empty($special_restaurants)){
						foreach ($special_restaurants as $restaurant) {?>
							<div class="info-type">
								<?php if(isset($restaurant['Restaurant']['0'])){
									$link = $restaurant['Restaurant']['0'];
								?>
									<img src='<?php echo $link ?>' width="71" height="69">
								<?php } else {
									echo $this->Html->image('nophoto.jpg', array('width' =>'71', 'height' => '69'));
									} ?>		
								<h3><?php $name = $restaurant['Restaurant']['name']; echo $this->Html->link("$name", array('controller' => 'foods','action' => 'get_food_list', $restaurant['Restaurant']['code'])) ?></h3>
								<p><?php echo $restaurant['Restaurant']['address'] ?></p>
								<p><?php echo $restaurant['Restaurant']['tel1'] ?></p>
								<p><?php echo $restaurant['Restaurant']['time_start'].' - '.$restaurant['Restaurant']['time_end'] ?></p>
							</div>
					<?php }?>
						<div class="viewmore"><?php echo $this->Html->link('Xem thêm', array()) ?></div>
					<?php } ?>
				</div>
			</div>
			<div class="contextual">
				<h3 class="title1">Đặt món nhanh - Giao hàng nhanh</h3>
				<div class="blog-img">
					<ul class="photo">
						<?php foreach ($food_list as $food) {?>
							<li>
								<?php if(!empty($food['url'])){?>
									<img src="<?php echo $food['url'] ?>" height="76" width="99">
								<?php } else {
									echo $this->Html->image('nophoto.jpg',array('controller' => 'foods', 'action' => 'get_food_list', $food['restaurant_code']));
								}
								echo $this->Html->link($food['name'], array('controller' => 'foods', 'action' => 'get_food_list', $food['restaurant_code']));?>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>

<?php echo $this->element('bottom') ?>
<script type="text/javascript">
	
	function goToRestaurant(restaurant_code){

		var host = window.location.host;
		var pathArr = window.location.pathname.split('/');
		var name = 'http://';
	  	var url = name.concat(host);
	  	var url = url.concat('/');
	  	var url = url.concat(pathArr[1]);
		url = url.concat('/foods/get_food_list/');
		var link = url.concat(restaurant_code);

		window.location.href = link;
		
	}

</script>