
<div class="lg-container">
	<h1>Xin chào!</h1>
	<?php 
		echo $this->Form->create("User", array('id' => 'lg-form', 'method' => 'post', 'inputDefaults' => array('label' => false))); 
		echo $error;
	?>
	<div>
		<label for="email">Email:</label>
		<?php echo $this->Form->input('email', array('type' => 'text', 'id' => 'email', 'placeholder' => 'email')) ?>
	</div>
	<div>
		<label for="Password">Password:</label>
		<?php echo $this->Form->input('password', array('type' => 'password', 'id' => 'password', 'placeholder' => 'mật khẩu'))?>
	</div>
	<div>
		<?php 
			echo $this->Html->link("Quên mật khẩu", array('action' => 'reset_password_admin'));
			echo $this->Form->button('Đăng nhập', array('type' => 'submit', 'id' => 'login')) ?>
	</div>
	<?php echo $this->Form->end;?>
</div>
	