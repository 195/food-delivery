<?php echo $this->element('top_customer') ?>

<div class="highlight">
	<div class="container-less">
		<div class="register">
			<h2>Đăng nhập</h2>
			<?php echo $this->Form->create("User",array("id" => "add", "novalidate" => true,"inputDefaults" => array("label" => false))); 
			echo '<div class="error-message">'.$error.'</div>';
			?>

			<table width="100%">
				<tr>
					<td>Email</td>
					<td><?php echo $this->Form->input('email') ?></td>
				</tr>
				<tr>
					<td>Mật khẩu</td>
					<td><?php echo $this->Form->input('password') ?></td>
				</tr>
			</table>
			<p class="btn">
				<?php echo $this->Form->submit('Đăng nhập', array('class' => 'button button-right')) ?>
			</p>
			<?php echo $this->Form->end() ?>
			<p><?php echo $this->Html->link('Quên mật khẩu', array('controller' => 'users', 'action' => 'reset_password_customer')) ?></p>
			<p>Bạn chưa là thành viên? <?php echo $this->Html->link('Đăng ký miễn phí', array('controller' => 'users', 'action' => 'register')) ?></p>
		</div>
	</div>
</div>