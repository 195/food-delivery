<?php echo $this->element('top_customer');?>

<div class="highlight">
	<div class="container">
		<div class="register">
			<h2>Đăng ký thành viên</h2>
			<p>
				<b>Lưu ý: </b>Bạn cần phải điền tất cả thông tin bên dưới.</br>Email được dùng làm tên đăng nhập.
			</p>
			<?php echo $this->Form->create("User",array("id" => "add", "novalidate" => true,"inputDefaults" => array("label" => false))); ?>
			<table width="100%">
				<tr>
					<td width="35%">Email</td>
					<td>
						<?php echo $this->Form->input("email",array("type" => "text")); ?>
					</td>
				</tr>
				<tr>
					<td>Mật khẩu</td>
					<td>
						<?php echo $this->Form->input('password',array("type" => "password")) ?>
					</td>
				</tr>
				<tr>
					<td>Xác nhận mật khẩu</td>
					<td>
						<?php 
							echo $this->Form->input('re_password',array("type" => "password"));
							echo '<p class="error-message">'.$error.'</p>';
						?>
					</td>
				</tr>
				<tr>
					<td>Điện thoại</td>
					<td>
						<?php echo $this->Form->input("phone",array('type' => 'text')); ?>
					</td>
				</tr>
				<tr>
					<td>Tên</td>
					<td>
						<?php echo $this->Form->input("name"); ?>
					</td>
				</tr>
				<tr>
					<td>Địa chỉ</td>
					<td>
					<?php echo $this->Form->input("address"); ?>
					</td>
				</tr>
			</table>
			<p class="btn">
				<?php echo $this->Form->submit('Đăng ký thành viên', array('class' => 'button')) ?>
			</p>
			<?php echo $this->Form->end()?>
		</div>
	</div>
</div>

<?php 
	echo $this->element('bottom')
?>