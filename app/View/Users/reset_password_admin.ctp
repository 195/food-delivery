
<div class="lg-container">
	<h3>Vui lòng điền email để lấy lại mật khẩu</h1>
	<?php 
		echo $this->Form->create("User", array('id' => 'lg-form', 'method' => 'post', 'inputDefaults' => array('label' => false))); 
		echo $result;
	?>
	<div>
		<label for="email">Email:</label>
		<?php echo $this->Form->input('email', array('type' => 'text', 'id' => 'email', 'placeholder' => 'email')) ?>
	</div>
	
	<div>
		<?php 
			echo $this->Html->link('Quay lại', array('controller' => 'users', 'action' => 'login_admin'));
			echo $this->Form->button('Gửi', array('type' => 'submit', 'id' => 'login')) ?>
	</div>
	<?php echo $this->Form->end;?>
</div>
	