<?php echo $this->element('top_customer') ?>

<div class="highlight">
	<div class="container-less">
		<div class="register">
			<h2>Đăng nhập</h2>
			<?php echo $this->Form->create("User",array("id" => "add", "novalidate" => true,"inputDefaults" => array("label" => false))); ?>
			<p>Vui lòng điển email đăng ký để lấy lại mật khẩu</p>
			<table width="100%">

				<tr>
					<td>Email</td>
					<td><?php echo $this->Form->input('email') ?></td>
				</tr>
			</table>
			<p class="btn">
				<div class = "button button-left"><?php echo $this->Html->link('Quay lại', array('controller' => 'users', 'action' => 'login_customer'))?></div>
				<?php echo $this->Form->submit('Lấy lại mật khẩu', array('class' => 'button button-right')) ?>
			</p>
			<?php echo $this->Form->end() ?>
		</div>
	</div>
</div>