<?php echo $this->element('top_customer') ?>

<div class="highlight">
<?php if($this->Session->check('user.name')){?>

	<div class="container">
		<div class="register">
			<h2>Thay đổi mật khẩu</h2>
			<?php echo $this->Form->create("User",array("id" => "update", "novalidate" => true,"inputDefaults" => array("label" => false))); 
				echo '<div class="error-message">'.$error.'</div>';
				echo '<div style="color:blue">'.$this->Session->read('success').'</div>';
			?>
			<table class='register-table' width="100%">
				<tr>
					<td width="30%"><label>Mật khẩu cũ <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('current_password', array('type' => 'password')) ?></td>
				</tr>
				<tr>
					<td><label>Mật khẩu mới<span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('new_password', array('type' => 'password')) ?></td>
				</tr>
				<tr>
					<td><label>Nhập lại mật khẩu <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('re_password', array('type' => 'password')) ?></td>
				</tr>
			</table>
			<p class="btn">
				<?php echo $this->Form->submit('Lưu', array('class' => 'button')) ?>
			</p>
			<?php echo $this->Form->end();?>
		</div>
	</div>
<?php } ?>