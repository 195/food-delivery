<?php echo $this->element('top_admin'); ?>
<?php echo $this->element('left_admin')?>
<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Thay đổi thông tin cá nhân</p>
			<?php echo $this->Form->create("User",array("method" => "post","novalidate" => true, "inputDefaults" => array("label" => false))); ?>
			<table class="addTable">
				<tr>
					<td colspan = '2'>
						<?php echo '<div style="color:blue">'.$this->Session->read('success').'</div>';?>
						<?php echo '<div class="error-message">'.$error.'</div>';?>
					</td>
				</tr>
		  		<tr>
					<td width="30%"><label>Mật khẩu cũ <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('current_password', array('type' => 'password')) ?></td>
				</tr>
				<tr>
					<td><label>Mật khẩu mới<span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('new_password', array('type' => 'password')) ?></td>
				</tr>
				<tr>
					<td><label>Nhập lại mật khẩu <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('re_password', array('type' => 'password')) ?></td>
				</tr>
			</table>
			<table class="button">
				<tr>
					<td width="75%"><?php echo $this->Html->link('Quay lại', array('action' => 'update_user_admin')) ?></td>
					<td><?php echo $this->Form->submit('Lưu'); ?></td>
				</tr>	
			</table>
			<?php $this->Form->end ?>
		</div>
	</div>
</div>

<?php 
	echo $this->element('bottom')
?>