<?php echo $this->element('top_admin'); ?>
<?php echo $this->element('left_admin')?>
<div id="main-contend">
	<div class="wrapper">
		<div class="showback">
			<p class="table_tittle">Thay đổi thông tin cá nhân</p>
			<?php echo $this->Form->create("User",array("method" => "post","novalidate" => true, "inputDefaults" => array("label" => false))); ?>
			<table class="addTable">
			<?php echo '<div style="color:blue">'.$this->Session->read('noti').'</div>';?>
		  		<tr>
					<td width="30%"><label>Tên người dùng <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('name', array('default' => $user['User']['name'])) ?></td>
				</tr>
				<tr>
					<td><label>Email <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('email', array('default' => $user['User']['email'])) ?></td>
				</tr>
				<tr>
					<td><label>Số điện thoại <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('phone', array('default' => $user['User']['phone'])) ?></td>
				</tr>
				<tr>
					<td><label>Địa chỉ</label></td>
					<td><?php echo $this->Form->input('address', array('default' => $user['User']['address'])) ?></td>
				</tr>
			</table>
			<table class="button">
				<tr>
					<td width="75%"><?php echo $this->Html->link('Đổi mật khẩu', array('action' => 'update_password_admin')) ?></td>
					<td><?php echo $this->Form->submit('Lưu'); ?></td>
				</tr>	
			</table>
			<?php $this->Form->end ?>
		</div>
	</div>
</div>

<?php 
	echo $this->element('bottom')
?>