<?php echo $this->element('top_customer') ?>

<div class="highlight">
<?php if($this->Session->check('user.name')){?>

	<div class="container">
		<div class="register">
			<h2>Thông tin cá nhân</h2>
			<?php 
				echo '<div style="color:blue">'.$this->Session->read('noti').'</div>';
				if(!empty($user)){
				
				echo $this->Form->create("User",array("id" => "update", "novalidate" => true,"inputDefaults" => array("label" => false))); 	
				echo $this->Form->input('id', array('type' => 'hidden', 'value' => $user['User']['id']))	;
			?>
			<table class='register-table' width="100%">
				<tr>
					<td width="30%"><label>Tên người dùng <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('name', array('default' => $user['User']['name'])) ?></td>
				</tr>
				<tr>
					<td><label>Email <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('email', array('default' => $user['User']['email'])) ?></td>
				</tr>
				<tr>
					<td><label>Số điện thoại <span style="color: red">*</span></label></td>
					<td><?php echo $this->Form->input('phone', array('default' => $user['User']['phone'])) ?></td>
				</tr>
				<tr>
					<td><label>Địa chỉ</label></td>
					<td><?php echo $this->Form->input('address', array('default' => $user['User']['address'])) ?></td>
				</tr>
			</table>
			<p class="btn">
				<?php echo $this->Form->submit('Lưu', array('class' => 'button')) ?>
			</p>
			<?php echo $this->Form->end(); } ?>
		</div>
	</div>
<?php } ?>

<?php 
	echo $this->element('bottom')
?>