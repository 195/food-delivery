-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2016 at 05:52 PM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `buyonline`
--
CREATE DATABASE IF NOT EXISTS `buyonline` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `buyonline`;

-- --------------------------------------------------------

--
-- Table structure for table `advertises`
--

CREATE TABLE IF NOT EXISTS `advertises` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_start` date NOT NULL,
  `time_end` date DEFAULT NULL,
  `restaurant_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `advertises`
--

INSERT INTO `advertises` (`id`, `name`, `time_start`, `time_end`, `restaurant_code`, `content`, `create_time`, `update_time`) VALUES
('1', 'Khuyến mại', '2016-04-12', '2016-04-13', 'DTN', NULL, '2016-04-07 00:00:00', NULL);

--
-- Triggers `advertises`
--
DROP TRIGGER IF EXISTS `advertise_UUID`;
DELIMITER //
CREATE TRIGGER `advertise_UUID` BEFORE INSERT ON `advertises`
 FOR EACH ROW SET NEW.id = UUID()
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `advertise_details`
--

CREATE TABLE IF NOT EXISTS `advertise_details` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `advertise_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `food_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `sale_off_price` int(15) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `advertise_details`
--
DROP TRIGGER IF EXISTS `advertise_detail_UUID`;
DELIMITER //
CREATE TRIGGER `advertise_detail_UUID` BEFORE INSERT ON `advertise_details`
 FOR EACH ROW SET NEW.id = UUID()
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE IF NOT EXISTS `bills` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `bill_code` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `restaurant_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `total` int(15) NOT NULL,
  `ship_price` int(10) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL,
  `receiver` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_phone` int(11) NOT NULL,
  `delivery_address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bill_code` (`bill_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`id`, `bill_code`, `restaurant_code`, `total`, `ship_price`, `status`, `receiver`, `delivery_phone`, `delivery_address`, `note`, `create_time`, `update_time`) VALUES
(1, 'DTN2016050001', 'DTN', 110000, 0, 1, 'Anh A', 1687975464, '112 Nguyễn Trãi', '', '2016-05-03 09:00:00', NULL),
(2, 'DTN2016050002', 'DTN', 78000, 0, 0, 'huyền', 123456789, '111 Nguyễn Trãi, Thanh xuân, hà nội', '', '2016-05-05 08:21:37', NULL),
(3, 'DTN2016050003', 'DTN', 80100, 2100, 2, 'huyền', 123456789, '112 Nguyễn Trãi, Thanh Xuân, Hà Nội', 'sdasfasdf', '2016-05-05 08:22:17', NULL),
(4, 'DTN2016050004', 'DTN', 100200, 22200, 0, 'Anh A', 123456789, 'chung cư VP6, hoàng liệt, hoàng mai', '', '2016-05-05 08:27:55', NULL),
(5, 'DTN2016050005', 'DTN', 86200, 22200, 0, 'huyền', 123456789, 'chung cư vp6, hoàng liệt hoàng mai', '', '2016-05-08 12:26:00', NULL),
(6, 'DTN2016050006', 'DTN', 135000, 18000, 1, 'chị hằng', 123456789, '42 hàng bài, hoàn kiếm hà nội', '', '2016-05-08 12:32:58', NULL),
(7, 'DTN2016050007', 'DTN', 126400, 11400, 0, 'Hà anh', 123456789, '235 Xã đàn, đống đa, hà nội', '', '2016-05-23 16:12:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bill_details`
--

CREATE TABLE IF NOT EXISTS `bill_details` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `bill_code` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `food_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(5) NOT NULL,
  `amount` int(15) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bill_details`
--

INSERT INTO `bill_details` (`id`, `bill_code`, `food_id`, `quantity`, `amount`, `create_time`, `update_time`) VALUES
('1', 'DTN2016050001', '5715eeb8-5e04-4936-9348-18a86fe0114a', 3, 75000, '2016-05-04 09:00:00', '2016-05-04 18:03:29'),
('10', 'DTN2016050007', '5711c47e-8e98-4d8c-b0f9-1d006fe0114a', 2, 78000, '0000-00-00 00:00:00', NULL),
('2', 'DTN2016050001', '5717222c-8574-4e28-9f24-08246fe0114a', 1, 35000, '2016-05-04 09:00:00', '2016-05-04 18:03:29'),
('3', 'DTN2016050002', '5711c409-7570-4066-ae90-1d006fe0114a', 2, 78000, '0000-00-00 00:00:00', NULL),
('4', 'DTN2016050003', '5711c409-7570-4066-ae90-1d006fe0114a', 1, 39000, '0000-00-00 00:00:00', NULL),
('5', 'DTN2016050003', '5711c47e-8e98-4d8c-b0f9-1d006fe0114a', 1, 39000, '0000-00-00 00:00:00', NULL),
('6', 'DTN2016050004', '5711c409-7570-4066-ae90-1d006fe0114a', 2, 78000, '0000-00-00 00:00:00', NULL),
('7', 'DTN2016050005', '5711c409-7570-4066-ae90-1d006fe0114a', 1, 39000, '0000-00-00 00:00:00', NULL),
('8', 'DTN2016050005', '5715eeb8-5e04-4936-9348-18a86fe0114a', 1, 25000, '0000-00-00 00:00:00', NULL),
('9', 'DTN2016050006', '5711c409-7570-4066-ae90-1d006fe0114a', 3, 117000, '0000-00-00 00:00:00', NULL);

--
-- Triggers `bill_details`
--
DROP TRIGGER IF EXISTS `bill_detail_UUID`;
DELIMITER //
CREATE TRIGGER `bill_detail_UUID` BEFORE INSERT ON `bill_details`
 FOR EACH ROW SET NEW.id = UUID()
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(15) DEFAULT NULL,
  `type` int(1) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `type`, `create_time`, `update_time`) VALUES
(1, 'Quán ăn', NULL, 0, '2016-03-28 00:00:00', NULL),
(2, 'Cafe', NULL, 0, '2016-03-28 00:00:00', NULL),
(3, 'buffet', NULL, 0, '2016-03-29 08:44:45', NULL),
(4, 'Trà sữa', NULL, 1, '2016-04-05 16:58:46', NULL),
(7, 'Bánh mỳ', NULL, 1, '2016-04-07 08:29:51', NULL),
(8, 'Vị trà trái cây', NULL, 1, '2016-04-16 11:46:52', NULL),
(9, 'Sữa chua uống', NULL, 1, '2016-04-16 11:53:07', NULL),
(10, 'Bakery', NULL, 0, '2016-04-25 16:47:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `restaurant_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `content`, `restaurant_code`, `parent_id`, `create_time`) VALUES
('5739315a-e474-41ca-bace-18746fe0114a', '571d8fbc-7524-4e01-95e5-1b086fe0114a', 'Nhà hàng ngon', 'DTN', NULL, '2016-05-16 09:32:58'),
('573a1018-8fc8-4bc6-9150-15ac6fe0114a', 'DTN', 'Cảm ơn nhiều', 'DTN', '5739315a-e474-41ca-bace-18746fe0114a', '2016-05-17 01:23:20');

--
-- Triggers `comments`
--
DROP TRIGGER IF EXISTS `comment_UUID`;
DELIMITER //
CREATE TRIGGER `comment_UUID` BEFORE INSERT ON `comments`
 FOR EACH ROW SET NEW.id = UUID()
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `favorite_restaurants`
--

CREATE TABLE IF NOT EXISTS `favorite_restaurants` (
  `id` int(50) NOT NULL,
  `user_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `restaurant_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favorite_restaurants`
--

INSERT INTO `favorite_restaurants` (`id`, `user_id`, `restaurant_code`, `create_time`, `update_time`) VALUES
(0, '571d8fbc-7524-4e01-95e5-1b086fe0114a', 'DTN', '2016-05-17 16:57:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

CREATE TABLE IF NOT EXISTS `foods` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(10) NOT NULL,
  `restaurant_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `number_order` int(15) NOT NULL,
  `status` int(1) NOT NULL,
  `is_sale` tinyint(1) NOT NULL DEFAULT '0',
  `category` int(5) NOT NULL,
  `description` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`id`, `name`, `price`, `restaurant_code`, `number_order`, `status`, `is_sale`, `category`, `description`, `create_time`, `update_time`) VALUES
('5711c409-7570-4066-ae90-1d006fe0114a', 'Trà chanh quất (M)', 39000, 'DTN', 0, 0, 0, 8, '', '2016-04-16 11:48:09', '2016-04-20 13:19:13'),
('5711c47e-8e98-4d8c-b0f9-1d006fe0114a', 'Trà sữa Hokkaido (M)', 39000, 'DTN', 0, 0, 0, 4, '', '2016-04-16 11:50:06', '2016-04-20 13:20:50'),
('5711c4b1-9708-41e8-ab76-1d006fe0114a', 'Trà sữa đường đen (M)', 39000, 'DTN', 0, 0, 0, 4, '', '2016-04-16 11:50:57', '2016-04-20 13:20:00'),
('5711c4f1-1738-445a-88b9-1d006fe0114a', 'Trà sữa trân châu (M)', 37000, 'DTN', 0, 0, 0, 4, '', '2016-04-16 11:52:01', '2016-04-20 13:21:03'),
('5711c533-4b0c-40df-834b-1d006fe0114a', 'Sữa chua uống vị dâu tây (M)', 39000, 'DTN', 0, 0, 0, 9, '', '2016-04-16 11:53:07', '2016-04-20 13:18:56'),
('5715eeb8-5e04-4936-9348-18a86fe0114a', 'Trà xanh (M)', 25000, 'DTN', 0, 0, 0, 8, '', '2016-04-19 15:39:20', '2016-04-19 15:47:49');

--
-- Triggers `foods`
--
DROP TRIGGER IF EXISTS `food_UUID`;
DELIMITER //
CREATE TRIGGER `food_UUID` BEFORE INSERT ON `foods`
 FOR EACH ROW SET NEW.id = UUID()
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `restaurant_code` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `food_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `restaurant_code`, `food_id`, `url`, `create_time`) VALUES
('5715eeb8-40b4-41e4-8b0e-18a86fe0114a', 'Tra-xanh-M.jpeg', NULL, '5715eeb8-5e04-4936-9348-18a86fe0114a', 'Http://localhost/buyonline/upload/Tra-xanh-M.jpeg', '2016-04-19 15:39:20'),
('5715f6e4-7ce8-4ccb-9c66-18a86fe0114a', 'Ding-Tea---Nguyen-Trai.jpeg', 'DTN', NULL, 'Http://localhost/buyonline/restaurant_img/Ding-Tea---Nguyen-Trai.jpeg', '2016-04-19 11:14:12'),
('57171f50-d320-4852-b70a-08246fe0114a', 'Sua-chua-uong-vi-dau-tay-M_DTN.jpeg', NULL, '5711c533-4b0c-40df-834b-1d006fe0114a', 'Http://localhost/buyonline/upload/Sua-chua-uong-vi-dau-tay-M_DTN.jpeg', '2016-04-20 13:18:56'),
('57171f61-a9b0-4566-aa38-08246fe0114a', 'Tra-chanh-quat-M_DTN.jpeg', NULL, '5711c409-7570-4066-ae90-1d006fe0114a', 'Http://localhost/buyonline/upload/Tra-chanh-quat-M_DTN.jpeg', '2016-04-20 13:19:13'),
('57171f90-cd2c-41d9-b729-08246fe0114a', 'Tra-sua-duong-den-M_DTN.jpeg', NULL, '5711c4b1-9708-41e8-ab76-1d006fe0114a', 'Http://localhost/buyonline/upload/Tra-sua-duong-den-M_DTN.jpeg', '2016-04-20 13:20:00'),
('57171fc2-e584-4743-8a8f-08246fe0114a', 'Tra-sua-Hokkaido-M_DTN.jpeg', NULL, '5711c47e-8e98-4d8c-b0f9-1d006fe0114a', 'Http://localhost/buyonline/upload/Tra-sua-Hokkaido-M_DTN.jpeg', '2016-04-20 13:20:50'),
('57171fcf-d3a8-4b61-af6f-08246fe0114a', 'Tra-sua-tran-chau-M_DTN.jpeg', NULL, '5711c4f1-1738-445a-88b9-1d006fe0114a', 'Http://localhost/buyonline/upload/Tra-sua-tran-chau-M_DTN.jpeg', '2016-04-20 13:21:03'),
('571de7b5-bd78-4d11-b11d-1d786fe0114a', 'Healthy-Corner-Cafe--Bakery.jpeg', 'HCC', NULL, 'Http://localhost/buyonline/restaurant_img/Healthy-Corner-Cafe--Bakery.jpeg', '2016-04-25 16:47:33'),
('8c714656-1f3c-11e6-8461-240a6423fbe0', 't-korean', 'TKR', NULL, 'http://images.hotdeals.vn/images/uploads/2015/06/20/152133/152133-am-thuc-han-quoc-t-korea-body-4.jpg', '2016-05-21 00:00:00'),
('cc830fab-1f43-11e6-8461-240a6423fbe0', 'cơm 123', '123', NULL, 'http://i.vinface.com/biz/photo/29_106156819153367da53e688/opt.jpg', '2016-05-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE IF NOT EXISTS `restaurants` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel1` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `price_zone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `manager` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `minimum_order` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `free_delivery` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_fee` int(10) NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `name`, `code`, `address`, `tel1`, `price_zone`, `manager`, `status`, `time_start`, `time_end`, `minimum_order`, `free_delivery`, `delivery_fee`, `website`, `email`, `create_time`, `update_time`) VALUES
(1, 'Ding Tea - Nguyễn Trãi', 'DTN', '306 Nguyễn Trãi, Quận Thanh Xuân, Hà Nội', '0438593749', '25000 - 60000', '56ecce8e-53f8-489c-9052-0a146fe0114a', 0, '07:00:00', '20:00:00', '55000', NULL, 3000, '', '', '2016-03-20 01:03:30', '2016-04-20 23:48:47'),
(2, 'T Korean - Ẩm Thực Hàn Quốc', 'TKR', '72 Tuệ Tĩnh, Quận Hai Bà Trưng, Hà Nội', '0438485749', '30000 - 100000', '56eda990-8f44-4dbe-8051-0a046fe0114a', 0, '08:00:00', '16:00:00', '0', NULL, 0, NULL, NULL, '2016-03-20 01:18:37', '2016-04-20 11:45:26'),
(3, 'Cơm 123 - Phố Huế', '123', '55 Phố Huế, Quận Hai Bà Trưng, Hà Nội', '0438593746', '30000 - 55000', '56ed9904-6974-4a06-8e7b-0a046fe0114a', 0, '08:00:00', '16:00:00', '0', NULL, 0, NULL, NULL, '2016-03-20 01:27:16', '2016-04-20 11:45:34'),
(7, 'Healthy Corner Cafe & Bakery', 'HCC', '34 Đào Tấn, Quận Ba Đình, Hà Nội', '0438593749', '50000 - 200000', '56de2151-9dac-4651-bd57-11286fe0114a', 0, '08:00:00', '21:00:00', '100000', '', 5000, '', '', '2016-04-25 17:03:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_categories`
--

CREATE TABLE IF NOT EXISTS `restaurant_categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `restaurant_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(15) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `restaurant_categories`
--

INSERT INTO `restaurant_categories` (`id`, `restaurant_code`, `category_id`, `create_time`) VALUES
(2, 'TKR', 1, '2016-03-28 00:00:00'),
(3, '123', 1, '2016-03-28 00:00:00'),
(6, 'TAM', 2, '2016-04-04 09:11:52'),
(7, 'HCC', 10, '2016-04-25 16:47:33'),
(8, 'HCC', 2, '2016-04-25 16:59:58');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE IF NOT EXISTS `slideshow` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `image_link` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `restaurant_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `master_id` int(2) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `password`, `email`, `phone`, `name`, `address`, `master_id`, `create_time`, `update_time`) VALUES
('56de2151-9dac-4651-bd57-11286fe0114a', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 'huyennm195@gmail.com', '12345678', 'huyền', '', 0, '2016-03-08 07:48:17', NULL),
('56ec2dab-feb0-4707-ad90-17c86fe0114a', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 'huyen_manager@gmail.com', '01687975494', 'huyền', '', 3, '2016-03-18 23:32:43', NULL),
('56ec2e2e-aa70-46c1-bb2c-17c86fe0114a', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 'huyen_customer@gmail.com', '0123456789', 'Huyền', '', 3, '2016-03-18 23:34:54', NULL),
('56ecce8e-53f8-489c-9052-0a146fe0114a', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 'manager_test@gmail.com', '01687975495', 'Huyền 2', '', 1, '2016-03-19 10:59:10', NULL),
('56ed9904-6974-4a06-8e7b-0a046fe0114a', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 'huyen_manager3@gmail.com', '01687975498', 'Huyền 3', '', 1, '2016-03-20 01:23:00', NULL),
('56eda990-8f44-4dbe-8051-0a046fe0114a', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 'huyen_manager4@gmail.com', '01687975494', 'Huyền 4', '', 1, '2016-03-20 02:33:36', NULL),
('56fe35a4-7cfc-4b7d-bc6a-02e06fe0114a', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 'huyen_manager5@gmail.com', '012345678', 'huyền 5', '', 1, '2016-04-01 15:47:32', '2016-04-01 15:50:44'),
('571d8fbc-7524-4e01-95e5-1b086fe0114a', 'e64cc8e8066d4a17b3d3bcd9497906988f846f2e817964f21ddb847a0a8e77d9', 'huyen123@gmail.com', '012345678', 'Khách hàng', 'linh đàm', 2, '2016-04-25 10:32:12', NULL);

--
-- Triggers `users`
--
DROP TRIGGER IF EXISTS `user_UUID`;
DELIMITER //
CREATE TRIGGER `user_UUID` BEFORE INSERT ON `users`
 FOR EACH ROW SET NEW.id = UUID()
//
DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
